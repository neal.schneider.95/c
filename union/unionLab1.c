#include <string.h>
#include <stdio.h>

void main() {
    union rein {
        float b;
        int count;
        char str[10];
    };

    union rein u;

    u.b = 3.14;
    printf("flt: %e\nint: %i\nstr: %10s\n\n", u.b, u.count, u.str);

    u.count = 144;
    printf("flt: %e\nint: %i\nstr: %10s\n\n", u.b, u.count, u.str);

    strcpy(u.str, "Howdy!");
    printf("flt: %e\nint: %i\nstr: %10s\n\n", u.b, u.count, u.str);

    printf("         size address \n");
    printf("u       %5ld %p\n",  sizeof (union rein), &u);
    printf("u.b     %5ld %p\n",  sizeof u.b, &u.b);
    printf("u.count %5ld %p\n",  sizeof u.count, &u.count);
    printf("u.str   %5ld %p\n",  sizeof u.str, &u.str);
}