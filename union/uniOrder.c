#include <stdio.h>
#include <stdint.h>

void main () {
union uni {
    uint32_t i;
    float f;
    char c[8];
} my_union;

union uni u = {"you"};            // <-- This will cause a compile error. (Wrong type)
union uni v = { 1, 3.14, "you" }; // <-- This too.  Can only be one item, since v.i is a single uint32_t
union uni w = { 42 };             // Can only be initialized with an unsigned int

}