#include <stdio.h>
#include <stdint.h>

void main() {
    typedef union ipv4 Ipv4;    // Incomplete typedef (No definition for ipv4 yet)
    union ipv4 {                // union ipv4's definition begins here
        uint8_t octet[4];
        uint32_t address;
    };                          // end of definition
    Ipv4 a;                     // Declaring 'a' as an Ipv4 ('struct ipv4')

    a.octet[3] = 192;
    a.octet[2] = 168;
    a.octet[1] = 0;
    a.octet[0] = 1;
    printf("%d.%d.%d.%d is %X\n",
        a.octet[3], a.octet[2], a.octet[1], a.octet[0],
        a.address);
}