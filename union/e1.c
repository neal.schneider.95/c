#include <stdint.h>
#include <stdio.h>
void main() {
    struct u {
        float f; 
        long long L;
        long double D;
        char s[16];
    };
    struct u u;
    printf("size of u: %3ld, address: %p\n", sizeof (u), &u);
    printf("size of f: %3ld, address: %p\n", sizeof (u.f), &u.f);
    printf("size of D: %3ld, address: %p\n", sizeof (u.D), &u.D);
    printf("size of s: %3ld, address: %p\n", sizeof (u.s), &u.s);
    printf("size of L: %3ld, address: %p\n", sizeof (u.L), &u.L);

    union address {
        int32_t bin;
        uint8_t octect[4];
    };

    union address a;
    for(int i = 0; i<4; i++) {
        a.octect[i] = i*17;
        printf("%4x\n", a.octect[i]);
    }
    printf("%8x\n", a.bin);
}