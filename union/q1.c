typedef union a {
    char name[80];
    float f[20];
    int i;
} A;

typedef struct b {
    char name[80];
    float f[20];
    int i;
} B;

void main (){
    A u = { "Pizza!" };
    B q = { "Pizza!" };
    B w = { "Pizza!", {1.1, 2.2} };
}