#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

void main() {
    typedef union ipv4 {
        uint8_t octet[4];
        uint32_t address;
    } Ipv4;
    Ipv4 *a;                            // Declares 'a' as a pointer to Ipv4

    a = malloc(sizeof Ipv4);    // Allocate and assign space for 'a'
    a->address = 0xC0A80001;            // Give the address a value
    printf("The highest order octet of the address %0X is %hd\n", a->address, a->octet[3]);
/*                                                                ↑           ↑
        Reference the whole 32-bit address with the '->' operator ┘           |
                                                                              |
   Reference only the highest byte of the address with the octet array member ┘
*/    
    free(a);                            // Always free your dynamic memory!
}