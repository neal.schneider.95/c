#include <stdio.h>
#include <stdint.h>

void main() {
    union address64bit {
        uint8_t octet[8];       // Address with octects
        uint64_t address;       // 64 bit address
    };
    union address64bit a;

    // Store the address of the union in the union's address member
    a.address = (uint64_t) &a;
    
    // Print 64 bit address
    printf("This union's address is at %016lX or \n-", a.address);
    
    // Print each of the octets, starting with the most significant (0)
    for (int i = 7; i >= 0; i--) {
        printf("%X-", a.octet[i]);
    }
    printf("\n");
}