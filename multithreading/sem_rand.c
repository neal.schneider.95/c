#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define MEMORY_POOL_SIZE 10

char memory_pool[MEMORY_POOL_SIZE];
sem_t mutex;
sem_t available_memory;

void *allocate_memory(int size) {
    sem_wait(&available_memory);
    sem_wait(&mutex);

    void *ptr = NULL;
    for (int i = 0; i < MEMORY_POOL_SIZE; i++) {
        if (memory_pool[i] == 0) {
            memory_pool[i] = 1;
            ptr = &memory_pool[i];
            break;
        }
    }

    sem_post(&mutex);
    return ptr;
}

void deallocate_memory(void *ptr) {
    sem_wait(&mutex);

    char *block_ptr = (char *)ptr;
    int index = block_ptr - memory_pool;
    if (index >= 0 && index < MEMORY_POOL_SIZE) {
        memory_pool[index] = 0;
        sem_post(&available_memory);
    }

    sem_post(&mutex);
}

void *thread_function(void *arg) {
    int thread_id = *((int *)arg);

    void *ptr = allocate_memory(1);
    if (ptr != NULL) {
        printf("Thread %d allocated memory at address %p\n", thread_id, ptr);

        // Simulate some work with the allocated memory
        int sleep_time = rand() % 3 + 1; // Sleep for 1 to 3 seconds
        printf("Thread %d will sleep for %d seconds\n", thread_id, sleep_time);
        sleep(sleep_time);

        deallocate_memory(ptr);
        printf("Thread %d deallocated memory\n", thread_id);
    }

    return NULL;
}

int main() {
    srand(time(NULL));

    sem_init(&mutex, 0, 1);
    sem_init(&available_memory, 0, MEMORY_POOL_SIZE);

    pthread_t threads[5];
    int thread_ids[5];
    for (int i = 0; i < 5; i++) {
        thread_ids[i] = i;
        pthread_create(&threads[i], NULL, thread_function, &thread_ids[i]);
    }

    for (int i = 0; i < 5; i++) {
        pthread_join(threads[i], NULL);
    }

    sem_destroy(&mutex);
    sem_destroy(&available_memory);

    return 0;
}
