#include <ctype.h>
#include "TestCode.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RUNLOCAL 1

// Structure representing a trie node
// struct Node {
//     struct Node* children[ALPHA_LENGTH];
//     int endOfWord; // 1 if this node represents the end of a word, 0 otherwise
// };

// Function to create and initialize a new trie node
struct Node* createNode() {
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode) {
        newNode->endOfWord = 0;
        for (int i = 0; i < ALPHA_LENGTH; i++) {
            newNode->children[i] = NULL;
        }
    }
    return newNode;
}

// Function to insert a word into the trie
void addWord(struct Node* root, const char* word) {
    struct Node* currentNode = root;
    int len = strlen(word);
    for (int i = 0; i < len; i++) {
        int index = tolower(word[i]) - 'a'; // Convert to lowercase and get the index
        if (!currentNode->children[index]) {
            printf("Creat")
            currentNode->children[index] = createNode();
        }
        currentNode = currentNode->children[index];
    }
    currentNode->endOfWord = 1;
}

// Function to search for a word in the trie
int findWord(struct Node* root, const char* word) {
    struct Node* currentNode = root;
    int len = strlen(word);
    for (int i = 0; i < len; i++) {
        int index = tolower(word[i]) - 'a'; // Convert to lowercase and get the index
        if (!currentNode->children[index]) {
            return 0; // Word not found
        }
        currentNode = currentNode->children[index];
    }
    return currentNode->endOfWord; // Return 1 if the node represents the end of a word
}

// Function to delete the entire trie
void deleteTree(struct Node **root) {
    if (!root || !(*root)) {
        return;
    }
    // for (int i = 0; i < ALPHA_LENGTH; i++) {
    //     // deleteTree((*root)->children[i]);
    //     printf("del-> %c\n", i + 'a');
    //     deleteTree(&((*root)->children[i]));
    // }
    // free(root);
}

// Function to build a trie from an array of words
struct Node* buildTree(const char **wordList, int length) {
    struct Node* root = createNode();
    for (int i = 0; i < length; i++) {
        addWord(root, wordList[i]);
    }
    return root;
}

#ifdef RUNLOCAL
int main() {
    struct Node *trie;
    const char *words[] = {"test", "testing", "bagel", "sandwich", "basket"};
    trie = buildTree(words, 5);

    printf("%s is not found: %d\n", "sand", findWord(trie, "sand"));
    printf("%s is not found: %d\n", "bagel", findWord(trie, "bagel"));
    deleteTree(&trie);
}

// int main() {
//     const char* words[] = {"a", "aa", "ab", "abc"};
//     int wordCount = sizeof(words) / sizeof(words[0]);

//     struct Node* root = buildTree(words, wordCount);

//     // Test findWord function
//     printf("a: %d\n", findWord(root, "a"));
//     printf("aa: %d\n", findWord(root, "aa"));
//     printf("aaa: %d\n", findWord(root, "aaa"));
//     printf("ab: %d\n", findWord(root, "ab"));
//     printf("abc: %d\n", findWord(root, "abc"));
//     printf("aaaa: %d\n", findWord(root, "aaaa"));
//     printf("c: %d\n", findWord(root, "c"));

//     // Clean up the trie
//     deleteTree(&root);

//     return 0;
// }

#endif