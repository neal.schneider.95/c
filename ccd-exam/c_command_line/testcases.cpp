#include <stdlib.h>
#include <gtest/gtest.h>
#include "testcases.h"
#include "TestCode.h"
#include <stdio.h>
#include <stddef.h>
#include <string.h>


// Defines size of string array used in fauxSystem() and length of strings.
#define LEN 256
#define MAXARGS 10

const char *testCodePath="TestCode";
const char *memAllocError = "TEST COULD NOT ALLOCATE MEMORY; STOPPING TESTS.\n";

#ifdef __cplusplus
extern "C" {
#endif
    int libmain(int a, char**b);
#ifdef __cplusplus
}
#endif

//This returns the command line that will be used.
//outFileName is the name of the file they will be writing output to (if not the default)
char *buildCommand(const char *commandFmt, const char *outFileName)
{
    int sz_command = 0;

    //Check command format validity
    if (commandFmt == NULL || strncmp(commandFmt, "", 1) == 0)
    {
        printf("The command format string cannot be null\n");
        return NULL;
    }

    //Suffix outFileName, if any
    if (outFileName == NULL || strncmp(outFileName, "", 1) == 0)
    {
        sz_command = snprintf(NULL, 0, commandFmt, testCodePath) + 1;
    }
    else
    {
        sz_command = snprintf(NULL, 0, commandFmt, testCodePath, outFileName) + 1;
    }

    //Put in the command
    char *command = (char *) calloc(sz_command, sizeof(char));
    if (command != NULL)
    {
        if (outFileName == NULL || strncmp(outFileName, "", 1) == 0)
        {
            snprintf(command, sz_command, commandFmt, testCodePath);
        }
        else
        {
            snprintf(command, sz_command, commandFmt, testCodePath, outFileName);
        }
    }

    return command;
}

//This function emulates calling system("TestCode arguments...") for our testing set up, and students should interpret
//it as a call to system(). Because our testing setup needs to make sure certain routines get called from main, and we
//don't want those cluttering up the student's main function, we have a somewhat elaborate workaround that students
//don't need to understand. Just pretend this function calls system() with the specified command string.
//Accepts command as char* string (just like system())
//Returns result from command's exit code (also like system())
int fauxSystem(char *command)
{
    //argv needs to be allocated on the stack, so that the students see the correct behavior. Otherwise, if they cause a
    int argc=0;
    char* argvi[MAXARGS], *argv[MAXARGS];
    char stack_str0[LEN], stack_str1[LEN], stack_str2[LEN], stack_str3[LEN], stack_str4[LEN], stack_str5[LEN],
         stack_str6[LEN], stack_str7[LEN], stack_str8[LEN], stack_str9[LEN]; 
    argvi[0] = stack_str0; argvi[1] = stack_str1; argvi[2] = stack_str2; argvi[3] = stack_str3; argvi[4] = stack_str4; 
    argvi[5] = stack_str5; argvi[6] = stack_str6; argvi[7] = stack_str7; argvi[8] = stack_str8; argvi[9] = stack_str9;
    assert(MAXARGS <= 10);
    for(int i=0;i<MAXARGS;i++) //Because we'll set argv[last_arg+1] to zero, we need to reinitialize argv every time
    {
        argv[i]=argvi[i];
    } 

    static char* save; //For strtok_r
    char* token=strtok_r(command, " ",&save);  //Plain strtok() is discouraged, e.g., non-reentrant
    while(token != NULL)
    {
        if (strlen(token) >= LEN) 
        {
            printf("FATAL:  token %s exceeded length %d\n",token,LEN-1);
            exit(1);
        }
        strcpy(argv[argc],token);
        argc++;
        token=strtok_r(NULL, " ", &save);
    }
    argv[argc]=0;
    assert(argc < MAXARGS);

    //Must call student libmain here so stack memory remains allocated for their function
    int ret=libmain(argc, argv);

    return ret;
}


void checkFile(const char *fname, const char *words[], int wordslen)
{
    FILE *fp;
    int buflen = 128;
    char buf[buflen];
    int i = 0;
    int cmp;
    fp = fopen(fname, "r");
    ASSERT_FALSE(fname == NULL);
    ASSERT_FALSE(words == NULL);
    ASSERT_FALSE(fp == NULL);
    while(fgets(buf, buflen, fp))
    {
        buf[strcspn(buf, "\n")] = '\0';
        cmp = strcmp(words[i], buf);
        if(cmp)
        {
            printf("words[i]: %s\n buf: %s\n", words[i], buf);
        }
        ASSERT_EQ(0, cmp);
        i++;
    }
}

TEST(FileDumpTests, validInfo)
{
    const char *fname = "fileDumpTest.txt";
    const char *words[] = {"apple", "bagel", "cranberry", "donut"};

    remove(fname); // In case it wasn't removed
    int numWords = 4;
    fileDump(fname, words, numWords);

    checkFile(fname, words, numWords);
    // Cleanup files
    remove(fname);
}

TEST(FileDumpTests, NoArgs)
{
    const char *fname = "noArgs.txt";
    const char *words[] = {};
    int numWords = 0;

    remove(fname); // In case it wasn't removed
    int status = fileDump(fname, words, numWords);

    ASSERT_TRUE(status);
    checkFile(fname, words, numWords);
    // Cleanup files
    remove(fname);
}

TEST(FileDumpTests, nullFname)
{
    const char *fname = NULL;
    const char *words[] = {};
    int numWords = 0;
    int status = fileDump(fname, words, numWords);

    ASSERT_FALSE(status);
}

TEST(FileDumpTests, nullWords)
{
    const char *fname = "nullArgs.txt";
    const char **words = NULL;
    int numWords = 0;

    remove(fname); // In case it wasn't removed
    int status = fileDump(fname, words, numWords);

    ASSERT_FALSE(status);
    ASSERT_TRUE(fopen(fname, "r") == NULL);
    // Cleanup files
    remove(fname);
}

TEST(CommandlineTests, normalUse)
{
    const char *fname = "testresults.txt";
    const char *words[] = {"apple", "bagel", "cranberry", "donut"};
    const char *commandFmt = "%s -f %s apple bagel cranberry donut";
    char *command = buildCommand(commandFmt, fname);
    if (command == NULL)
    {
        FAIL() << memAllocError;
        return;
    }

    remove(fname); // In case it wasn't removed
    int status = fauxSystem(command);
    free(command);
    command = NULL;
    ASSERT_EQ(0, status);
    checkFile(fname, words, 4);
    // Cleanup files
    remove(fname);
}

TEST(CommandlineTests, longOption)
{
    const char *fname = "testresults.txt";
    const char *words[] = {"apple", "bagel", "cranberry", "donut"};
    const char *commandFmt = "%s --filename %s apple bagel cranberry donut";
    char *command = buildCommand(commandFmt, fname);
    if (command == NULL)
    {
        FAIL() << memAllocError;
        return;
    }

    remove(fname); // In case it wasn't removed
    int status = fauxSystem(command);
    free(command);
    command = NULL;
    ASSERT_EQ(0, status);
    checkFile(fname, words, 4);
    // Cleanup files
    remove(fname);
}

TEST(CommandlineTests, defaultFile)
{
    const char *fname = "text.txt";
    const char *words[] = {"apple", "bagel", "cranberry", "donut"};
    const char *commandFmt = "%s apple bagel cranberry donut";
    char *command = buildCommand(commandFmt, "");
    if (command == NULL)
    {
        FAIL() << memAllocError;
        return;
    }

    remove(fname); // In case it wasn't removed
    int status = fauxSystem(command);
    free(command);
    command = NULL;
    ASSERT_EQ(0, status);
    checkFile(fname, words, 4);
    // Cleanup files
    remove(fname);
}

TEST(CommandlineTests, fileNameIsParam)
{
    const char *fname = "apple";
    const char *words[] = {"bagel", "cranberry", "donut"};
    const char *commandFmt = "%s -f apple bagel cranberry donut";
    char *command = buildCommand(commandFmt, ""); // We technically didn't specify a file name
    if (command == NULL)
    {
        FAIL() << memAllocError;
        return;
    }

    remove(fname); // In case it wasn't removed
    int status = fauxSystem(command);
    free(command);
    command = NULL;
    ASSERT_EQ(0, status);
    checkFile(fname, words, 3);
    // Cleanup files
    remove(fname);
}

TEST(CommandlineTests, fileNameIsParamLong)
{
    const char *fname = "apple";
    const char *words[] = {"bagel", "cranberry", "donut"};
    const char *commandFmt = "%s --filename apple bagel cranberry donut";
    char *command = buildCommand(commandFmt, ""); // We technically didn't specify a file name
    if (command == NULL)
    {
        FAIL() << memAllocError;
        return;
    }

    remove(fname); // In case it wasn't removed
    int status = fauxSystem(command);
    free(command);
    command = NULL;
    ASSERT_EQ(0, status);
    checkFile(fname, words, 3);
    // Cleanup files
    remove(fname);
}

TEST(CommandlineTests, paramsWithNoFileName)
{
    const char *fname = "text.txt";
    const char *words[] = {"apple", "bagel", "cranberry", "donut"};
    const char *commandFmt = "%s apple bagel cranberry donut -f";
    char *command = buildCommand(commandFmt, "");
    if (command == NULL)
    {
        FAIL() << memAllocError;
        return;
    }

    remove(fname); // In case it wasn't removed
    int status = fauxSystem(command);
    free(command);
    command = NULL;
    ASSERT_EQ(0, status);
    checkFile(fname, words, 4);
    // Cleanup files
    remove(fname);
}

TEST(CommandlineTests, NoArgs)
{
    const char *fname = "text.txt";
    const char *words[] = {"", "", "", ""};
    const char *commandFmt = "%s";
    char *command = buildCommand(commandFmt, "");
    if (command == NULL)
    {
        FAIL() << memAllocError;
        return;
    }

    remove(fname); // In case it wasn't removed
    int status = fauxSystem(command);
    free(command);
    command = NULL;
    ASSERT_EQ(0, status);
    checkFile(fname, words, 4);
    // Cleanup files
    remove(fname);
}

TEST(CommandlineTests,fileNoArgs)
{
    const char *fname = "testNoArg.txt";
    const char *words[] = {"", "", "", ""};
    const char *commandFmt = "%s -f %s";
    char *command = buildCommand(commandFmt, fname);
    if (command == NULL)
    {
        FAIL() << memAllocError;
        return;
    }

    remove(fname); // In case it wasn't removed
    int status = fauxSystem(command);
    free(command);
    command = NULL;
    ASSERT_EQ(0, status);
    checkFile(fname, words, 4);
    // Cleanup files
    remove(fname);
}

//Test for file open failing case.  Make it fail by using bad path (attempting to write the directory / as a file)
TEST(CommandlineTests,unwriteableBadPath)
{
    const char *fname = "/";  //Directory name is invalid as a filename
    const char *commandFmt = "%s -f %s";
    char *command = buildCommand(commandFmt, fname);
    if (command == NULL)
    {
        FAIL() << memAllocError;
        return;
    }

    int status = fauxSystem(command);
    free(command);
    command = NULL;
    ASSERT_EQ(1, status);
    //Don't need to check or remove the file since we couldn't access it anyway
}

int doTest(const char *binPath)
{
    int status = 0;
    ::testing::InitGoogleTest();

    testCodePath = binPath;

    status = RUN_ALL_TESTS();

    return status;
}

//Students should write the main() function in TestCode.c.
//They do not need to modify or understand what is in here.
int main(int a,char**b) 
{
    if(a==1)
    {
        return doTest(b[0]);
    }
    else
    {
        return libmain(a,b);
    }
}