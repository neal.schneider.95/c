#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "testcases.h"
#include "TestCode.h"

// Refer to README.md for the problem instructions

int fileDump(const char *fname, const char *words[], int wordLen)
{
    if (!fname || !words) {
        printf("FileDump: %p, %p, %d\n", fname, words, wordLen);
        return 0;
    }
    // printf("opening %s\n", fname);
    FILE *outfile = fopen(fname,"w");
    if (!outfile) {
        // printf("Unwritable file: %s\n", fname);
        return 0;
    }
    for(int i = 0; i < wordLen; i++) {
        if (!words[i]) {
            printf("words[%d] is NULL\n", i);
            return 0;
        }
        fprintf(outfile, "%s\n", words[i]);
    }
    if (fclose(outfile)) {
        printf("Error on close:\n");
        return 0;
    }
    return 1;
}

#define MAXWORDS 30
#define FILENAME_LEN 50

int main(int argc, char *argv[]) {
    char **words = calloc(MAXWORDS, sizeof(char*));
    char fname[FILENAME_LEN] = "text.txt";
    int dump_res = 0;
    // for(int i = 0; i<argc; i++) {
    //     printf("%s ", argv[i]);
    // }
    // printf("\n");
    int word_index = -1;
    for(int i = 1; i<argc; i++) {
        if ((strcmp(argv[i],"-f") == 0) ||
            (strcmp(argv[i], "--filename") == 0)) {
                i++;
                if (i<argc) {
                    strcpy(fname, argv[i]);
                }
        } else {
            words[++word_index] = argv[i]; 
        }
    }
    // printf("File: %s -> ", fname);
    // for(int i = 0; i <= word_index; i++) {
    //     printf("%s-", words[i]);
    // }
    // printf("\n");
    dump_res = fileDump(fname, (const char**)words, word_index+1);
    free(words);
    if (word_index == 0) {
        return EXIT_SUCCESS;
    }
    if (!dump_res) {
        return EXIT_FAILURE;
    } else {
        return EXIT_SUCCESS;
    }
}

