#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>

#include"TestCode.h"

// Refer to README.md for the problem instructions

int isMagicSquare(int values[ROWS][COLS]) {
    /*
    The Magic Square has the following properties:

    The grid contains each of the numbers 1 through 9 exactly once.
    The sum of each row, each column, and each diagonal all add up to the same number.
    */
    
    // count each time a number appears
    const int NUM_CELLS = (ROWS * COLS) + 1;
    int8_t count[(ROWS * COLS) + 1] = {0}; // int type would be fine
    for (int r = 0; r < ROWS; r++) {
        for (int c = 0; c < COLS; c++) {
            if ((values[r][c] < 1) || values[r][c] > ROWS * COLS) {
                return 0;
            }
            count[values[r][c]]++;
        }
    }
    // check for exactly one of each number 1-9
    for (int i = 0; i < NUM_CELLS; i++) {
        if (count[i] > 1) { // the count array should be 
            return 0;  // this is not a magic square
        }
    }

    // sum the rows
    int sum_rows[ROWS] = {0};
    for (int r = 0; r < ROWS; r++) {
        for (int c = 0; c < COLS; c++) {
            sum_rows[r] += values[r][c];
        }
    }
    int magic_sum = sum_rows[0]; // this is the magic number
    // check sum of rows
    for (int i = 1; i < ROWS; i++) { // no need to check the 0 index
        if (sum_rows[i] != magic_sum) {
            return 0;  // this is not a magic square
        }
    }

    // sum the cols
    int sum_cols[COLS] = {0};
    for (int c = 0; c < COLS; c++) {
        for (int r = 0; r < ROWS; r++) {
            sum_cols[c] += values[r][c];
        }
    }
    // check sum of cols
    for (int i = 0; i < COLS; i++) {
        if (sum_cols[i] != magic_sum) {
            return 0;  // this is not a magic square
        }
    }

    // sum the diagonals
    const int NUM_DIAGS = 2;  // there are only 2 diagonals in a square
    int sum_diag[2] = {0};  
    for (int i = 0; i < ROWS; i++){ // it's a square ROWS = COLS
        sum_diag[0] += values[i][i];
    }
    for (int i = 0; i < ROWS; i++){ // it's a square ROWS = COLS
        sum_diag[1] += values[i][ROWS-i-1];
    }

    // check sum of diagonals
    for (int i = 0; i < NUM_DIAGS; i++) {
        if (sum_diag[i] != magic_sum) {
            return 0;  // this is not a magic square
        }
    }    

    // all checks passed
    return 1;
}
