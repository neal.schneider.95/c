#include <stdio.h>
// Declare a student struct with name and a seat #
struct student {
    char name[50];
    int seat;
};

void main(){
    // a Function to print the student
    void print_student(struct student stu) {
        printf("Seat %d: %s\n", stu.seat, stu.name);
    }

    struct student stu1 = { "Tom Ellis", 1 };
    struct student stu2 = { "Gabby Gnome", 2 };
    struct student stu3 = { "Ernest Shackleton", 3 };

    print_student(stu1);
    print_student(stu2);
    print_student(stu3);
}