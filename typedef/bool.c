// #include <stdbool.h>

#include <stdio.h>
void main() {
    // Define the new type alias, bool
    typedef unsigned char bool;
    // Use the new type to declare a variable
    bool logical_result = 1 + 1 == 2;
    if (logical_result) {
        printf("1 + 1 == 2. The world is good!\n");
    } else {
        printf("1 + 1 != 1? Something is terribly wrong.\n");
    }
}