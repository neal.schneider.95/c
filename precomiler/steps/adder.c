#include <stdio.h>

#include "add.h"

void main () {
    int x, y, z;
    x = 50;
    y = 51;
    z = add(x,y);
    printf("%i + %i = %i", x, y, z);
}