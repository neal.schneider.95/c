#define NUM 68.8
#include <stdio.h>

int main(void)
{
    float x = NUM;
    int numSize = sizeof(NUM);
    int xSize = sizeof(x);
    int charsize = sizeof(char);

    if (numSize != xSize){
        printf("%d, %d", numSize, xSize);
        return -1;
    }
    return 0;
}