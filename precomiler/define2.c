#include <stdio.h>
#define TIMES(a,b) (a) * (b)
#define TIMES2(a,b) a * b

int main(void)
{
    int result = (45 + 45) * 2;
    printf("%d\n", result);        

    int result2 = TIMES(45 + 45, 2);
    // will result in this:
    // int result2 = 45 + 45 * 2;

    printf("%d\n", result);

    return 0;
}