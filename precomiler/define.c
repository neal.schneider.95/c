#include <stdio.h> 

#define TIMES(a,b) (a) * (b)
// #define TIMES(a,b) a * b  // note the difference when parentheses are not used!


int main (){
    int result = (45 + 45) * 2;
    printf("%d\n", result);

    int result2 = TIMES(45 + 45, 2);
    result = (45 + 45) * (2);
    printf("%d\n", result2);

}