/*
This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0097 - Create and use pointers.
	S0081 - Implement a looping construct.
	S0082 - Implement if and if/else constructs.
	S0034 - Declare and implement appropriate data types for program requirments.
	S0036 - Declare and implement a char * array (string).
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathmatical equations.
	S0108 - Utilize post and pre increment/decrement operators.
	S0051 - Implement a function that implements pass by reference input parameters.
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0110 - Implement error handling.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0079 - Validate expected input.
	S0160 - Utilize the standard library.
	S0033 - Utilize assignment operators to update a variable.
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
 * The function letterFrequency takes as input a one-line strings
 * and determines the total occurrences of each letter of the alphabet in the line.
 *
 * Case sensitivity is not an issue i.e. "A" and "a" are the considered the same for this question
 * If a non-alpha character is encountered it should be ignored, adding nothing to any count.
 *
 * @param sentence			The input string to process
 * @param frequencyTable	An already allocated buffer in which to place the output
 *
 * Expected Return Values:
 *		- The task is successful: 1
 *		- Bad input is provided: 0
 */

int letterFrequency(const char* sentence, int* frequencyTable)
{
	//declare i as a looping variable
    int i = 0;
	//declare bad char to count bad chars
    int bad_char = 0;
	//if sentece is null return 0
    if(!sentence){
        return 0;
    }
	//while sentece at i is not equal to a terminator ie the end of the string
    while(sentence[i] != '\0'){
		//if the char is not a valid char 
        if(!(isalpha(sentence[i]))){
			//increment bad char
            bad_char++;
			//move to next char in sentence
            i++;
            continue;
        }
        else{
			//add a count to the letter on frequency table
			//use tolower or toupper when using ASCII otherwise the math gets weird
            frequencyTable[tolower(sentence[i])-'a']++;
			//increment i moving to next char in sentence
            i++;

        }
    }
	//check to see how many bad chars are counted if >= the length of the sentence no good
    if(i < bad_char){
        return 0;
    }
    else{
        return 1;
    }
}