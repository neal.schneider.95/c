#include <stdio.h>
int abs (int i){
    return i>0 ? i :-i;
}

int main (){
    int size = 0;
    printf ("Enter the size of the triangles: ");
    scanf ("%d", &size);
    printf("Pattern a\n");
    for (int i = 0; i < size; i++) {
        for (int j = 0; j<=i; j++) {
            printf("*");
        }
        printf("\n");
    }
    printf("\n");

    printf("Pattern b\n");
    for (int i = size; i>0; i--) {
        for (int j = i; j>0; j--) {
            printf ("*");
        }
        printf ("\n");
    }
    printf ("\n");
    
    printf("Pattern c\n");
    for (int i = size; i>0; i--) {
        for (int j = 0; j<size-i; j++){
            printf(" ");
        }
        for (int j = i; j>0; j--) {
            printf("*");
        }
        printf("\n");
    }
    printf ("\n");

    printf("pattern D\n");
    for (int i = 0; i>size; i++) {
        for (int j = 0; j<size-i; j++){
            printf(" ");
        }
        for (int j = i; j>0; j--) {
            printf("*");
        }
        printf("\n");
    }
    printf ("\n");

    printf("Diamond pattern\n");

    for (int i = 0; i< size*2; i ++){
        for (int j = 0; j < abs(i-size); j++){
            printf (" ");
        }
        for (int j = i; j < abs(size-i*2); j++){
            printf ("*");
        }
        printf("\n");
    }

    for (int i = 0; i< size; i ++){
        for (int j = i; j > 0; j--){
            printf (" ");
        }
        for (int j = 0; j < i*2; j++){
            printf ("*");
        }
        printf("\n");
    }

}