// 6.  Using the ```switch``` statement, write a program that converts a numerical grade into a letter grade:

// ```sh
// Enter numerical grade: 84
// Letter grade: B
// ```

// Use the following grading scale: A = 90-10, B = 80-89, C = 70-79, D = 60-69, F = 0-59. Print an error message if the grade is larger than 100 or less than 0. *Hint*: Break the grade into two digits, then use a ```switch``` statement to test the ten's digit.

#include <stdio.h>

void print_single_digit(int digit) {
    switch (digit) {
        case 1:
            printf ("one");
            break;
        case 2:
            printf ("two");
            break;
        case 3:
            printf ("three");
            break;
        case 4:
            printf ("four");
            break;
        case 5:
            printf ("five");
            break;
        case 6:
            printf ("six");
            break;
        case 7:
            printf ("seven");
            break;
        case 8:
            printf ("eight");
            break;
        case 9:
            printf ("nine");
            break;
    }
}

int main ()  
{
    int in, tensdigit, onesdigit;
    printf("Enter a two digit number?\n");
    scanf("%d", &in);
    if (in < 0 || in > 99) {
        printf ("Not a two digit number. \n");
        return -1;
    }
    tensdigit = in / 10;
    onesdigit = in % 10;
    printf("(%d %d)\n", tensdigit, onesdigit);
    switch (tensdigit) {
        case 0:
            print_single_digit(onesdigit);
            break;
        case 1:
            switch (onesdigit) {
                case 1: 
                  printf ("eleven");
                  break;
                case 2: 
                  printf ("twelve");
                  break;
                case 3: 
                  printf ("thirteen");
                  break;
                case 4: 
                  printf ("fourteen");
                  break;
                case 5: 
                  printf ("fifteen");
                  break;
                case 6: 
                  printf ("sixteen");
                  break;
                case 7: 
                  printf ("seventeen");
                  break;
                case 8: 
                  printf ("eighteen");
                  break;
                case 9: 
                  printf ("ninteen");
            }
            printf ("\n");
            return 0;
        case 2:
            printf("twenty ");
            break;
        case 3:
            printf("thirty ");
            break;
        case 4:
            printf("fourty ");
            break;
        case 5:
            printf("fifty ");
            break;
        case 6:
            printf("sizty ");
            break;
        case 7:
            printf("seventy ");
            break;
        case 8:
            printf("eighty ");
            break;
        case 9:
            printf("ninety ");
            break;
    }
    print_single_digit(onesdigit);
}