#include <stdio.h>

int main(void)
{
    int integer = 1;
    float singlePrecision = 2.2;
    double doublePrecision = 3.3;
    char singleChar = '$';
    char singleChar2 = 33;

    printf("Using expressions:\n");
    printf("size of int is %lu \n", sizeof integer);
    printf("size of float is %lu \n", sizeof singlePrecision);
    printf("size of double is %lu \n", sizeof doublePrecision);
    printf("size of char 1 is %lu \n", sizeof singleChar);
    printf("size of char 2 is %lu \n", sizeof singleChar2);

    printf("Using types:\n");
    printf("size of char is %lu \n", sizeof(char));
    printf("size of short is %lu \n", sizeof(short));
    printf("size of int is %lu \n", sizeof(int));
    printf("size of long is %lu \n", sizeof(long));
    printf("size of long long is %lu \n", sizeof(long long));
    printf("size of unsigned long long is %lu \n", sizeof(unsigned long long));
    printf("size of float is %lu \n", sizeof(float));
    printf("size of double is %lu \n", sizeof(double));
    printf("size of long double is %lu \n", sizeof(long double));

    return 0;
}