#include <stdio.h>

int main(void)
{
    int grade1 = 90, grade2 = 99; // student grades
    double gradeAvg = 0;            // student average
    gradeAvg = (double)((grade1 + grade2) / 2);
    printf("Grade is %f\n", gradeAvg);  // Note the result... how can we change it?
    return 0;
}