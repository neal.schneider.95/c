; This is the output from gcc when using the -S option
; Notice that the compiler uses ; for commenting and not separating statements
; Statements here must be on different lines


	.file	"hello_echo.c"
	.text
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
LC0:
	.ascii "Hello World!\0"
LC1:
	.ascii "Greetings, fellas\0"
LC2:
	.ascii "%s\0"
	.text
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB13:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	andl	$-16, %esp
	subl	$1024, %esp
	.cfi_offset 7, -12
	call	___main
	movl	$LC0, (%esp)
	call	_puts
	movl	$1632837705, 24(%esp)
	movl	$1734418553, 28(%esp)
	movl	$745433441, 32(%esp)
	movl	$1818576928, 36(%esp)
	movl	$774795116, 40(%esp)
	movl	$663854, 44(%esp)
	leal	48(%esp), %edx
	movl	$0, %eax
	movl	$244, %ecx
	movl	%edx, %edi
	rep stosl
	movl	$LC1, 4(%esp)
	movl	$LC2, (%esp)
	call	_printf
	movl	$0, %eax
	movl	-4(%ebp), %edi
	leave
	.cfi_restore 5
	.cfi_restore 7
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE13:
	.ident	"GCC: (i686-posix-dwarf-rev0, Built by MinGW-W64 project) 8.1.0"
	.def	_puts;	.scl	2;	.type	32;	.endef
	.def	_printf;	.scl	2;	.type	32;	.endef
