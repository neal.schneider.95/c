#include <stdio.h>

int main(void)
{
       int integer = 1;
       float singlePrecision = 2.2;
       double doublePrecision = 3.3;
       char singleChar = '$';
       char singleChar2 = 33;

       printf("your integer is %d, (size %ld)\n", integer, sizeof(int));
       printf("your float is %f , (size %ld)\n", singlePrecision, sizeof(singlePrecision));
       printf("your double is %lf, (size %ld) \n", doublePrecision, sizeof(doublePrecision));
       printf("your first char is %c, (size %ld) \n", singleChar, sizeof(singleChar));
       printf("your second char is %c, (size %ld) \n", singleChar2, sizeof(singleChar));

       return 0;
}