#define TIMES(a,b) a * b

#include <stdio.h>

void main () {
    int x = TIMES(4, 4);
    printf("%d\n", x);

    x = TIMES(10 + 10, 3);
    printf("%d\n", x);    
}