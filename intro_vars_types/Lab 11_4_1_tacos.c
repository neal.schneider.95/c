/*ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Preprocessor/performance_labs/lab24.html
PREPROCESSOR DIRECTIVES
Make a program using preprocessor directives that:
    1. Has a BUFFER_SIZE of 64
    2. Has a macro VICTORY that prints a victory message then exits the program
Then, inside of main:
    1. Declare a string with a size of BUFFER_SIZE
    2. Take user input as a string
    3. If the user's string is "Tacos", then use the VICTORY macro
*/

#include <stdio.h>
#include <string.h>

#define BUFFER_SIZE 64

#define VICTORY()\
printf("The world is at peace...");\
return(0);\

int main (){
    char s[BUFFER_SIZE];
    int i ;
    printf("All's good if you give me tacos...");
    scanf ("%d", s);
    if (strcmp(s, "tacos") == 0) {
        VICTORY();
    }
    else {
        printf ("I didn't get tacos!");
    }
}