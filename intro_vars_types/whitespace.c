#include <stdio.h>

void main (){
    printf("          1         2         3 \n");
    printf("0123456789012345678901234567890123456\n");
    printf("t0\tt1\tt2\tt3\n\n");
    printf("t0-hi\tt1-bye\tt2-word\tt3-word\n\n");
    printf("vtab0\vvtab1\vvtab2\n\n");
    printf("ret0__ret0\rcat1\rret2\n\n");
    printf("ff\fff1\fff2\n\n");
    printf("nl0\nnl1\nnl2\n\n");

    // char s[] = {"a\aa\n"};
    // for (char i = 'a'; i <= 'z'; i++){
    //     s[0] = s[2] = s[3] = i;
    //     printf("%s", s);
    // }
    printf("\n");
}
