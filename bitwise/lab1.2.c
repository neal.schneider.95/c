// Ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Bitwise_operators/performance_labs/Perf_labs.html
// 1. Write a program to shift the entered number by three bits left and display the result.
// 2. Write a program to shift the entered number by five bits right and display the result
// 3. Write a program to mask the most significant digit of the entered number. Use AND operator.
// 4. Write a program to enter two numbers and find the smallest out of them. Use conditional operator.

#include <stdio.h>

float main (){
    int i;
    printf ("Enter a number to shift right 5 bits: ");
    scanf("%d", &i);
    printf ("shifted r 5: %d, in hex: %x\n", i>>5, i>>5);
    return 2.3;
}