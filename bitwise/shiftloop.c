#include <stdio.h>

int main (){
    int x = 1;
    char y =1;
    printf("1.Shifting left **INT** ....");
    for (int i=0; i<34; i++){
        printf ("%x\n", x);
        x = x <<1;
    }

    printf("2. Now shifting right...");
    x = 1<<30;
    for (int i=0; i<34; i++){
        printf ("%x\n", x);
        x = x >> 1;
    }

    printf("3. Now shifting right again, starting with (mostly) full bits...");
    x = 0xF0F0F0F0;
    for (int i=0; i<34; i++){
        printf ("%x\n", x);
        x = x>>1;
    }

    printf("4. Shifting left ****CHAR** ....");
    for (char i=1; i<10; i++){
        printf ("%x\n", y);
        y = y << 1;
    }

    printf("5. Now shifting right...");
    y = 0xF0;
    for (char i=0; i<10; i++){
        printf ("%x\n", y);
        y = y >> 1;
    }

    printf("6. Now shifting right again, starting with (mostly) full bits...");
    y = 0x0F;
    for (char i=0; i<10; i++){
        printf ("%x\n", y);
        y = y >> 1;
    }

    printf("7.  Shifting left **CHAR with casting** ....");
    y = 1;
    for (char i=1; i<10; i++){
        printf ("%x\n", y);
        y = y<<1;
    }

    printf("8. Now shifting right...");
    y = 0xF0;
    for (char i=0; i<10; i++){
        printf ("%x\n", y);
        y= y>>1;
    }

    printf("9. Now shifting right again, starting with (mostly) full bits...");
    y = 0xF0;
    for (char i=0; i<10; i++){
        printf ("%x\n", y);
        y = y>>1;
    } 

}