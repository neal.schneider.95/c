// Ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Bitwise_operators/performance_labs/Perf_labs.html
// 1. Write a program to shift the entered number by three bits left and display the result.
// 2. Write a program to shift the entered number by five bits right and display the result
// 3. Write a program to mask the most significant digit of the entered number. Use AND operator.
// 4. Write a program to enter two numbers and find the smallest out of them. Use conditional operator.

#include <stdio.h>

float main (){
    int i, mask = 0x8000;
    printf ("Enter a number to mask: ");
    scanf("%d", &i);
    printf ("masked: %d, in hex: %x\n", i & mask , i & mask);
    printf("sizeof(int) = %lu, sizeof(__uint16_t)= %lu\n", sizeof(int), sizeof(__uint16_t));
    return 2.3;
}