#include <stdio.h>
void printBinary(int n)
{
    if (n > 1)
        printBinary(n >> 1);
    printf("%d", n & 1);
}

void main () {
    int a = 0b1010;
    int b = 0b1100;
    int shift = 5;
    int or_result = a | b;
    int and_result = a & b;
    int xor_result = a ^ b;
    int ls_result = a << shift;
    int rs_result = a >> shift;


    printf("%d, %X, ",a,a);
    printBinary(a);
    printf("\n");

    printf("%d, %X, ",or_result,or_result);
    printBinary(or_result);
    printf("\n");

    printf("%d, %X, ",and_result, and_result);
    printBinary(and_result);
    printf("\n");

    printf("%d, %X, ",xor_result, xor_result);
    printBinary(xor_result);
    printf("\n");

    printf("%d, %X, ",ls_result, ls_result);
    printBinary(ls_result);
    printf("\n");

    printf("%d, %X, ",rs_result, rs_result);
    printBinary(rs_result);
    printf("\n");

}