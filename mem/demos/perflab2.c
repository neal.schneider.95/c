#include <stdlib.h>
#include <string.h>
#include <stdio.h>
/*
PERFORMANCE LAB 1
Follow instructions below!
The basis of this lab is simple... allocate a section of memory that will
hold a string (your first name). Print the name out, then cleanup the memory and exit.
*/

//TODO: Include needed headers


int main(void)
{
    //TODO: Create a string containing your first name
    char s[] = "Neal";

    //TODO: Get the size of this string
    int ssize = strlen(s);
    //TODO: Declare a char pointer *str
    char *new_str;


    //TODO: Allocate a section of memory of type char
    //TODO: Set the size of this allocated space to 40 bytes
    //TODO: Asign the address of this allocated space to the pointer value

    new_str = (char*)calloc (40, sizeof(char));


    //TODO: Copy your name into the allocated space using strcpy()
    strcpy (new_str, s);

    //TODO: Print out your name that is stored in the allocated memory space
    printf("%s, (%ld chars)\n", new_str, sizeof(new_str));
    //TODO: Reallocate the memory space using the size of the string rather than 40 bytes
    new_str = realloc(new_str, ssize+1);
    //TODO: Print out the string again
    printf("%s, (%ld chars)\n", new_str, sizeof(new_str));
}