#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main (void) 
{
    float *float_ptr; // Pointer to the array... size TBD

    float_ptr = (float*) calloc(10, sizeof(float));

    if (float_ptr==NULL)
    {
        printf("Error: insufficient memory");
        return 1;
    }

    for (int i = 0; i<10; i++)
    {
        *(float_ptr + i) = sqrt (10*i);
    }

    for (int i = 0; i<10; i++)
    {
        printf ("sqrt (%d) =  %f\n", i*10, float_ptr[i]);
    }

    // clear the memory before returning if concerned about contents
    for (int i = 0; i<10; i++)
    {
        float_ptr[i] = 0.0f;
    }

    free(float_ptr);

    float_ptr = NULL;
}