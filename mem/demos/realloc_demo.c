#include <stdio.h>
#include <stdlib.h>
#include <math.h>  // will need to be compiled with -lm

int main (void) 
{
    float *float_ptr; // Pointer to the array... size TBD

    //float_ptr = (float*) calloc(10, sizeof(float));

    float_ptr = (float*) calloc(10, sizeof(float));

    if (float_ptr==NULL)
    {
        printf("Error: insufficient memory");
        return 1;
    }

    for (int i = 0; i<10; i++)
    {
        float_ptr[i] = sqrt (i);
    }

    for (int i = 0; i<10; i++)
    {
        printf ("sqrt (%d) =  %f\n", i, float_ptr[i]);
    }

    // change the size from 10 to 100
    // using realloc...
    float_ptr = (float*)realloc(float_ptr, sizeof(float)*100);
    if (float_ptr==NULL)
    {
        printf("Error: insufficient memory");

        return 1;
    }

    printf("reallocated to 100 instead of 10...\n");
    for (int i = 0; i<100; i++)
    {
        float_ptr[i] = sqrt (i);
    }

    for (int i = 0; i<100; i+=5)
    {
        printf ("sqrt (%d) =  %f\n", i, float_ptr[i]);
    }

    // clear the memory before returning if concerned about contents
    for (int i = 0; i<100; i++)
    {
        float_ptr[i] = 0.0f;
    }

    free(float_ptr);

    float_ptr = NULL;
}