/*
Haystack Needle Lab

Purpose:
Remove the needle from the haystack
AKA: Move the substring from the string using memory operators and functions

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int haystack_needle(char *haystack, char *needle);

int main()
{
  char string[] = "This is my brand new sentence";
  char substring[] = " brand new";

  haystack_needle(string, substring);

  // TODO: Print out the modified string
  printf("%s\n", string);

  return 0;

}

int haystack_needle(char *haystack, char *needle)
{
    // TODO: Create a char buffer named *buffer using malloc \
    ensure it is large enough to hold string[] + a nul-terminator
    char *buffer = malloc(strlen(haystack)+ 1);

    // TODO: Ensure buffer was created
    if (!buffer)
    {
        return -1;
    }

    int needleLength = strlen(needle);

    // TODO: Using strstr() find the needle in the haystack (substring in string)\
    the pointer that is returned is pointing to the substring within the string \
    Set that pointer to a new char pointer called *mark_position
    char *mark_position = strstr(haystack, needle);
    // TODO: Check to see if the needle is in the haystack and return error if not
    if (!mark_position) 
    {
        return -1;
    }

    // TODO: Using strcpy(), store the end of the haystack into the buffer... \
    you are not authorized to use the variable needle.
    strcpy(buffer, mark_position + needleLength);

    // TODO: Using memmove, overwrite the haystack (string)... effectively removing the needle \
    // HINT: start at the mark_position... that is after all where we need to start replacing.
    memmove(mark_position, buffer, strlen(buffer) + 1);

    // TODO: Give back the memory from buffer
    free(buffer);

    return 0;
}