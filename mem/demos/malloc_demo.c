#include <stdio.h>
#include <stdlib.h>

int main (void) 
{
    int *integer_ptr; 
    integer_ptr = (int*) malloc(sizeof(int));

    if (integer_ptr==NULL)
    {
        printf("Error: insufficient memory");
        return 1;
    }

    *integer_ptr = 999;
    printf ("Value is: %d\n", *integer_ptr);

    // clear the memory before returning if concerned about contents
    *integer_ptr = 0;
    free(NULL);
    free(integer_ptr);

    integer_ptr = NULL;
}