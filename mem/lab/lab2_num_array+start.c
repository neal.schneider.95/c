#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main () {
    int start;
    int stop;
    int powers;
    int *array;
    int val;
    int rows;

    printf("1: Enter start, stop and number of powers? ");
    scanf("%d %d %d", &start, &stop, &powers);
    if (stop >= start && powers > 0) {
        rows = stop - start + 1;
        printf("start: %d, stop: %d, powers: %d, rows: %d\n", start, stop, powers, rows);
        array = calloc(rows * (powers + 1), sizeof(int));
        for (int row = 0; row <= rows; row++) {
            for (int col = 0; col <= powers; col++) {
                val = start + row;
                for (int p = 0; p < col; p++) {
                    val = val * (start + row);
                }
                *(array + col + (row * powers)) = val;
            }
        }
        for (int row = 0; row <= rows; row++) {
            printf("\n%d: ", *(array + row * powers));
            for (int col = 1; col <= powers; col++) {
                printf("%5d ", *(array + col + (row * powers)));
            }
        }
        free(array);
    } else {
        printf("Stop must be at least as big as start, and powers greater than 0.\n");
    }
}