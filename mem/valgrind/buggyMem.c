#include <stdlib.h>
#include <stdio.h>

int * f(void)
{
    int* x = (int*)malloc(10 * sizeof(int));
    for (int i = 0; i<=10; i++) {
        printf("the %d element: %d\n",i, x[10]);
    }
    x[9] = 20;        // problem 1: heap block overrun
    return x;
}                    // problem 2: memory leak -- x not freed

int main(void)
{
    int *x_array = f();
    free(x_array);
    return 0;
}