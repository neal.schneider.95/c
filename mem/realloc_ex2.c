#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void main() {

int *array_ptr = calloc(10, sizeof *array_ptr);        // Allocate/zeroize 10 shorts
if (array_ptr == NULL) {                               // Check for failure
    fprintf(stderr, "calloc failed.\n");      
    exit(EXIT_FAILURE);
}
array_ptr[0] = 2001;                                    // Assign some values
array_ptr[1] = 42;
array_ptr[2] = 1 << 8;
for (int i = 0; i < 10; i++){                           // Confirmation print
    printf("%d ", *(array_ptr + i));
}
printf("\n");
int *new_ptr = realloc(array_ptr, 5 * sizeof *new_ptr); // Realocates for 5 ints
if (new_ptr == NULL) {                                  // Check for failure
    fprintf(stderr, "realloc failed.\n");
    exit(EXIT_FAILURE);
}
for (int i = 0; i < 5; i++){                            // Confirmation print
    printf("%d ", *(new_ptr + i));
}
printf("\n");
free(new_ptr);                  // You do not have to free the original array_ptr
                  
}