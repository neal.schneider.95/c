#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main() {
    int *i_ptr = malloc(sizeof *i_ptr);         // Allocate memory for 1 int
    if (i_ptr == NULL) {                        // Check for failure
        fprintf(stderr, "malloc failed.\n");      
        exit(EXIT_FAILURE);
    }
    *i_ptr = 2001;                              // Assigns value into that memory
    printf("%d\n", *i_ptr);                     // Print confirms value assigned

    char *s_ptr = malloc(6 * sizeof(*s_ptr));   // Allocates memory for 6 chars
    if (s_ptr == NULL) {                        // Check for failure
        fprintf(stderr, "malloc failed.\n");     
        exit(EXIT_FAILURE);
    }
    strcpy(s_ptr, "Hello");                     // Assignment
    printf("%s\n", s_ptr);                      // Confirmation

    printf("ptr size: %ld\n", sizeof(i_ptr));
}