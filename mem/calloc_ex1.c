#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main() {
    double *d_ptr = calloc(1, sizeof *d_ptr); // Allocate/zeroize 1 double
    if (d_ptr == NULL) {                      // Check for failure
        fprintf(stderr, "calloc failed.\n");      
        exit(EXIT_FAILURE);
    }
    *d_ptr = 42.2001;                         // Assigns a value into that memory
    printf("%lf\n", *d_ptr);                  // Print confirms value assigned

    char *s_ptr = calloc(6, sizeof *s_ptr);   // 6 chars
    if (s_ptr == NULL) {                      // Check for failure
        fprintf(stderr, "calloc failed.\n");
        exit(EXIT_FAILURE);
    }
    strcpy(s_ptr, "Hello");                    // Assignment
    printf("%s\n", s_ptr);                     // Confirmation print
}
