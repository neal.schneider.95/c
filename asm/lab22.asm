; Assignment 2.2
; 2. Write a program that computes the final percentage grade of a student in a Computer Architecture class based on the following scores. The result should be a whole number (e.g., 75 to represent 75%).

; The student took four exams (points earned/points possible).
; 25/30
; 89/100
; 49/50
; 80/150

global _start


_start:
	; print message
	mov ax, [amps]   ; load amps
	mov bx, [volts]	; load volts 
	mul bx 
    nop 
    nop
    nop
	
section .data
	grade1: db equ (100*25/30)
	grade2: db equ (100*89/100)
	grade3: db equ (100*49/50)
	grade4: db equ (100*80/150)
