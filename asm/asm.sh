#!/bin/bash

echo '[+] Assembling with Nasm ... '
nasm -f elf $1.asm

echo '[+] Linking ...'
ld -m elf_i386 $1.o -o $1 

echo '[+] Done!'
