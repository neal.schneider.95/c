; hellowworld.asm
; First program

global _start

section .text

_start:
	; print hello world!
	mov eax, 0x4 		; load call (write)
	mov ebx, 0x1		; to STDOUT 
	mov ecx, message	; message
	mov edx, mlen 		; length
	int 0x80		
	
;	mov eax, 0x1 		; load exit call
;	mov ebx, 0x5		; exit status
;	int 0x80c
	

section .data
	message: db "Hello World!", 10, "Second Line", 10
	mlen equ $-message

