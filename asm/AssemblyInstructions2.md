
Literal value (immediate)  -> 42, "Hello World"

memory operand(variable) -> numOfStudents, grade_one

Registers -> eax, ebx, ...etc 

# Instructions

mov - moving data 

mov eax, 5 ; moving literal 5 into eax register

xchg -> performs a swap in a single instruction

Nasm xchg syntax

xchg size M/R, M/R  ; Memory to memory not allowed. It has to be a combo of M and R 

xchg DWORD  [sum], edx 


INC and DEC

NASM syntax

inc size M/R 

inc dword [sum]
dec eax 

ADD and SUB

 Nasm syntax
add SIZE M/R, L/M/R  
add word [sum],50
sub eax, [val]

NEG -> gives you the reverse sign of a value into its Two's complement. 

neg db [sum]

NASM example code 

section .data

sum: dd 0
val: dd 25

section .text

global _start

_start:

mov eax, 0
inc eax         ; eax is now 1
add eax, 200    ; eax is now 201
sub eax, [val]  ; eax is now 176
mov [sum], eax  ; sum is now 176
dec dd [sum]    ; sum is now 175  ; test to see if dd is necessary for dec/inc 
neg dd [sum]    ; sum is now -175

mov eax, 1
mov ebx, 0
int 80h


MUL -> multiplication 
it performs unsigned integer multiplication

mul <multiplier>

Accumulator register 

                    Multiplicand        Product
8bit number         al                  ax
16bit number        ax                  dx:ax
32bit number        eax                 edx:eax
64bit number        rax                 rdx:rax


8096 x 64 = product 518144

mov ax, 64   ;store the multiplicand in 16 bit location
mov bx, 8096 ; store multiplier  in 16 bit location 
             ; ax is used as the multiplicand and this produces 32 bit result
mul bx      ; bx * ax = 518144
                      = 00000000000001111110100000000000
                      dx=0000000000000111, ax=  1110100000000000



IMUL -> signed multiplication 

imul size M/R     ; imul dd [sum]
imul  R, L/M/R  ; imul ax, [val]
imul  R, M/R, L ; imul eax, [val], 10


DIV -> unsigned division

Dividend            quotient            remainder
ax                  al                  ah
dx:ax               ax                  dx
edx:eax             eax                 edx
rdx:rax             rax                 rdx

mov  edx, 0  ;load edx:eax with dividend, 32
mov eax, 32  ; 
mov ecx, 3   ; load ecx with the divisor , 3
div ecx       ; 32 / 3 = 10r2
                ; eax = Ah  , edx = 2h




BIT shifting 

SHL -> left shift
SHR -> right shift

shl size M/R, L  ; shl dd [val], 2         
shr size M/R, L  ; shr ebx, 1              


01000000 = 64  shift left 2
00000000 = 0 is the result 



















