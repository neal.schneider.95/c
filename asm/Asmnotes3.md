

LEA (Load Effective Address)
loads an operand's address into the destination

lea syntax

lea R, [M]

ARRAYS
byte array -> a value at every byte
DWORD array -> a value at every four bytes

Nasm syntax

exampleZ: DD 2,4,6,8      ; len = 16 

arrayZ: db 2, 4, 6, 8 
len: equ ($ - arrayA)  ; len = 4

mov R, [M + constant] 

mov eax, [arrayZ + 1]  ; eax = 4 


arrayX: DD FFFFF, FFFFE, FFFFD, FFFFC

MOV SIZE [M+CONSTANT] , L/R  

store the value 10 into item 2 of arrayx, replace replace FFFFD

mov dd [arrayX + 8], 10  ; arrayX = 0xFFFFF, 0xFFFFE, 0xA, 0xFFFFC









