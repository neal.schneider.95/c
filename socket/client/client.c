#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define SERVER_IP "127.0.0.1" // Change this to the server's IP address
// #define SERVER_IP "192.168.145.128" // Change this to the server's IP address
#define SERVER_PORT 12345
#define BUFFER_SIZE 1024

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <file_to_send>\n", argv[0]);
        exit(1);
    }

    const char *file_to_send = argv[1];
    FILE *file = fopen(file_to_send, "rb");
    if (file == NULL) {
        perror("Error opening file");
        exit(1);
    }

    int client_socket;
    struct sockaddr_in server_addr;

    // Create a socket
    client_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket == -1) {
        perror("Error creating socket");
        fclose(file);
        exit(1);
    }

    // Set up the server address struct
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
    server_addr.sin_port = htons(SERVER_PORT);

    // Connect to the server
    if (connect(client_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        perror("Error connecting to server");
        fclose(file);
        close(client_socket);
        exit(1);
    }

    // Send the filename to the server
    send(client_socket, file_to_send, strlen(file_to_send) + 1, 0);

    char buffer[BUFFER_SIZE];
    size_t bytes_read;

    // Send the file contents to the server
    while ((bytes_read = fread(buffer, 1, sizeof(buffer), file)) > 0) {
        if (send(client_socket, buffer, bytes_read, 0) == -1) {
            perror("Error sending file");
            fclose(file);
            close(client_socket);
            exit(1);
        }
    }

    fclose(file);
    close(client_socket);

    printf("File '%s' sent to server %s:%d\n", file_to_send, SERVER_IP, SERVER_PORT);

    return 0;
}
