#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 12345
#define BUFFER_SIZE 1024

int main() {
    int server_socket, client_socket;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_addr_len = sizeof(client_addr);

    // Create a socket
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket == -1) {
        perror("Error creating socket");
        exit(1);
    }

    // Set up the server address struct
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(PORT);

    // Bind the socket to the server address
    if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        perror("Error binding socket");
        close(server_socket);
        exit(1);
    }

    // Listen for incoming connections
    if (listen(server_socket, 5) == -1) {
        perror("Error listening for connections");
        close(server_socket);
        exit(1);
    }

    printf("Server is listening on port %d...\n", PORT);

    while (1) {
        // Accept a client connection
        client_socket = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_len);
        if (client_socket == -1) {
            perror("Error accepting client connection");
            continue;
        }

        printf("Accepted connection from %s:%d\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

        // Receive and save the file
        char buffer[BUFFER_SIZE];
        FILE *file;
        ssize_t bytes_received;

        char filename[256];
        bytes_received = recv(client_socket, filename, sizeof(filename), 0);
        if (bytes_received < 0) {
            perror("Error receiving filename");
            close(client_socket);
            continue;
        } else {
            printf("Filename: %s (len:%ld, rx: %ld)\n", filename, strlen(filename), bytes_received);
        }

        filename[bytes_received] = '\0';
        file = fopen(filename, "wb");
        if (file == NULL) {
            perror("Error opening file for writing");
            close(client_socket);
            continue;
        }

        while (1) {
            bytes_received = recv(client_socket, buffer, sizeof(buffer), 0);
            if (bytes_received <= 0)
                break;
            fwrite(buffer, 1, bytes_received, file);
        }

        fclose(file);
        printf("Received file '%s' from %s:%d\n", filename, inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
        close(client_socket);
    }

    close(server_socket);

    return 0;
}
