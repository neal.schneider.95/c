/*
// Ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Labs/Performance/string-io-perf.html

Read a string from stdin into a char array of dimension 10
Write that string to stdout
Use fgets() and fputs()
Test it with the following input:

- Program
- Class rules
- <Enter>
- 1<tab>2<space>3456789
- |$(5)$|
- ~\_<Ctrl-D>_/~

// Neal Schneider
// 22 Sep 21
*/

#include <stdio.h>

int main(){
    char chars[10];

    fgets (chars, 10, stdin);
    fputs (chars, stdout);
}