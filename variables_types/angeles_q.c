#include <stdio.h>

// Global Const Buffer Size
const int BUFF = 2;

int main(void) {
    char my_input[BUFF] = "A";
    printf("Enter string input: ");
    fgets(my_input, sizeof(my_input) + 20, stdin);
    printf("You entered: ");
    fputs(my_input, stdout);
    fputs("\n", stdout);
    return 0;
}