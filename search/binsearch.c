// 1. Write a C program that implements a binary search algorithm.

// Example output:

// Array: 5 10 15 20 25 26 34 56 77

// Please enter the searched key: 26
// The key 26 was found at 5 ( Starting from 0)
// ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Data_Structures/Searching/performance_labs.html#2-search-the-file-numberstxt--that-has-a-sorted-2d-array-create-a-program-to-search-for-a-number-from-the-file-and-also-display-something-if-the-number-is-not-found

#include <stdio.h>

int binsearch(int* arr, int end, int search_item){
    int start = 0;
    int mid;
    int found = 0;

    while (start != end){
        mid = (start + end) / 2;
        if (arr[mid] == search_item) {
            found = 1;
            break;
        }
        if (arr[mid] < search_item) {
            end = mid;
        } else {
            start = mid;
        }
    }
}

void main() {
    int array[] = {5, 10, 15, 20, 25, 26, 34, 56, 77};
    int search_item;

    printf("Please enter the searched key: ");
    scanf("%d", search_item);
}