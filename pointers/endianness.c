#include <stdio.h>
union sharedData {
    long long int llinteger;
    unsigned int uinteger; 
    unsigned char byte [8];
} datablock;

void printCharBlock(union sharedData d, char *s){
    printf("%s:\n %x ", s);
    for (int i = 0; i<8; i++) {
        printf("%x ", d.byte[i]);
    }   
    printf("\n");
}

int main() {
    printf("sizeof lli: %d\n",sizeof(long long int));
    union sharedData data;
    printCharBlock(data, "Uninitialized");
    data.llinteger = 0x123456789ff00ff;
    printCharBlock(data, "lli 1234...");
    data.llinteger = 0;
    printCharBlock(data, "zeroized");
    data.uinteger = 0xABCD1234;
    printCharBlock(data, "uint");
}