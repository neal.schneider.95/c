#include <stdio.h>
typedef struct{
    char point_name[28];
    float entropy;
} My_struct;

void main() {
    My_struct struct_array[5] = {};
    printf("Struct Size: %ld\n", sizeof(My_struct));
    for (int i = 0; i<5; i++) {
        printf("struct_array[%d] is at %p\n", i, &struct_array + i );
    }
    // (*struct_array).i = 42;
    // printf("%d\n", struct_array[0].i);
    // (*(struct_array+2)).x = 3.14;
    // printf("%f\n", struct_array[2].x);

}