// Complete Example 2: Split string
#include <stdio.h>

char *split(char *input_str, int n);                    // Original function prototype
char *(*split_fn_ptr) (char *phrase, int x) = &split;   // Concurrent definition

int main() {
    char my_str[] = "This phrase is a little be too long!";
    int break_at = 17;                      // Where to do the split
    char *tail;                             // String to capture the results

    tail = split_fn_ptr(my_str, break_at);  // Call using function pointer
    printf("Phrase: %s\n Split: %s\n", my_str, tail);
}

char *split(char *input_str, int n) {       // split definition
    return input_str + n;                   // Using pointer math to get get split location
}