
//*************pass by value example***************
// #include <stdio.h>
// void swap_nums(int a, int b){
//     int tmp = a;
//     a = b;
//     b = tmp;
// }

// int main(){
//     int x = 4; int y = 7;

//     printf("x= %d, y= %d\n", x, y);
//     swap_nums(x, y);
//     printf("x= %d, y= %d\n", x, y);
//     // What are the values of x and y now?
//     return 0;
// }

///***********************passby reference*****************

// #include <stdio.h>
// void swap_nums(int *a, int *b){
//     //derefrencing the variables and changing the contents at the address
//     int tmp = *a; 
//     *a = *b;
//     *b = tmp;
// }
// int main(){
//     int x = 4; int y = 7;
//     printf("x= %d, y= %d\n", x, y);
//     swap_nums(&x, &y); //sending the address of the variables
//     // What are the values of x and y now?
//     printf("x= %d, y= %d\n", x, y);
//     return 0;
// }

//****************array passbyrefrence*************


// #include <stdio.h>
// void change_index2(int somearray[]){
//     //derefrencing the variables and changing the contents at the address
//     int *ptr = somearray;
    // printf("This is the ptr: %p", ptr);
    // printf("This is the value at the ptr: %d", *ptr);
//     *(ptr + 2) = 42;
// }

// int main(){
//     int nums[] = {4,5,7,2,11,12};
//     printf("This is index 2: %d\n", nums[2]);
//     change_index2(nums); //sending the address of the variables
//     // What are the values of x and y now?
//     printf("This is index 2: %d\n", nums[2]);
//     return 0;
// }

//////passing arrays with size /////////////////

#include <stdio.h>

#define MAX 10

/* function declaration */
double getAverage(int arr[], int size);

int main () {

    int numGrades;
    /* an int array with MAX elements */
    printf("Enter the amount of grades to average: ");
    scanf("%d",&numGrades);
    int grades[MAX];
    double avg;

    // Check to make sure numGrades is not larger than MAX
    if ( numGrades > MAX)
    {
        printf("Number of grades is invalid: Enter number =< than 10\n");
        return 0;
    }

    // Get the grades using scanf()
    printf("Enter %d Grades:\n", numGrades);
    for(int i = 0; i < numGrades; i++)
    {
        scanf("%d",&grades[i]);
    }

    printf("%p is the starting address of the grades array\n", grades);

    /* pass pointer to the array as an argument */
    // pass the array
    avg = getAverage( grades, numGrades ) ;
 
    /* output the returned value */

    printf( "Average value is: %f \n", avg );
    printf("first grade is %d\n", grades[0]);
    return 0;
}

double getAverage(int arr[], int size) {

    int i;
    double avg;
    double sum = 0;

    printf("%p is the starting address of the grades array\n", arr);

    //add 10 to the first grade
    arr[0]+=10;

    for (i = 0; i < size; ++i) {
        sum += arr[i];
    }

    avg = sum / size;

    return avg;
}