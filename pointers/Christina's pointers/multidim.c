// #define ROWS 6
// #define COLUMNS 7

// int main()
// {
//     int numberArray[ROWS][COLUMNS] = { 0 };
//     int i = 0;    // Iterate through rows
//     int j = 0;    // Iterate through columns

//     for (i = 0; i < ROWS; i++)
//     {
//         for (j = 0; j < COLUMNS; j++)  
//         {
//             numberArray[i][j] = (i * 10) + j;

//         }
//     }

// }

//*****************example2*************************

#include <stdio.h>

#define ROW 4
#define COL 6

int main()
{
    // Initialize iterators
    int i;
    int j; 

    // Initialize multi-dimensional array dbaseTable1[4][6]
    double dbaseTable1[ROW][COL] = {
            { 1.1, 1.2, 1.3, 1.4, 1.5, 1.6},
            { 2.1, 2.2, 2.3, 2.4, 2.5, 2.6},
            { 3.1, 3.2, 3.3, 3.4, 3.5, 3.6},
            { 4.1, 4.2, 4.3, 4.4, 4.5, 4.6}
        };

    // Iterate through the first dimension of the array [ROW]
    for (i = 0; i<ROW; i++)
    {
        // Iterate through the second dimension of the array [COL]
        for (j = 0; j<COL; j++)
        {
            printf("%.1f\t", dbaseTable1[i][j]);
        }
        printf("\n");
    }

    return 0;
}