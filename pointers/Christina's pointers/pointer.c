#include <stdio.h>

int main() {
  

  int nine = 9;    // Integer variable "nine" defined as 9
  printf("The variable nine address is %p and it's value is %d\n", &nine, nine);

  int zero = 0;    // Integer variable "zero" defined as 0
  printf("The variable zero addres is %p and it's value is %d\n", &zero, zero);

  int *int_ptr = nine;   // Pointer variable for an integer declared and assigend to address of nine
  printf("The variable int_ptr addres is %p, its value is %p and it dereferenced is %d\n", &int_ptr, int_ptr, *int_ptr);


    printf("Reassining the zero variable to that of the value in the int_ptr\n");
    zero = *int_ptr; // "zero" assigned value at int_ptr address
    //   /* "zero", now 9, should now equal "nine" */

    printf("The address for the int_ptr is %p and the address for zero is %p the value of zero is now %d and the value of the int_ptr is %p\n", &int_ptr, &zero, zero, int_ptr);
    //   printf("The value for the zero variable is now %d\n", zero);

    printf("Now reassigning the value of the int_ptr to be 1\n");
    *int_ptr = 1;    // Value at int_ptr address assigned 1
    //   /* "zero" is now equal to 1 */
    printf("The address of the int_ptr is %p and the value of the int_ptr is %p and the int_ptr dereferenced is %d\n", &int_ptr, int_ptr, *int_ptr);
    //   printf("The int_ptr is now assigned the the address assigned to 1: %p\n", int_ptr);

    printf("The variable nine address is %p and it's value is %d\n", &nine, nine);
    printf("The variable zero addres is %p and it's value is %d\n", &zero, zero);


  return 0;
}