#include <stdio.h>
#include <stdlib.h>

// Comparison function for qsort
int compare_floats(const void* a, const void* b) {
    float float_a = *((const float*)a);
    float float_b = *((const float*)b);

    if (float_a < float_b)
        return -1;
    else if (float_a > float_b)
        return 1;
    else
        return 0;
}

int main() {
    float arr[10] = {267.20, 373.67, 568.75, 830.86, 419.81, 456.77, 241.11, 185.77, 293.35, 252.87};

    printf("Unsorted array:\n");
    for (int i = 0; i < 10; i++) {
        printf("%.2f ", arr[i]);
    }

    // Using qsort to sort the array
    qsort(arr, 10, sizeof(float), &compare_floats);

    printf("\n\nSorted array:\n");
    for (int i = 0; i < 10; i++) {
        printf("%.2f ", arr[i]);
    }
    printf("\n");

    return 0;
}
