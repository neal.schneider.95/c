#include <stdio.h>

#define BOARD_SIZE 5

// Function prototypes
void moveUp(int *x, int *y);
void moveDown(int *x, int *y);
void moveLeft(int *x, int *y);
void moveRight(int *x, int *y);

/* Function pointer array definition
   ╭── Return type
╭──┤   ╭───────────────┬─ Function pointer array's name  */
void (*movementFunctions[])(int*, int*) = {moveUp, moveDown, moveLeft, moveRight};
/*    │                 ╰┤  ╰┬───────╯     ╰─┬─────────────────────────────────╯
      │                  │   │               ╰─ Functions that populate the array
      │                  │   ╰─ Parameters: two int pointers to allow modification
      │                  ╰── Array, implicit sizing
      ╰── Asterisk denotes that this is a function pointer  */

int main() {
    // Define game board
    char board[BOARD_SIZE][BOARD_SIZE];
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board[i][j] = '.';
        }
    }

    // Game piece starting position
    int x = BOARD_SIZE / 2;
    int y = x;
    board[x][y] = '@';

    // Game loop
    char input;
    while (1) {
        // Display game board
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                printf("%c ", board[i][j]);
            }
            printf("\n");
        }
        printf("Enter movement command (w-up, s-down, a-left, d-right, q-quit): ");
        scanf(" %c", &input);
        if (input == 'q') {
            break;
        }

        // Execute movement function based on input
        switch (input) {
            case 'w':
                movementFunctions[0](&x, &y);
                break;
            case 's':
                movementFunctions[1](&x, &y);
                break;
            case 'a':
                movementFunctions[2](&x, &y);
                break;
            case 'd':
                movementFunctions[3](&x, &y);
                break;
            default:
                printf("Invalid input. Try again.\n");
                continue;
        }

        // Update game board
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (i == x && j == y) {
                    board[i][j] = '@';
                } else {
                    board[i][j] = '.';
                }
            }
        }
    }
    printf("Thanks for playing!\n");
    return 0;
}

// Movement functions
// The first line of each checks to to stay in bounds of the board
void moveUp(int *x, int *y) {
    if (*x > 0) {
        (*x)--;
    }
}

void moveDown(int *x, int *y) {
    if (*x < BOARD_SIZE - 1) {
        (*x)++;
    }
}

void moveLeft(int *x, int *y) {
    if (*y > 0) {
        (*y)--;
    }
}

void moveRight(int *x, int *y) {
    if (*y < BOARD_SIZE - 1) {
        (*y)++;
    }
}
