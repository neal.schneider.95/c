#include <stdio.h>
#include <stdint.h>
void main(){
    uint32_t num = 0x5678ABCD;               // A 4 byte number
    printf("%x\n", num);
    uint8_t *byte_ptr = (uint8_t *)&num;    // Cast to unsigned 8 bit integers
    for (int i = 0; i<(sizeof(int)); i++) { // Loop over size of a an int
        printf("%x ", byte_ptr[i]);         // Print each byte in hex
    }
    printf("\n");
}