#include <stdio.h>
#define ROWS 6
#define COLUMNS 7


int main (){
    char board [ROWS][COLUMNS] = { ' ' };
    int i = 0;	  // Iterate through rows
    int j = 0;	 // Iterate through columns

    
    for (i = 0; i < ROWS; i++)
    {
        for (j = 0; j < COLUMNS; j++)  
        {
                if ((i+j) %3 == 0) {
                    board[i][j] = '#';
                } ;
        }
    }

    for (i = 0; i < ROWS; i++)
    {
        for (j = 0; j < COLUMNS; j++)  
        {
            printf("%4c ", board[i][j]);
        }
        printf("\n");
    }
}

