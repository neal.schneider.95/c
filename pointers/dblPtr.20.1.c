#include <stdio.h>

int main(){
    char * rule0 = "Talk about Byte Club.";
    char * rule1 = "Please talk about Byte Club.";
    char * rule2 = "If a byte is 0x0 the string is over.";
    char * byteClubRules[] = {rule0, rule1, rule2};
    /* byteClubRules is an array of character pointers */ 
    for (char i = 0; i < 3; i++)
    {
        printf("Rule #%d:\t%s\n" , i, byteClubRules[i]);
    }

// rule_ptr points to mem addresses of elements in byteClubRules

    char ** dblRulePtr = byteClubRules;

    for (int i = 0; i < 3; i++)
    {
        printf("Rule #%d:\t%s\n" , i, *dblRulePtr);
        dblRulePtr++;
    }
}
