// Declare two variables, named var1 and var2, of the same data type
// Declare a pointer variable, named var_ptr, of the same data type
// Define the first variable with an arbitrary value
// Assign var1’s memory address to var_ptr
// Define var2 by dereferencing var_ptr
// Compare var1 to var2 and print human-readable results

#include <stdio.h>

int main (){
    float var1 = 100.1, var2 = 200.2;
    float *var_ptr;
    var_ptr = &var1;
    var2= *var_ptr;
    printf ("var1 = %f, var2 = %f, ptr = %x\n", var1, var2, var_ptr);
    printf ("addresses: var1 = %x, var2 = %x", &var1, &var2);
    return 0;
}

NULL