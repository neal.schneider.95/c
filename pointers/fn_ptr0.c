#include <stdio.h>

int add (int a, int b) {
    return a + b;
}

/* Declare add_fn_ptr:
  ╭── Return type  
╭─┤   ╭────────┬─ Function pointer variable's name */
int *add_fn_ptr (int, int);
/*   │            ╰────────┴─ Parameter List
     ╰── Asterisk denotes that this is a pointer  */

int main () {
    int sum;
    add_fn_ptr = &add;
    for (int i = 0; i<4; i++){
        sum = add_fn_ptr(i, i);
        printf("%d\n", sum);
    }
}
