// CCD Pointer Lab Solution
// high-low
// Evaluates Passing an array and pointer math

#include <stdio.h>

void high_low(int *data, int* hi_low ) {
    int *hi = hi_low;
    int *lo = hi_low + 1;
    for (int i = 1; i < 10; i++) {
        // Use pointer math to access array
        // Find the highest element
        if (*hi > *(data + i)) {
            *hi = *(data + i);
        }
        // Find the lowest element
        if (*lo < *(data + i)) {
            *lo = *(data + i);
        }
    }
}

void main() {
    int data[] = {99, 101, 1776, 314, 13466917, 90, 505, 3000, 42, 8128};
    int hl[2];
    high_low(data, hl);
    // Using pointer math to locate the returned values
    printf("High = %d\n Low = %d\n", *hl, *(hl + 1));
}