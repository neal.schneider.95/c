<!-- from Doc: packages/website/docs/BasicCourse/M3-C-Programming/3005-C-Fundamentals/Pointers.md 
Consider adding at a later time in the class 
-->

## Pointer Type Casting

It's possible to change the type of a pointer in an expression.  This is most commonly done when allocating space, where the function returns a block of memory as a `void` pointer that should be specified before assigning a pointer variable to it. However, this can be done for any pointer.  When doing so, it's important to understand the structure of the data being stored down to the bit level.

To cast to a new type, enclose the type followed by * in parenthesis before the pointer or address.

Syntax:

```c
(<type> *)<address>
```

Example:

```c
uint16_t *alt_view = (uint16_t *)matrix_ptr;
char *s;
s = (char *)&alpha;
```

This could be used to view contents in different formats.  For example, this could be used to examine bytes of another block of memory.  In the program below, pointer math is used to print out the bytes of a 4 byte number.

```c {} showLineNumbers
#include <stdio.h>
#include <stdint.h>
void main(){
    uint32_t num = 0x5678ABCD;               // A 4 byte number
    printf("%x\n", num);
    uint8_t *byte_ptr = (uint8_t *)&num;    // Cast to unsigned 8 bit integers
    for (int i = 0; i<(sizeof(int)); i++) { // Loop over size of a an int
        printf("%x ", byte_ptr[i]);         // Print each byte in hex
    }
    printf("\n");
}
```

```text
5678abcd
cd ab 78 56 
```

Because linux and windows are little-endian, the first byte is the least significant, which causes it to be reversed.


<!-- removed from Slides: packages/website/slides/BasicCourse/3005-Pointers.md
Slides 
Slides
Slides -->


### Pointer Type Casting

<med_text>

- Change from `void *` to usable type
  - Allocated space from the OS
- Allows reinterpretation of data
  - Must know data to the bit level

</med_text>

Syntax:

```c
(<type> *)<address>
```

Example:

```c
uint16_t *alt_view = (uint16_t *)matrix_ptr;
char *s;
s = (char *)&alpha;
```

note:

---down---

### Example

<med_text>

```c [|6]
#include <stdio.h>
#include <stdint.h>
void main(){
    uint32_t num = 0x5678ABCD;               // A 4 byte number
    printf("%x\n", num);
    uint8_t *byte_ptr = (uint8_t *)&num;    // Cast to unsigned 8 bit integers
    for (int i = 0; i<(sizeof(int)); i++) { // Loop over size of a an int
        printf("%x ", byte_ptr[i]);         // Print each byte in hex
    }
    printf("\n");
}
```

</med_text>

```text
5678abcd
cd ab 78 56 
```

note:

- Interprets a 4 byte int as 1 byte ints
- Each pass takes a byte starting at the same address and prints each one
- Little-endian, so starts with the least significant byte first
