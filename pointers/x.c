#include <stdio.h>

int main (void)
{
     int   count = 10, x;
     int   *int_pointer;

     int_pointer = &count;
     x = *int_pointer;

     printf ("count = %i, x = %i\n", count, x);
     printf ("address of x = %p, address of count = %p.\n ", &x, &count);

     count = 200;
     x= 500;

     printf ("count = %i, x = %i\n", count, x);
     printf ("address of x = %p, address of count = %p.\n ", &x, &count);
    

     return 0;
}