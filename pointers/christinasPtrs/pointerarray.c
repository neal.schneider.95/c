//******version 1******************
// #include <stdio.h>


// //array names are constant pointers to the address of the first element
// int main()
// {
//     char rule0[] = {"Talk about Byte Club."};
//     // printf("%d", rule0); //to get type in warning
//     char rule1[] = {"Please talk about Byte Club."};
//     char rule2[] = {"If a byte is 0x0 the string is over."};
//     char * byteClubRules[] = {rule0, rule1, rule2};
//     /* byteClubRules is an array of character pointers */ 
//     for (int i = 0; i < 3; i++)
//     {
//         printf("Rule #%d:\t%s\n" , i, byteClubRules[i]);
//     }
//     return 0;

// }

//**************version 2***************************

// # include <stdio.h>

// int main()
// {

//     char * rule0 = "Talk about Byte Club.";
//     // printf("%d", rule0); //to get the type in a warning
//     char * rule1 = "Please talk about Byte Club.";
//     char * rule2 = "If a byte is 0x0 the string is over.";
//     char * byteClubRules[] = {rule0, rule1, rule2};
//     /* byteClubRules is an array of character pointers */ 
//     for (int i = 0; i < 3; i++)
//     {
//         printf("Rule #%d:\t%s\n" , i, byteClubRules[i]);
//     }
//     return 0;

// }


//********************diff datatye with function*************************

// #include <stdio.h>

// // Recieves array of grades and the size of array and averages the students grade
// float average_students_grades(float grades[], int size);

// int main(){
//     // Initialization of three Student Grades arrays       
//     float stu1Grades[5] = { 90.1, 100.0, 89.9, 78.1, 87.6};
//     float stu2Grades[5] = { 70.5, 79.3, 68.9, 3.0, 0.0};
//     float stu3Grades[5] = {100.0, 100.0, 100.0, 99.9};

//     // Initialize Array of pointers to the student grades 
//     float * classGrades[] = { stu1Grades, stu2Grades, stu3Grades};
//     /* classGrades is an array of floating point integer pointers */

//     float tempAvgGrade = 0;

//     printf("\nStudent Averages\n-------------------\n");

//     //iterate over classGrades, dereference and send each pointer element to the function to be averaged
//     for (int i = 0; i < 3; i++)
//     {  
       
//         printf("This is what is being sent to the function: %p\n", *(classGrades + i)); //the addess of the index in array
//         tempAvgGrade = average_students_grades(*(classGrades + i), 5);
//         printf("Student #%d:\t%2.1f\n", (i + 1), tempAvgGrade);
//     }
//     printf("----------------\n");
// }

// float average_students_grades(float grades[], int size)
// {
//     float sum = 0;

//     // printf("This is the grades array first element %f", grades[0]);
//     for(int i = 0; i < size; i++)
//     {
//         printf("Grade is %f\n", grades[i]);
//         sum += grades[i];
//     }

//     return sum/size;
// }


//*******************with puts no function************

#include <stdio.h>

int main()
{    	
    // Initialize iterator i
    int i = 0;

    // Initialize 3 char arrays of initials
    char initials0[] = { 'j', 'e', 'h', 0x0 };
    char initials1[] = { 'm', 'a', 't', 0x0 };
    char initials2[] = { 'c', 's', 'n', 0x0 };

    // Initialize Array of pointers to our character arrays
    char * arrayOfInits[] = { initials0, initials1, initials2};
    
    // Iterate through arrayOfInits and puts() each char array (string)
    for (i = 0; i < 3; i++)
    {
        puts(*(arrayOfInits+i));
    }
}