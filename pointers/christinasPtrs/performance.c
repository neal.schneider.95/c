// Declare userInput, tempValue, and input_ptr as the same data type

// Read user input into variable userInput

// Ensure tempValue contains the same value as userInput utilizing one or more memory operators

// tempValue = userInput; // NOT PERMITTED... use a memory operator

// Print the value and the pointer for each of the three variables in a human readable format

// Locate those addresses in the "memory window"

// Submit your work to the appropriate location for instructor review

// "input_ptr" should be a pointer variable.


# include <stdio.h>

int main()
{

    // Declare userInput, tempValue, and input_ptr as the same data type
    int tempValue;
    int user_input;
    int *input_ptr = NULL;

    // Read user input into variable userInput
    printf("Enter a number: \n");
    scanf("%d", &user_input);
    printf("You entered: %d\n", user_input);

    input_ptr = &user_input;

    printf("input_ptr: %p\n", input_ptr);
    printf("input_ptr dereferenced: %d\n",*input_ptr);

    tempValue = *input_ptr;
    printf("this is tempValue: %d\n", tempValue);


    return 0;
}