// Demonstration Lab - Memory Operators
// Declare two variables, named var1 and var2, of the same data type
// Declare a pointer variable, named var_ptr, of the same data type
// Define the first variable with an arbitrary value
// Assign var1's memory address to var_ptr
// Define var2 by dereferencing var_ptr
// Compare var1 to var2 and print human-readable results

#include <stdio.h>

// int main()
// {
//     // Declare two variables, named var1 and var2, of the same data type
//     // Declare a pointer variable, named var_ptr, of the same data type
//     int var1 = 12;
//     int var2 = 7;

//     // Define the first variable with an arbitrary value
//     int *var_ptr = NULL;

//     // Pointers have no default value. The value they have is just
//     // whatever junk was in the memory they're using now. Sometimes
//     //  specific compiler will zero out memory, but that's not standard so don't count on it.)

//     // Assign var1's memory address to var_ptr
//     var_ptr = &var1;
//     printf("var1 is %d at address %p\n", var1, &var1);
//     printf("var_ptr is %p at address %p\n", var_ptr, &var_ptr);
//     printf("Value at %p derefrenced is %d\n", var_ptr, *var_ptr);
//     printf("\n\n*********\n\n");

//     // Define var2 by dereferencing var_ptr
//     var2 = *var_ptr;

//     printf("var2 is %d at address %p\n", var2, &var2);
//     printf("var_ptr is %p at address %p\n", var_ptr, &var_ptr);
//     printf("Value at %p dereferenced is %d\n", var_ptr, *var_ptr);
//     printf("\n\n**************\n\n");

//     printf("Var1 versus Var2 versus var_ptr\n");
//     printf("*****************FIGHT*********************\n");
//     printf("The var1 Address: %p - Value: %d\n", &var1, var1);
//     printf("The var2 Address: %p - Value: %d\n", &var2, var2);
//     printf("The var_ptr Addr: %p - Value: %p  - dereferenced: %d\n", &var_ptr, var_ptr, *var_ptr);

//     return 0;
// }

//**********************demo 2******************array arithmetic****************
// The little int that could
// int * find_smallest_natural_num (int * intArr_ptr, int arrSize);
// Return Value:

// Int pointer to the smallest natural number found in the array
// Parameters:

// intArr_ptr - Pointer to an array of ints
// arrSize - Length of the array
// Purpose:

// Find smallest natural number
// Requirements:

// Return NULL if intArr_ptr is NULL
// Return NULL if arraySize is unrealistic
// A natural number is a whole, positive number > 0

int main()
{

    int numArray[] = {3, 6, 77, 82, 5, 99, 34, 2, 21, 11};

    int *num_ptr = numArray;
    // printf("This is the num pointer: %p\n", num_ptr);
    // printf("This is it's value: %d\n", *num_ptr);
    if (*num_ptr == '\0')
    {
        printf("The array is empty");
    }

    // To determine the number of elements in the array, we can divide the total size of the array by the size of the array element.
    int array_size = sizeof(numArray)/sizeof(numArray[0]);
    // printf("This is the size of the array %d\n", array_size);

    if (array_size < 1 || array_size > 20)
    {
        printf("I'm too tired for these shenanigans\n");
    }

    int i = 0;
    int *temp = num_ptr;

    while (i < array_size)
    {
        if (*num_ptr < 0 || *num_ptr > 100)
        {
            printf("Enough shenanigans!\n");
            break;
        }

        if (*num_ptr <*temp)
        {
            temp = num_ptr;
        }
        num_ptr++;
        i++;
    }
    printf("The smallest item is %d\n", *temp);
    return 0;
}