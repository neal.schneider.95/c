// Cube a variable using pass-by-value.
#include <stdio.h>

void cubeByRef( int *n ); // prototype

int main( void )
{
   int number = 5; // initialize number

   printf( "The original value of number is %d", number );
   
   // pass number by value to cubeByValue
   cubeByRef( &number );

   printf( "\nThe new value of number is %d\n", number );
} // end main

// calculate and return cube of integer argument                 
void cubeByRef( int *n )                                            
{                                                                   
   *n = *n * *n * *n; // cube local variable n and return result
} // end function cubeByValue  
