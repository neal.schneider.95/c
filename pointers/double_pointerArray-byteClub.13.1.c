#include <stdio.h>

int main(){
    // char * rule0 = "Talk about Byte Club.";
    // char * rule1 = "Please talk about Byte Club.";
    // char * rule2 = "If a byte is 0x0 the string is over.";
    // char * byteClubRules[] = {rule0, rule1, rule2};

    char byteClubRules[3][40] = {
    "Talk about Byte Club.",
    "Please talk about Byte Club.",
    "If a byte is 0x0 the string is over."
    };



    /* byteClubRules is an array of character pointers */ 
    for (char i = 0; i < 3; i++)
    {
        printf("Rule #%d:\t%s\n" , i, byteClubRules[i]);
    }
}
