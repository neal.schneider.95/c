#include <stdio.h>

void main(){
    
    //Example code will be executed per line and displayed in memory
    int someList[] = { 0xFEEDFACE, 0xC001C0DE, 0xCAFEF00D, 0xDECAFBAD };
    someList[0] = 0x8BADF00D;		// Normal
    int * someList_ptr = &someList[1];	// By pointer reference...
    * someList_ptr = 0xC0DEDEAD;		// ...and then dereferencing it
    * (someList + 2) = 0x1BADD00D;		// Array name pointer math…
    someList_ptr += 2;			// ...AKA Address arithmetic
    * someList_ptr = 0xDEADBEEF;		// Dereference a pointer

}