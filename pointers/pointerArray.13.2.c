#include <stdio.h>

int main(){
	
    float stu1Grades[5] = { 90.1, 100.0, 89.9, 78.1, 87.6};
    float stu2Grades[5] = { 70.5, 79.3, 68.9, 3.0, 0.0};
    float stu3Grades[5] = {100.0, 100.0, 100.0, 99.9};
    float * classGrades[] = { stu1Grades, stu2Grades, stu3Grades};
    /* classGrades is an array of floating point integer pointers */
    
    float tempAvgGrade = 0;
    printf("\nStudent Averages\n-------------------\n");
    for (int i = 0; i < 3; i++)
    {
        tempAvgGrade = average_students_grades(*(classGrades + i), 5);
        printf("Student #%d:\t%2.1f", (i + 1), tempAvgGrade);
    }
    printf("----------------\n");
}
