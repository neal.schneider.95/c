#include <stdio.h>

int add (int a, int b);

/* Declare add_fn_ptr:
  ╭── Return type  
╭─┤   ╭────────┬─ Function pointer variable's name */
int (*add_fn_ptr) (int, int);
/*   │            ╰────────┴─ Parameter List
     ├── Asterisk denotes that this is a pointer  
     ╰── Without the (), this would be a function prototype returning an int* 
*/

int main () {
    int sum;
    add_fn_ptr = &add;
    for (int i = 0; i<4; i++){
        sum = add_fn_ptr(i, i);
        printf("%d\n", sum);
    }
}

int add (int a, int b) {
    return a + b;
}
