#include <stdio.h>
typedef struct{
    char point_name[28];
    float grade;
} My_struct;

void main() {
    My_struct struct_array[10] = {};    // zerorize an array of 10 structs
    printf("Struct Size: %ld\n", sizeof(My_struct));
    for (int i = 0; i<10; i++) {
        printf("struct_array[%d] is at %p\n", i, &struct_array[i]);
    }
}