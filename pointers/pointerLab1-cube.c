// CCD Pointer Lab Solution
// Cube_it
// Evaluates Passing a pointer

#include <stdio.h>

// Fn definition:  Must be a pointer, not a returned value
void cube_it(float *x) {
    *x = *x * *x * *x;
}

void main() {
    float num;
    printf("Enter a number to cube: ");
    scanf("%f", &num);
    cube_it(&num);
    printf("The number cubed is: %f\n", num);
}