#include <stdio.h>
#include <string.h>

void main() {
    char t[][] = {"W", "i", "l", "l", "\0"};

    char copied_str[200] = "Blah, blah";
    char original_str[] = "The answer is 42!";
    printf ("%s\n", copied_str);
    strncpy(copied_str, original_str, 8);
    printf ("%s\n", copied_str);

    printf ("%s\n", copied_str);
    strncpy(copied_str, original_str, sizeof(original_str));
    printf ("%s\n", copied_str);
    // prints:
}