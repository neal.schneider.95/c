#include <stdio.h>
void main(){
    char *sAry[] = {"hello", "there", "this", "is", "not", "a", "char[][]"};

    for(int i = 0; i < 7; i++){
        printf("%s ", sAry[i]);
    }
    printf("\n");

    char *sAry2[10];
    sAry2[0] = "hello";
    sAry2[1] = "there";
    sAry2[2] = "this";
    sAry2[3] = "is";
    sAry2[4] = "not";
    sAry2[5] = "a";
    sAry2[6] = "char[][]";

    for(int i = 0; i < 7; i++){
        printf("%s ", sAry2[i]);
    }
    printf("\n");
    
    char *sAry3[10] = {"hello", "there", "this", "is", "not", "a", "char[][]"};
    for(int i = 0; i < 10; i++){ //may segfault or may produce garbage because the values at the end of the array are not initialized
        printf("%s ", sAry[i]);
    }
    printf("\n");

}
