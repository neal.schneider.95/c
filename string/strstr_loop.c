#include <stdio.h>
#include <string.h>

void main() {
    char haystack[] = "bbbbooth yoda bootwo kimchee boothree";
    char needle[] = "boot";
    char *found;

    found = strstr(haystack, needle);
    while (found) {
        printf("%s\n", found);
        found = strstr(found +1, needle);
    }
}