#include <stdio.h>
#include <string.h>

void main() {
    char haystack[] = "bbbboone bootwo boothree";
    char needle[] = "boot";
    char *found;

    found = strstr(haystack, needle);
    if (found) {
        printf("%s\n", found);
    } else {
        printf("%s not found\n", needle);
    }

    found = strstr(found+1, needle);
    if (found) {
        printf("%s\n", found);
    } else {
        printf("%s not found\n", needle);
    }

    found = strstr(found+1, needle);
    if (found) {
        printf("%s\n", found);
    } else {
        printf("%s not found\n", needle);
    }
}