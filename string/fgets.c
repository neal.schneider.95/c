#include <stdio.h>

void main() {
    char name[10] = {};
    printf("Your name, please: ");
    fgets(name, 10, stdin);
//         │    │     └─── stream from the console
//         │    └── max size
//         └── string to write to
    printf("Welcome to class, %s!\n", name);
}