#include <stdio.h>

void main() {
    char name[10] = {};
    int age = 0;
    printf("Your name and age, please: ");
    scanf("%10s%d", name, &age);
    //      └┴── String Width
    printf("Welcome, %s of %d years!\n", name, age);

    printf("Your name and age, please: ");
    scanf("%10s%d", name, &age);
    //      └┴── String Width
    printf("Welcome, %s of %d years!\n", name, age);

}