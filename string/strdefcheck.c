#include <stdio.h>
#include <string.h>

void main() {
    char studentLastName1[] = {89, 'o', 0x75};
    char studentLastName2[32] = "You";
    char format[] = "%s\n";
    printf (format, studentLastName1);
    printf (format, studentLastName2);

    for (int i=0; i<32; i++) {
        studentLastName1[i]='X';
        studentLastName2[i]='X';
    }
    printf (format, studentLastName1);
    printf (format, studentLastName2);
}