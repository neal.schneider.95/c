#include <stdio.h>

void main() {
    float dist = 402844;
    char *celestial_body = "Moon";
    char origin[] = "Earth";
    char phrase[100] = {};
    sprintf(phrase, "The %s is %.0fkm away from the %s.", celestial_body, dist, origin);
    //      └─┰──┘  └─────────┰────────────────────────┘  └─┰────────────────────────┘
    //   Destination          │                             │
    //         Format String ─┘           Output arguments ─┘ 
    puts(phrase);
}