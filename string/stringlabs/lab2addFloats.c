#include <stdio.h>
#include <stdlib.h>                 // defines strof

int main(){
    char i;                         // counter
    char s[100] = {0};              // input buffer;
    float floats[4];
    float sum = 0;                  // accumulator
    printf("Enter four numbers:\n");
    for (i = 0; i<4; i++)
    {          
        printf("%d:", i+1);         // prompt the user
        fgets(s, 100, stdin);       // Safely get the input
        // Convert the string to a float using the stdlib fn and add
        floats[i] = strtof(s, NULL);
    }

    for (i = 0; i<4; i++) {
        sum += floats[i];           // add the values
        printf("%15.5f\n", floats[i]);  // print each value
    }
    printf("-------------------");
    printf("%15.5f sum\n", sum);    // print the sum
}