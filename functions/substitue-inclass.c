#include <stdio.h>

// replace newlines in a string with a space.
// input : string to operate one
// output: number of characters replaced
#define MAXBUFFSIZE 255
#define ERR_NULL_POINTER -1
#define ERR_NONE_FOUND 0
char data[] = "Data to search\n\n\n";

int delete_newline(char *string) {
    int count = 0;
    int i = 0;
    if (string) {
        while (string[i] && i < MAXBUFFSIZE)
        {
            if (string[i] == '\n') {
                string[i] = ' ';
                count = count + 1;
            } // end if
            i++;
        } // end for
    }
    else
    {
        return ERR_NULL_POINTER;
    }

    return count;
}

int main (){
    int count = 0;
    
    char str[] = "\n\n\n\nNothing much here... \n\n";
    printf("Initailly: \n%s", str);
    count = delete_newline(str);

    printf ("After %d subs:\n%s ", count, str);
}