// Ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Labs/Performance/healthy_substitutions.html

// "Healthy Substitutions"

// int replace_character(char * string, const char findThisChar, const char replaceItWithThis);
// Return Value - number of characters replaced
// Parameters
// Pointer to a null-terminated string
// Character to find
// Character to replace it with
// Purpose - replace all occurrences of findThisChar with replaceItWithThis
// Use pre-defined return values as indicated in shell code

// Neal Schneider
#include <stdio.h>
#define ERR_NULL_POINTER -1
#define ERR_NONE_FOUND 0

int replace_character(char * string, const char findThisChar, const char replaceItWithThis) {
    int nlCount = 0;
    int i = 0;
    while (string[i] && i < 640) {
        if (string[i] == findThisChar) {
            string[i] = replaceItWithThis;
            nlCount += 1;
        }
        i+=1;
    }
    return nlCount;
}

int main (){
    char strings[][640] = {"\n\n\n\n\n\n\n\n\n\nNot\n much\n here...\n\n\n\n\n\n\n\n",\
    "The quick brown fox jumed over the lazy dog",\
    "a\nab\nabc\nabcd\abcde\n",\
    "O say can you see, by the dawn's early light,\n\
What so proudly we hailed at the twilight's last gleaming,\n\
Whose broad stripes and bright stars through the perilous fight,\n\
O'er the ramparts we watched, were so gallantly streaming?\n\
And the rocket's red glare, the bombs bursting in air,\n\
Gave proof through the night that our flag was still there;\n\
O say does that star-spangled banner yet wave\n\
O'er the land of the free and the home of the brave?", \
""};

    int count;

    for (int i=0;i<5;i++) {
        printf ("String %i is :\n %s\n", i, strings[i]);
        count = replace_character(strings[i], '\n', ' ' );
        printf ("With %i subs, String %i is now:\n %s\n\n", count, i, strings[i]);
    }
}