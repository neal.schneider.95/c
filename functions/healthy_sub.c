// Ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Labs/Performance/healthy_substitutions.html

// "Healthy Substitutions"

// int replace_character(char * string, const char findThisChar);
// Return Value - number of characters replaced
// Parameters
// Pointer to a null-terminated string
// Character to find
// Character to replace it with
// Purpose - replace all occurrences of findThisChar with replaceItWithThis
// Use pre-defined return values as indicated in shell code
#include <stdio.h>
#define MAX_LEN 640

int replace_character(char * string, char oldchar, char newchar);

int replace_character(char * string) {
    int count = 0;
    int i = 0;
    while (string[i] && i < MAX_LEN) {
        if (string[i] == '\n') {
            string[i] = ' ';
            count += 1;
        }
        i++;
    }
    return count;
}

int main() {
    int charcount = 0;
    char s[] = "O say can you see, by the dawn's early light,\n\
What so proudly we hailed at the twilight's last gleaming,\n\
Whose broad stripes and bright stars through the perilous fight,\n\
O'er the ramparts we watched, were so gallantly streaming?\n\
And the rocket's red glare, the bombs bursting in air,\n\
Gave proof through the night that our flag was still there;\n\
O say does that star-spangled banner yet wave\n\
O'er the land of the free and the home of the brave?";
    printf("String initially: %s\n", s);
    charcount = replace_character(s);
    printf("Replaced %d Charaters\n", charcount);
    printf("S is now:\n%s", s);
}