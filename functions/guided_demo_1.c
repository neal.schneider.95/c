//User defined functions

#include<stdio.h>

//Function declarations or function prototypes

void println();
int add(int, int);
int sub(int x, int y);

//main function, the master function

void main()
{
    int a,b,sum, diff;
    printf("Enter the values\t");
    scanf("%d %d",&a, &b);
    //Function invocations
    //Asking the workers to do work
    sum=add(a,b);
    diff=sub(a,b);
    println();
    //Master presents the results returned by workers
    printf("Result of addition is %d\n",sum);
    printf("Result of subtraction is %d\n",diff);
}

//Function definitions

void println()
{
    printf("-------------------------\n");
}

int add(int a, int b)
{
    return a+b;
}

int sub(int a, int b)
{
    return a-b;
}