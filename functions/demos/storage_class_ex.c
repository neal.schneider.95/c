#include <stdio.h>

int incrementer (int i) {

    // By declaring calls as static, it's data will persist between calls to incrementer
    static int calls = 0;

    calls++;
    printf("%d calls\n", calls);
    return i++;
}

int main () {
    printf("In Main\n");
    for (int i=0; i< 400; i += 20){
        incrementer (i);
        printf("i = %d, ", i, incrementer(i));
    }
}