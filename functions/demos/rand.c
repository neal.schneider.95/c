#include <stdio.h>
#include <stdlib.h> // where rand() lives
#include <time.h>   // where time() used for rand initialization lives

int pickrand (int * array, int size) {
    return (array[rand() % size]);
}

void printArr (int *arr, int size){
    printf("A random member of [");
    for (int i = 0; i<size; i++) {
        printf("%d", arr[i]);
        if (i<size-1) printf(", ");
    }
    printf("] is:\n");
}

int main () {
    // initialze random number generator
    srand(time(0));

    int a1[] = {2, 4, 6, 8, 10};
    int a2[] = {3, 5, 7, 9, 11};
    int a3[] = {6, 10, 14, 18, 22};
    int a4[] = {10, 20, 30, 40, 50, 60, 70, 80};
    int a5[] = {300, 301, 302, 310, 311};

    printArr(a1, 5);
    printf("%d \n", pickrand(a1, 5));
    printArr(a2, 5);
    printf("%d \n", pickrand(a2, 5));
    printArr(a3, 5);
    printf("%d \n", pickrand(a3, 5));
    printArr(a4, 8);
    printf("%d \n", pickrand(a4, 8));
    printArr(a5, 5);
    printf("%d \n", pickrand(a5, 5));
} 