#include <stdio.h>
// F(n) = F(n-1) + F(n-2)
int iterations =0;

int fib (int num) 
{
    iterations ++;
    if (num <= 1)
    {
        return num;
    }
    else 
    {
        return fib(num-1) + fib(num-2);
    }
}



int fib_linear (int num) {
    int first = 0, second = 1, result;
    if (num <= 1){
        return num;
    } else {
        for (int i = 1; i < num; i++) {
            result = first + second;
            first = second;
            second = result;
            iterations++;
        }
    return result;
    }
}


int main () {
    for (int i =0; i<45; i++)
    {
        iterations = 0;
        int result = fib(i);
        printf("fib(%d) = %d (iterations = %d)\n", i, result, iterations);
    }
}
