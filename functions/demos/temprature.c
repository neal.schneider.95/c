#include <stdio.h>
// Convert F to C
float celsius (float f) {
    return ((f - 32.0) * 5.0 / 9.0);
}

// Convert C to F
float fahrenheit (float c) {
    return (c *9.0 / 5.0) +32.0;
}

int main () {
    printf (" C -> F\n----------\n");
    for ( float t = 0; t <= 100; t += 1.0){
        printf ("%3.1f -> %3.1f\n", t, fahrenheit(t));
    }
        printf ("\n F -> C\n----------\n");
    for ( float t = 32; t <= 212.0; t += 1.0){
        printf ("%3.1f -> %3.1f\n", t, celsius(t));
    }
}