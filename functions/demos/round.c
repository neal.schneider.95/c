#include <stdio.h>
#include <math.h>

// A generalized function that allows for any number of significant digits
float roundToPlace(float number, int digits) {
    float scale = pow(10.0, (double) digits);
    return (float) (floor(number * scale + 0.5) / scale);
}

float roundToInteger( float number ){
    return (float) floor(number + .5 );
}

float roundToTenths( float number ) {
    return (float) floor( number * 10 + .5 ) / 10.0;
}

float roundToHundreths( float number ){
    return (float) floor( number * 100 + .5 ) / 100.0;
}

float roundToThousandths( float number ){
    return (float) floor( number * 1000 + .5 ) / 1000.0;
}

int main () {
    float value;
    while (1) {
        printf("Enter a value to round (0 to quit): ");
        scanf("%f", &value);
        if (!value) {
            break;
        }
        printf ("your value: %f\n", value);
        printf ("rounded to ingeter: %f\n", roundToInteger(value));
        printf ("rounded to tenths : %f\n", roundToTenths(value));
        printf ("rounded to hundreths: %f\n", roundToHundreths(value));
        printf ("rounded to thousandths: %f\n", roundToThousandths(value));

        printf ("rounded to ingeter: %f\n", roundToPlace(value, 0));
        printf ("rounded to tenths : %f\n", roundToPlace(value, 1));
        printf ("rounded to hundreths: %f\n", roundToPlace(value, 2));
        printf ("rounded to thousandths: %f\n", roundToPlace(value, 3));
    }
}