// Ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Labs/Demonstration/function-prototypes.html

// Demo: "Newline Records"

// int remove_newline(char * buffer);
// We are going to create a function that takes a pointer (don't get too caught up on the use of a poitner) 
// to a buffer, containing a nul-terminated string. We are then going to replace all newline characters 
// with spaces and return the number of newline characters changed.

// Return Value : Number of newline characters changed

// Parameters : Pointer to a null-terminated string

// Purpose : Replace all newline characters with spaces

// Requirments :

// Ensure buffer is not NULL
// Return ERR_NULL_POINTER if buffer is NULL
// Return ERR_NONE_FOUND if no newlines are found

// Neal Schneider
#include <stdio.h>
#define ERR_NULL_POINTER -1
#define ERR_NONE_FOUND 0
#define MAX_BUFF_SIZE 640

int remove_newline(char *buffer) {
    if (buffer == NULL) {
        return ERR_NULL_POINTER;
    }
    int nlCount = 0;
    int i = 0;
    while (buffer[i] && i < MAX_BUFF_SIZE) {
        if (buffer[i] == '\n') {
            buffer[i] = ' ';
            nlCount += 1;
        }
        i+=1;
    }
    return nlCount;
}

int main (){
    char strings[][MAX_BUFF_SIZE] = {
    "\n\n\n\n\n\n\n\n\n\nNot\n much\n here...\n\n\n\n\n\n\n\n",
    "The quick brown fox jumed over the lazy dog",
    "a\nab\nabc\nabcd\abcde\n",
    "", 
    "O say can you see, by the dawn's early light,\n\
What so proudly we hailed at the twilight's last gleaming,\n\
Whose broad stripes and bright stars through the perilous fight,\n\
O'er the ramparts we watched, were so gallantly streaming?\n\
And the rocket's red glare, the bombs bursting in air,\n\
Gave proof through the night that our flag was still there;\n\
O say does that star-spangled banner yet wave\n\
O'er the land of the free and the home of the brave?", \
""};

    int count;

    for (int i=0;i<5;i++) {
        printf ("String %i is :\n %s\n", i, strings[i]);
        count = remove_newline(strings[i]);
        printf ("With %i subs, String %i is now:\n %s\n\n", count, i, strings[i]);
    }
    
    printf("remove_newline(NULL) is: %d\n", remove_newline(NULL));
}