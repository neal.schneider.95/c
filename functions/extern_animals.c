// animals.c
#include <stdio.h>

int animals = 8; // globals are externally linked by default
const int i = 5;

void call_me()
{ // functions are externally linked by default
    printf("%d %d\n", i, animals);
}