// int fibonacci_number(int num);
// Purpose
// Define fibonacci_number() as a recursive function that returns a Fibonacci number of position "num"
// Allow the user to choose how many numbers are calculated
// Parameters
// Fibonacci Sequence number position to calculate
// Return Value
// sequenceNumber
// -1 if sequenceNumber is unrealistic
// The Fibonacci Sequence:
// Starts with 0 and 1 then each subsequent number is calculated by adding the two previous sequence numbers

// F(n) = F(n-1) + F(n-2)

#include <stdio.h>

int fib (int num) {
    int first = 0, second = 1, result;
    if (num <= 1){
        return num;
    } else {
        for (int i = 1; i < num; i++) {
            result = first + second;
            first = second;
            second = result;
        }
    return result;
    }
}

int main () {
    // int sequence_num;
    // printf ("Fibonacci sequence: ");
    // scanf ("%d", &sequence_num);
    // printf ("The Fibonacci number for %i is %llu", sequence_num, fib(sequence_num));
    for (int i = 0; i <10; i++){
        printf ("%d -> %d\n", i, fib(i));
    }
}