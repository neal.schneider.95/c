#include <stdio.h>

int print_offset_str(char *input_str, int offset) 
{
    int char_count;
    char_count = printf ("%*s\n", offset, input_str);
    return char_count;
}

int main ()
{
    char strs[3][10] = {"Good", "Bad", "Ugly"};
    for (int i = 0; i<3; i++) 
    {
        print_offset_str (strs[i] , 20 - 5*i);
    }
    print_offset_str("Han Solo",0);
}