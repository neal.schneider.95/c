#include <stdio.h>

float power (float base, int exponent ) ;

float power (float base, int exponent ) {
    float result = 1;
    for (int i = 0; i<exponent; i++) {
        result *= base;
    }
    return result;
}

int main () {
    for (int i = 0; i< 128; i++) {
        printf(" %d -> %d\n", i, power(11.1, i));
    }
}
pow()