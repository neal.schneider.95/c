/*
TSgt Clemente Fernandez
27 September 2021
Lab 2:
    Given the length and breadth of a rectangle, write a program to find 
    whether the area of the rectangle is greater than its perimeter. For 
    example, the area of the rectangle with length = 5 and breadth = 4 is 
    greater than its perimeter.
*/
#include <stdio.h>
#define PERM "The perimeter is greater than its area!"
#define AREA "The area is greater than its perimeter!"

const char* area_or_perm(double dblLength, double dblBreadth);
int main(void)
{
    // vars
    double dblLength;
    double dblBreadth;
    
    // prompt user for input
    printf("Enter Length: ");
    // take input and initialize length
    scanf("%lf", &dblLength);
    printf("Enter Breadth: "); 
    // take input and initialize breadth
    scanf("%lf", &dblBreadth);

    printf("%s", area_or_perm(dblLength, dblBreadth));
}
// takes two doubles(length and breadth) calculates the area/perm
// returns message stating which is larger.
const char* area_or_perm(double dblLength, double dblBreadth)
{
    // vars
    double dblArea;
    double dblPerimeter;

    // calc area
    dblArea = dblLength * dblBreadth;
    // calc perimeter
    dblPerimeter = (dblLength * 2) + (dblBreadth * 2);

    // compares area and permiter
    char *message = dblArea > dblPerimeter ? AREA : PERM;

    return message;   
}
