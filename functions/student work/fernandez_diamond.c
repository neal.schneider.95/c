/*
TSgt Clemente Fernandez
28 September 2021
Lab 11
    Diamond pattern 
    Write a program that prints the following diamond shape. You may use printf 
    statements that print either a single asterisk (*) or a single blank. 
    Maximize your use of iteration (with nested for statements) and minimize 
    the number of printf statements.
*/
#include <stdio.h>
#include <string.h>

int main(void)
{
//     int i;
//     int j;
//     int k = 5;
    

//     ///////// CUMBERSOME WAY ////////////
//     for (i = 1; i <= 9 ; i = i + 2)
//     {   
//         printf("%*s", k, "*");
//         for ( j = 1; j < i ; j++)
//         {
//             printf("%s", "*");    
//         }
//         printf("\n");
//         k -= 1;
//     }
//     k = 2;
//     for (i = 7; i >= 0 ; i = i - 2)
//     {   
//         printf("%*s", k, "*");
//         for ( j = 1; j < i ; j++)
//         {
//             printf("%s", "*");    
//         }
//         printf("\n");
//         k += 1;
//     }
// }

    int n;
    int o;

    for (int i = 1; i < 21; i = i+2)
    {
        if (i < 11)
        {
            for (o = 4; o > 0; o--)
            {
                printf(" ");
            }
            
            for (n = 1; n <= i ; n++)
            {
                printf("*");
            }
        }
        printf("\n");    
    }
}