/*
TSgt Clemente Fernandez
27 September 2021
Lab 1:
    Write a program to check whether a triangle is valid or not, when the 
    three angles of the triangle are entered through the keyboard. A triangle
    is valid if the sum of all the three angles is equal to 180 degrees.
*/
#include <stdio.h>

const char* tri_angles(double dblAngleOne, double dblAngleTwo, double dblAngleThree);

int main(void)
{   
    // var
    double dblAngleOne;
    double dblAngleTwo;
    double dblAngleThree;
    
    // prompt user for input
    printf("Enter angle #1: ");
    // take input and initialize angle one
    scanf("%lf", &dblAngleOne);
    printf("Enter angle #2: "); 
    // take input and initialize angle two
    scanf("%lf", &dblAngleTwo);
    printf("Enter angle #3: ");
    // take input and initialize angle three
    scanf("%lf", &dblAngleThree);
    
    // displays determination for user
    printf("%s", tri_angles(dblAngleOne, dblAngleTwo, dblAngleThree));
    
    
}

// takes 3 doubles as arguments, returns determination as string
const char* tri_angles(double dblAngleOne, double dblAngleTwo, double dblAngleThree)
{
    // var
    double dblSum;

    // calculate angle
    dblSum = dblAngleOne + dblAngleTwo + dblAngleThree;

    // not sure if this is an appropriate use of a pointer. 
    char *message = dblSum == 180 ? "Valid Triangle" : "Invalid Triangle";   

    // returns string with appropriate message
    return message;
}
