### 1. Write a program in C to find the square of any number using a function.
```    
Test Data :
Input any number for square : 20
Expected Output :
The square of 20 is : 400.00
```
### 2. Write a program in C to find the square of any number using a function. 

### 3. Write a program in C to swap two numbers by passing by reference.

### 4. Write a program in C to check a given number is even or odd by implementing an is_odd function.

### 5. write a program in C to get the largest element of an array using the function.

### 6. write a program in C to find the maximum and minimum of a set of numbers by passing an array.

### 7. Write a program in C to determine if two strings are anagrams by implement an is_anagram() function.  (All the characters in each string appear the same number of times in the other.) 