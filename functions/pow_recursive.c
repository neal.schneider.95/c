#include <stdio.h>

long rec_power(int base, int exp)
{
    if (0 == exp)
    {
        return 1;
    }
    else if (exp > 0)
    {
        return base * rec_power(base, exp - 1);
    }
    else
    {
        return 1;
    }
}

int main () {
    for (int i = 1; i<10; i++) {
        long temp = rec_power (2, i);
        printf ( "%lu\n", temp );
    }
}