#include <stdio.h>

int main () {
    FILE *f;
    if ((f = fopen ("text.txt", "w")) == NULL) {
        puts("File could not be opened.");
    }
    fprintf(f, "Howdy!\n");
    fclose(f);
}