#include <stdio.h>
#include <string.h>
#define MAXSTRLEN  20

int MAXSIZE = 8;       
char  stack[8][MAXSTRLEN];     
int top = -1;            

int isempty() {

   if(top == -1)
      return 1;
   else
      return 0;
}
   
int isfull() {

   if(top == MAXSIZE)
      return 1;
   else
      return 0;
}

char* peek() {
   return stack[top];
}

char* pop() {
   char* data;
    
   if(!isempty()) {
      top = top - 1;   
      return stack[top];
   } else {
      printf("Could not retrieve data, Stack is empty.\n");
   }
}

int push(char* data) {

   if(!isfull()) {
      strcpy (stack[top], data);
      top = top + 1;   
   } else {
      printf("Could not insert data, Stack is full.\n");
   }
}

int main () {
   char inputStr[1024] = {0};
   char *word;

   // get the sentence
   printf ("Enter a sentence to reverse: ");
   scanf("%[^\n]s", inputStr); 
   char *delim =  " \t";

   // parse and push on stack
   word = strtok (inputStr, delim);
   while (word != NULL) {
      push (word);
      word = strtok(NULL, delim);
   }

   // print stack data 
   while(!isempty()) {
      char* str = pop();
      printf("%s\n",str);
   }
}