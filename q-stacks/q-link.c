#include<stdio.h>   
#include<stdlib.h>  
struct node   
{  
    int data;   
    struct node *next;  
};  
struct node *front;  
struct node *rear;  
int inputVal;
void insert(int val);  
int delete();  
void display();  
void main ()  
{  
    int choice;   
    while(choice != 4)   
    {     
        //printf("\n*************************Main Menu*****************************\n");  
        // printf("\n=================================================================\n");  
        printf("\n1.Insert an element  2.Delete an element  3.Display the queue  4.Exit\n");  
        printf("\nEnter your choice ? ");  
        scanf("%d",& choice);  
        switch(choice)  
        {  
            case 1:  
                printf ("Enter value to add: " );
                scanf ("%d", &inputVal);
                insert(inputVal); 
                display();
                break;  
            case 2:  
                printf ("removed %d from the queue\n", delete()); 
                display();
                break;  
            case 3:  
                display();  
                break;  
            case 4:  
                exit(0);  
                break;  
            default:   
                printf("\nEnter valid choice??\n");  
        }  
    }  
}  
void insert(int val)  
{
    struct node *newNode =(struct node*) malloc (sizeof (struct node));
    if (!newNode) {
        printf("Out of memory!\n");
        return;
    }
    newNode->data = val;
    newNode->next = NULL;
    if (rear) {
        rear->next = newNode;
        rear = newNode;
    }
    else {
        rear = front = newNode;
    }

}     
int delete ()  
{  
    if (front) {
        struct node *link = front;
        int tmpval = front->data;
        front = front->next;
        if (front==NULL) {
            rear = NULL;
        }
        free (link);
        return tmpval;
    }
    else {
        printf ("ERROR: Underflow!\n");
    }
}  
void display()  
{  
    struct node * link = front;
    if (link == NULL) {
        printf ("Empty Queue!\n");
        return;
    }
    printf ("Front -> ");
    while (link) {
        printf("%d -> ", link->data);
        link = link->next;
    }
    printf ("Rear\n");
}  
