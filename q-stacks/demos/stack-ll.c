#include <stdio.h>
#include <stdlib.h>
#define TRUE 1
#define FLASE 0

struct node
{
    int data;
    struct node *next;
};
typedef struct node node;

node *top;
void init()
{
    top = NULL;
}

void push(int value)
{
    node *tmp;
    tmp = (node*)malloc(sizeof(node));
    tmp -> data = value;
    tmp -> next = top;
    top = tmp;
}

int pop()
{
    node *tmp;
    int n;
    tmp = top;
    n = tmp->data;
    top = top->next;
    free (tmp);
    return n;
}

int peek()
{
    return top->data;
}

int isempty()
{
    return top == NULL;
}

void display(node *head)
{
    if (head == NULL) {
        printf("NULL\n");
    } else
    {
        printf ("%d -> ", head->data);
        display(head->next);
    }
}

int main ()
{
    init();
    push(10);
    push(20);
    push(30);
    push(60);
    display(top);
    printf("The top is %d\n", peek());
    pop();
    printf("The top after pop is %d\n", peek ());
    display(top);
}