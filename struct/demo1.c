#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
Struct Demonstration Lab
String Splitter
Define a function that inputs a string with a delimiter and returns a student struct.

struct student split_the_string(char * string_ptr, char delimiter);

Test Input Should Include:
Input String
Delimiter
Return a struct that contains the data from the string_ptr
Print the human-readable results
Sample Input
("Charlie Chaplin, 87, 50", ',')

Sample Output

Name: Charlie Chaplin
Grade: 87
Favorateness Ranking: 50+
The function also returns a student struct with the above data.
*/

#define MAX_STR_LEN 100

typedef struct {
    char name [MAX_STR_LEN];
    int grade;
    int fav;
} Student;

void print_student (Student st) {
    printf("    name: %s\n", st.name);
    printf("   grade: %d\n", st.grade);
    printf("fav rank: %d\n", st.fav);
}

Student split_the_string(char * string_ptr, char delimiter) {
    int i = 0;
    int token_start = 0;
    Student temp_stu;
    char tmp_str[MAX_STR_LEN]= {'\0'};

    while (string_ptr[i] && string_ptr[i] != delimiter && i < MAX_STR_LEN){
        temp_stu.name[i] = string_ptr[i];
        i++;
    }
    temp_stu.name[i]= 0; 
    token_start = ++i;

    while (string_ptr[i] && string_ptr[i] != delimiter && i < MAX_STR_LEN){
        tmp_str[i-token_start] = string_ptr[i];
        i++;
    }
    tmp_str[i]  = 0;
    token_start = ++i;
    temp_stu.grade = atoi(tmp_str);

    while (string_ptr[i] && string_ptr[i] != delimiter && i < MAX_STR_LEN){
        tmp_str[i-token_start] = string_ptr[i];
        i++;
    }
    tmp_str[i]  = 0;
    temp_stu.fav = atoi(tmp_str);

    print_student(temp_stu);

    return temp_stu;
}

int main () {
    Student stu;
    stu = split_the_string ("Charlie Chaplin, 87, 50", ',');
    print_student (stu);
    stu = split_the_string ("Angeles, 100, 101", ',');
    stu = split_the_string ("Fernandez:101:100", ':');

}
