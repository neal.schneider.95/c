#include <stdio.h>
typedef struct houseForSale {
    char mailing_address[1024];
    float cost_per_sq_ft;
    float square_footage;
    float total_cost;
} HouseForSale;

int main(void)
{
    HouseForSale FF4HQ = {
        "Baxter Building, 42nd Street, Madison Avenue, Manhattan", 107.15, 28000, 0
    };
    HouseForSale durdenManor = {
        "420 Paper St. Wilmington, DE 19886", 0.01, 4500, 0
    };

    HouseForSale mustSellHouses[4] = { FF4HQ, durdenManor }; 
    HouseForSale backList[4];
    backList [0] = mustSellHouses[0];

    printf("address=%s \n$/sqft=%0.2f \nsqft=%0.2f \ncost=%0.2f \nmem_address=%p\n\n",
        FF4HQ.mailing_address, 
        FF4HQ.cost_per_sq_ft, 
        FF4HQ.square_footage, 
        FF4HQ.total_cost,
        &FF4HQ); // <- the address of FF4HQ
    printf("address=%s \n$/sqft=%0.2f \nsqft=%0.2f \ncost=%0.2f \nmem_address=%p\n\n",
        mustSellHouses[0].mailing_address, 
        mustSellHouses[0].cost_per_sq_ft, 
        mustSellHouses[0].square_footage, 
        mustSellHouses[0].total_cost,
        &mustSellHouses[0]); // <- the address of the copy of FF4HQ inside the array
    printf("%p %p\n", &(FF4HQ.mailing_address[0]), &(mustSellHouses->mailing_address[0]));
    printf("%p %p\n", &(FF4HQ.mailing_address), &(mustSellHouses->mailing_address));
    printf("%p %p\n", &FF4HQ.mailing_address, &mustSellHouses->mailing_address);

    printf("address=%s \n$/sqft=%0.2f \nsqft=%0.2f \ncost=%0.2f \nmem_address=%p\n\n",
        backList[0].mailing_address, 
        backList[0].cost_per_sq_ft, 
        backList[0].square_footage, 
        backList[0].total_cost,
        &backList[0]); // <- the address of FF4HQ
}