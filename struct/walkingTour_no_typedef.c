#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int main() {
    struct location {
        float lat;
        float lon;
    };
    struct place {
        char name[50];
        char description[100];
        struct location location;
    };

    struct place tour[10] = { { "Walking Tour start", "Remember Me!",
                { 29.425713467226863, -98.48604518679957 } },
            { "River Center", "Shopping!", 
                { 29.423999726290614, -98.48463224568548 } },
            { "Torch", "Gift from Mexico.",
                { 29.423513261399002, -98.48745811147319 } },
            { "Tower", "Great Views, Overpriced food.",
                { 29.41902267701717, -98.48359673586972 }}};
    
    printf ("Walking tour Agenda: \n\n");

    for (int i = 0; tour[i].name[0]; i++) {
        printf ("Site %d: %s\n", i+1 , tour[i].name);
        printf ("Hint: %s\n", tour[i].description);
        printf ("Coordinates (Lat, Lon): (%5.8f, %5.8f)\n\n",
            tour[i].location.lat, tour[i].location.lon);
    }
}