#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct animal {
    char common_name[50];
    char species[50];
    char genus[50];
    float weight ;
    int population;
    int legcount;
} Animal;

int main (){
    Animal spot = { "Dog", "canine", "domesticus", 25, 1, 3};
    Animal *whiskey;

    whiskey = (Animal* ) malloc (sizeof(Animal));
    whiskey->weight = 12;
    whiskey->population = 1;
    whiskey-> legcount = 4;
    strcpy (whiskey->common_name, "cat");
    strcpy (whiskey->species, "feline");
    strcpy (whiskey->genus, "dom.");

    printf ("My animal is %s, %s, %s, %f pounds, with %d legs.\n", spot.common_name, spot.genus, spot.species, spot.weight, spot.legcount);
    printf ("My other animal is %s, %s, %s, %f pounds, with %d legs.\n", whiskey->common_name, whiskey->genus, whiskey->species, whiskey->weight, whiskey->legcount);
    
}