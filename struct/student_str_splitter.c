#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STR_LEN 100
typedef struct Student {
    char name[MAX_STR_LEN];
    int grade;
    int fav;
} student;

void print_student (student st){
    printf ("    Name: %s\n", st.name);
    printf ("   Grade: %d\n", st.grade);
    printf ("Fav rank: %d\n", st.fav);
}
// // Method iterating through the input string character by character
// student split_the_string(char * string_ptr, char delimiter){
//     int i = 0;
//     int token_start = 0;
//     student tmp_stu;
    
//     //parse out the name until the delimiter is reached 
//     char tmp_str[MAX_STR_LEN]= {0};
//     while (string_ptr[i] && string_ptr[i] != delimiter && i < MAX_STR_LEN){  // Also checking for null char and overflow
//         tmp_stu.name[i] = string_ptr[i];
//         i++;
//     }
//     tmp_stu.name[i] = 0;        // Add null terminator
//     token_start = ++i;          // Make note of where the next word starts and increment index

//     // Parse out the grade
//     while (string_ptr[i] && string_ptr[i] != delimiter && i < MAX_STR_LEN){
//         tmp_str[i-token_start] = string_ptr[i];
//         i++;
//     }
//     tmp_str[i] = 0;
//     token_start = ++i; 
//     tmp_stu.grade = atoi(tmp_str);
    
//  // parse out the favoritness
//     while (string_ptr[i] && string_ptr[i] != delimiter && i < MAX_STR_LEN){
//         tmp_str[i-token_start] = string_ptr[i];
//         i++;
//     }
//     tmp_str[i] = 0;
//     tmp_stu.fav = atoi(tmp_str);

//     print_student(tmp_stu);
//     return tmp_stu;
// }

// Another method using string library calls of strtok

student split_the_string(char * string_ptr, char delimiter){
    int i = 0;
    int token_start = 0;
    student temp_stu;
    
    char *temp_name, temp_string[MAX_STR_LEN];
    char delim_str[2] = {0};            // this line and the next convert delimiter to a char[]
    delim_str[0] = delimiter;
    strcpy(temp_string, string_ptr);    // Make a local copy of the input string (strtok is destructive)
    
    // get the first string
    temp_name = strtok(temp_string, delim_str);
    if (temp_name) {
        strcpy (temp_stu.name, temp_name);
    }
    // get the numbers
    temp_stu.grade = atoi (strtok (NULL, delim_str));
    temp_stu.fav = atoi (strtok (NULL, delim_str));
    print_student (temp_stu);
    return temp_stu;
}

int main () {
    student stu;
    stu = split_the_string ("Charlie Chaplin, 87, 50", ',');
    print_student (stu);
    stu = split_the_string ("Albert Einstein, 100, 110", ',');
    stu = split_the_string ("Dennis Meniacal : 20 : 20", ':');
}