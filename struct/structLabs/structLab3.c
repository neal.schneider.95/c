// Solution for Struct Lab Task 3

#include <stdio.h>  // for fgets
#include <string.h> // for strlen, 

int main()
{
    typedef struct          // define Name with first and last members
    {
        char first[20];
        char last[25];
    } Name;
    typedef struct          // Define Contact with name + phone#
    {
        Name name;
        char phone[20];
    } Contact;
    
    Contact list[50] = {};  // Zeroize an array of 50 contacts
    int i = 0;              // Index variable
    
    printf("Enter Contact list: \n(enter twice to exit)\n");

    while (i < 50) {        // Don't go out of array bounds
        printf("Person %d First Name: ", i+1);
        // get first name, limiting to alotted size
        fgets(list[i].name.first, 20, stdin);
        // overwrite \n with null terminator
        list[i].name.first[strlen(list[i].name.first)-1] = 0;
        // If empty, break out of loop
        if (list[i].name.first[0] == 0) break;

        printf("    Last Name: ");
        fgets(list[i].name.last, 25, stdin);
        list[i].name.last[strlen(list[i].name.last)-1] = 0;

        printf("        Phone: ");
        fgets(list[i].phone, 20, stdin);
        list[i].phone[strlen(list[i].phone)-1] = 0;

        i++;            // Increment index
    }
    int list_size = i;  // Save size for loop below

    printf("\nThe entered data is:\n");

    for (i = 0; i< list_size; i++) {        // loop to print data
        printf("%3d %20s %25s %20s\n", i+1, 
            list[i].name.first,
            list[i].name.last,
            list[i].phone);
    }
    return(0);
}