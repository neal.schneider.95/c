#include <stdio.h>

void main() {
    typedef struct my_struct_one {
        short s;
        int i;
        char c;
    } my_struct_one;

    typedef struct my_struct_two {
        int i1;
        int i2;
        char c;
    } my_struct_two;

    my_struct_one s1 = {};
    s1.s = 42;
    s1.i = 2001;
    s1.c = 'A';

    printf("the short, int, char struct: %hi %i %c\n", s1.s, s1.i, s1.c);
    printf("size of each part: %li %li %li\n", sizeof s1.s, sizeof s1.i, sizeof s1.c);
    printf("size of entire struct: %li\n\n", sizeof s1);

    char *byteArray1 = (char *)&s1; 
    printf("each byte of the short, int, char struct (displayed in base 10):\n");
    for (int i = 0; i < sizeof s1; i++) { 
        printf("%d ", byteArray1[i]); 
    }
    printf("\n\n");

    my_struct_two s2 = {};
    s2.i1 = 30;
    s2.i2 = 1775;
    s2.c = 'B';
    
    printf("the int, int, char struct: %i %i %c\n", s2.i1, s2.i2, s2.c);
    printf("size of each part: %li %li %li\n", sizeof s2.i1, sizeof s2.i2, sizeof s2.c);
    printf("size of entire struct: %li\n\n", sizeof s2);


    char *byteArray2 = (char *)&s2; 
    printf("each byte of the int, int, char struct (displayed in base 10):\n");
    for (int i = 0; i < sizeof s2; i++) { 
        printf("%d ", byteArray2[i]); 
    }
    putchar('\n');
}