#include <string.h>

struct lotForSale
{
    int lot_number;
    float lot_cost;
    float square_footage;
    char address[100];
};

int main(void)
{
    //Declare a struct variable
    struct lotForSale elmStreet = {};

    //Define the first member
    elmStreet.lot_number = 8755;
    
    //Define the second member
    elmStreet.lot_cost = 25000;
    
    //Define the third member
    elmStreet.square_footage = 6534;

    //Define the last member
    strcpy (elmStreet.address, "101 Moetown Rd., Crestview, WA 97192");
}