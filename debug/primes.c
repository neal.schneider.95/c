#include <stdio.h>
#include <stdbool.h>

void sieve_of_eratosthenes(int n) {
    bool is_prime[n + 1];
    for (int i = 0; i <= n; i++) {
        is_prime[i] = true;
    }

    for (int p = 2; p * p <= n; p++) {
        if (is_prime[p]) {
            for (int i = p * p; i <= n; i += p) {
                is_prime[i] = false;
            }
        }
    }

    printf("Prime numbers up to %d: ", n);
    for (int p = 2; p <= n; p++) {
        if (is_prime[p]) {
            printf("%d ", p);
        }
    }
    printf("\n");
}

int main() {
    int range;
    
    printf("Enter the range to find prime numbers up to: ");
    scanf("%d", &range);
    
    if (range >= 2) {
        sieve_of_eratosthenes(range);
    } else {
        printf("There are no prime numbers in the given range.\n");
    }

    return 0;
}
