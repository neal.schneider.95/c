#include <stdio.h>
#include <string.h>

// Define a struct to represent a person
struct Person {
    int age;
    char name[50];
    float height;
};

int main() {
    // Create an instance of the struct and populate it with data
    struct Person person;
    for (int i = 0; i< 50; i ++) {
        person.name[i] = 'X';
    }
    strcpy(person.name, "John Doe");
    person.age = 30;
    person.height = 6.0;

    // Open a binary file for writing
    FILE *file = fopen("person.dat", "wb");

    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    // Serialize and write the struct to the file
    size_t num_items_written = fwrite(&person, sizeof(struct Person), 1, file);

    if (num_items_written != 1) {
        perror("Error writing to file");
        fclose(file);
        return 1;
    }

    // Close the file
    fclose(file);

    printf("Serialization complete!\n");

    return 0;
}
