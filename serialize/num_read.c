#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>

#define SIZE (50)

int main() {
    int nums[SIZE];
    int nums2[SIZE];
    
    // Open a binary file for writing
    FILE *file = fopen("nums.dat", "rb");

    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }


    // Read the data to the file.  
    size_t num_items_written = fread(nums, sizeof(int) * SIZE, 1, file);

    if (num_items_written != 1) {
        perror("Error writing to file");
        fclose(file);
        return 1;
    }
    
    // Close the file
    fclose(file);

    // Convert from network to host byte ordering and print values
    for (int i = 0; i < SIZE; i++) {
        nums2[i] = ntohl(nums[i]);
        printf("%d, ", nums2[i]);
    }

    printf("\nDeserialization complete!\n");

    return 0;
}
