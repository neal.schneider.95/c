#include<stdio.h>
#include<stdlib.h>

typedef struct node
{
    int value;
    struct node *left, *right;
}Node, *Node_ptr;

Node_ptr new_node(int value)
{
    Node_ptr tmp = (Node_ptr)calloc(1,sizeof(struct node));
    tmp->value = value;
    tmp->left = tmp->right = NULL;
    return tmp;
}

void printInOrder(struct node *root_node) // displaying the nodes!
{
    if (root_node != NULL)
    {
        printInOrder(root_node->left);
        printf("%d ", root_node->value);
        printInOrder(root_node->right);
    }
}
void printPreOrder(struct node *root_node) // displaying the nodes!
{
    if (root_node != NULL)
    {
        printf("%d ", root_node->value);
        printPreOrder(root_node->left);
        printPreOrder(root_node->right);
    }
}
void printPostOrder(struct node *root_node) // displaying the nodes!
{
    if (root_node != NULL)
    {
        printPostOrder(root_node->left);
        printPostOrder(root_node->right);
        printf("%d ", root_node->value);
    }
}

int main()
{
printf("Implementation of a Binary Tree in C.\n");

struct node *root_node = NULL;
root_node = insert_node(root_node, 36);
int ints[] = { 59, 18, 9, 12, 27, 42, 67, 5, 21, 32, 40, 50, 64, 70};
for (int i = 0; i < 14; i++) 
    insert_node(root_node, ints[i]);

printf("In Order:\n");
printInOrder(root_node);

printf("\n\nPre Order:\n");
printPreOrder(root_node);

printf("\n\nPost Order:\n");
printPostOrder(root_node);

return 0;
}