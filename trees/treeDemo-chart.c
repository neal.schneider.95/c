#include<stdio.h>
#include<stdlib.h>

typedef struct node
{
    char letter;
    struct node *left, *right;
}Node, *Node_ptr;

Node_ptr new_node(char letter)
{
    Node_ptr tmp = (Node_ptr)calloc(1,sizeof(struct node));
    tmp->letter = letter;
    tmp->left = tmp->right = NULL;
    return tmp;
}

void print(struct node *root_node) // displaying the nodes!
{
    if (root_node != NULL)
    {
        print(root_node->left);
        printf("%c \n", root_node->letter);
        print(root_node->right);
    }
}

Node_ptr insert_node(Node_ptr node, char letter) // inserting nodes!
{
    if (node == NULL) return new_node(letter);
    if (letter < node->letter)
    {
        node->left = insert_node(node->left, letter);
    }
    else if (letter > node->letter)
    {
        node->right = insert_node(node->right, letter);
    }
    return node;
}
int main()
{
printf("Implementation of a Binary Tree in C.\n");

struct node *root_node = NULL;
root_node = insert_node(root_node, 36);
insert_node(root_node, 'm');
insert_node(root_node, 'e');
insert_node(root_node, 'r');
insert_node(root_node, 'o');
insert_node(root_node, 'c');
insert_node(root_node, 'a');

print(root_node);

return 0;
}