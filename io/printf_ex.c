#include <stdio.h>

void main (){
// Variable Initialization 
    int theAnswer = 54;                      // Life, universe, and everything
    float pi = 3.141592;                     // Pi
    double posSqrtTwo = 1.41421356237;       // Pos square root of 2
    char questionMark = 63;                  // A question mark
    char nickName[] = "Gilligan\0";      // A nickname
    
    // PRINTF STATEMENTS 
    printf("The answer is %d. \n", theAnswer);
    printf("Pi is approx equal to %f. \n", pi);
    printf("The positive square root of 2 is %f. \n", posSqrtTwo);
    printf("Who is %s%c \n", nickName, questionMark);
    printf("%s is. \n", nickName);
}