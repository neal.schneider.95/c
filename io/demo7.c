// demo7.c
# include <stdio.h>
# include <stdlib.h>
void main()
{
  FILE *fs;
  int i=0,x,y,c=0,sb=0,b=0;
  fs=fopen("demo7.c","r");
  if (fs==NULL)
  {
    printf("\n File opening error.");
    exit(1);
  }
  while((x=fgetc(fs))!=EOF)
  {
    switch(x)
    {
      case ';' :
      c++;
      break;
      case '{' :
      sb++;
      break;
      case '(' :
      b++;
      break;
      case '#' :
      i++;
      break;
    }
  }
  fclose (fs);
  printf("\n Summary of 'C' Program\n");
  printf("=========================");
  printf("\n Total Statements : %d ",c+i);
  printf("\n Include Statements : %d",i);
  printf("\n Total Blocks {} : %d",sb);
  printf("\n Total Brackets () : %d",b);
  printf("\n==========================");
}
