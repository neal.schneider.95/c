#include <stdio.h>

void main () {
    int intArr[6][7] = { 0 };
    int row = 0; int col = 0;

    for (row = 0; row < 6; row++)  // loop: iterates i = 0, 1, etc... to rows -1
    {
        for (col = 0; col < 7; col++)  
        {
            intArr[row][col] = (col * 10) + row;
        }
    }

    for (row = 0; row < 6; row++)  // loop: iterates i = 0, 1, etc... to rows -1
    {
        for (col = 0; col < 7; col++)  
        {
            printf("%3d", intArr[row][col]);
        }
        printf("\n");
    }


}