#include <stdio.h>
#include <string.h>

void main() {
    int arr[] = {10, 20, 100, 10000};
    printf("%lu %lu\n", sizeof (arr), sizeof(arr[0]));
    for (int i = 0; i< 4; i++) {
        printf("%d ", arr[i]);
    }
    arr[0] *= 55;
    for (int i = 0; i< 4; i++) {
        printf("%d ", arr[i]);
    }
}