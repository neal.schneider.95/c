#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define NROLLS 100000000
#define CLIP 200

int roll() {  // function definition for one roll:
    // returns the number of rolls it took to win or loose
    // win is returned as a positive, loss is negative
    short a, b;  
    short result;
    short comeOut;
    short nroll = 1, throw = 0;

    a = rand() % 6 + 1;
    b = rand() % 6 + 1;
    comeOut = a + b;

    if (comeOut == 2 || comeOut == 3 || comeOut == 12) {
        return -nroll;          // loss, so return -1
    } else if (comeOut == 7) {
        return nroll;           // win, so return +1
    } 
    while (1) {                 // Roll until you get comeOut (win) or 7 (loss)
        a = rand() % 6 + 1;
        b = rand() % 6 + 1;
        throw = a + b;
        nroll += 1;
        if (throw == comeOut)   // win, so return number of rolls
            return nroll;
        if (throw == 7)         // CRAPS!  lose, so return negative number of rolls
            return -nroll;
    }
}

void main() {
    int i = 0, wins, loss;
    int rollCount;              // number of rolls from a game
    int totalRolls = 0;         // needed for the avg game length
    int totalWins = 0;
    int totalLoss = 0;
    int counts[CLIP*2+1] = {0};     // zeroized array to collect game lengths
    /* Note that the array size is known at compile time.  This would cause a compile
    error if the expression included a variable, not a preprocessor defined constant. */
    
    char sign = '\0';               // used to print the + to show aggregation

    srand((unsigned) time(NULL));   // seed the random number generator

    // execute the games
    printf("Executing %d games of craps...\n", NROLLS);
    for (i; i<NROLLS; i++) {        // loop for the number of games to play
        rollCount = roll();         // play a game    
        if (rollCount < -CLIP) rollCount = -CLIP;   // clip the game lengths to CLIP
        if (rollCount >  CLIP) rollCount =  CLIP;
        counts[rollCount + CLIP] += 1;              // update the results array
        totalRolls += abs(rollCount);               // update total rolls
    }

    printf ("roll\twins\tloss\t%% win\n");          // print table header
    // loop through possible game lengths
    for(i=1; i<CLIP+1; i++) {
        wins = counts[CLIP+i];
        loss = counts[CLIP-i];
        totalWins += counts[CLIP+i];
        totalLoss += counts[CLIP-i];
        if (i==CLIP) sign = '+';
        if ( !wins && !loss ) continue;
        printf("%d%c\t%d\t%d\t%.2f\n", i, sign, wins, loss, (float)wins/(wins+loss));
    }
    printf("Total\t%d\t%d\t%.2f\n", totalWins, totalLoss, 
        totalWins/(float)(totalWins+totalLoss)*100);
    printf("Average Game length = %.5f\n", (float)(totalRolls)/NROLLS);
}