#include <stdio.h>
#define MAX 20
int main (void)
{
     int  Fibonacci[MAX], i;

     Fibonacci[0] = 0;    // by definition
     Fibonacci[1] = 1;    // ditto

     for ( i = 2;  i < MAX;  ++i )
          Fibonacci[i] = Fibonacci[i-2] + Fibonacci[i-1];

     for ( i = 0;  i < MAX;  ++i )
          printf ("fib %3d %i\n", i, Fibonacci[i]);

     return 0;
}