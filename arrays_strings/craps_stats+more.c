#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define NROLLS 100000

int roll() {
    // returns the number of rolls it took to win or loose
    // win is returned as a positive, loss is negative
    short a, b;  
    short result;
    short comeOut;
    short nroll = 1, throw = 0;

    a = rand() % 6 + 1;
    b = rand() % 6 + 1;
    comeOut = a + b;

    if (comeOut == 2 || comeOut == 3 || comeOut == 12) {
        return -nroll;
    } else if (comeOut == 7) {
        return nroll;
    } 
    while (1) {                 // Roll until you get comeOut (win) or 7 (loss)
        a = rand() % 6 + 1;
        b = rand() % 6 + 1;
        nroll += 1;
        throw = a + b;
        if (throw == comeOut) {
            return nroll;
        } 
        if  (throw == 7)
            return -nroll;
    }
}

void main() {
    int i = 0, wins, loss, rollcount;
    int totalwins=0, totalloss = 0, maxrolls = 0;
    int counts[42] = {0};

    srand((unsigned) time(NULL));

    for (i; i<NROLLS; i++) {
        rollcount = roll();
        if (rollcount > maxrolls) maxrolls = rollcount; 
        if (rollcount < -20) rollcount = -20;
        if (rollcount >  20) rollcount =  20;
        counts[rollcount + 20] += 1;
    }
    printf ("roll\twins\tloss\t%% win\t%% overall\n");
    for(i=1; i<=20; i++) {
        wins = counts[20+i];
        totalwins += wins;
        loss = counts[20-i];
        totalloss += loss;
        printf("%d\t%d\t%d\t%.2f\t%.2f\n", i, wins, loss, 
            wins/(float)(wins+loss)*100, 
            (float)(wins+loss)/NROLLS*100);
    }
    printf("Total\t%d\t%d\t%.2f\t%.2f\n", totalwins, totalloss, 
        totalwins/(float)(totalwins+totalloss)*100,
        (float)(totalwins+totalloss)/NROLLS*100);
    printf("Max number of rolls: %d\n", maxrolls);
}