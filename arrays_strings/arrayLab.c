
// Necessary if we want to compile the code... Need a main function for entry point.
void main () { 
    // Task 1: 
    
    float myFloats[40] = {0};
    char alphas[14] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'};
    int myInts[10]= {101, 500, 1923, 1966, 1024, 1000000, -23, 0, 67, 9999};


    // Task 2: Declare and initialize and arrays for the herd

    // Partially initialize to ensure that the remaining spaces are zeros
    char cowType[100] = {'B', 'C', 'C', 'C', 'C', 'b', 'h'};

    // Partially initialize to ensure that the remaining spaces are zeros
    int cowWeight[100] = {1650, 1200, 1200, 1100, 1300, 350, 290};

    // Task 2b:   
    // Spring comes and goes... and the cattle put on weight
    
    cowWeight[0] = cowWeight[0] + 50;
    cowWeight[1] = cowWeight[1] + 50;
    cowWeight[2] = cowWeight[2] + 50;
    cowWeight[3] = cowWeight[3] + 50;
    cowWeight[4] = cowWeight[4] + 50;
    cowWeight[5] = cowWeight[5] + 50;
    cowWeight[6] = cowWeight[6] + 50;

    /* // or using a loop, we could do the same thing:
    for (int i = 0; i < 7; i++) {
        cowWeight[i] += 50;
    }
    */
   
    // and have calves!

    cowType[7] = 'b';
    cowWeight[7] = 170;
    cowType[8] = 'h';
    cowWeight[8] = 170;
    
    // Bonus:
    #include <stdio.h>                  // include the library where printf is defined
    int cowCount = 0;                   // cow counter
    int totalWeight = 0;                // weight accumulator
    printf("Herd stats:\n");
    for (int i = 0; i < 100 ; i++) {     // iterate i from 0 to 11
        if (cowType[i] != 0) {          // if there's a cow in the array at index i, 
            printf("%c: %d lbs\n", cowType[i], cowWeight[i]);   // print the type and weight
            cowCount ++;                // count the cows
            totalWeight += cowWeight[i];// add weight to total for average    
        } // end if
    } // end for
    printf("Current heard size is %d head.\n", cowCount);
    printf("Total weight: %d\n", totalWeight);
    printf("Average weight is %.2f\n", (float) totalWeight/cowCount);
}