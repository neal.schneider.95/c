---
title: Array Lab Solutions
sidebar_position: 1
---

## Student Performance Labs

## Fill in the blanks in each of the following

C stores lists of values in __arrays__.

The elements of an array are related by the fact that they __have the same type__.

When referring to an array element, the position number contained within square brackets is called a(n) __index__.

The names of the five elements of array p are __p[0]__, __p[1]__, __p[2]__, __p[3]__, __p[4]__.

The contents of a particular element of an array is called the __value__ of that element.

Naming an array, stating its type and specifying the number of elements in the array is called __declaring__-the array.

The process of placing the elements of an array into either ascending or descending order is called __sorting__.

In a double-subscripted array, the first subscript identifies the __row__ of an element and the second subscript identifies the __column__ of an element.

An m-by-n array contains __m__ rows, __n__ columns and __m * n__ elements.

To assign a value to the element in row 3 and column 5 of array d write : __```d[3][5] = value```__.

## State which of the following are true and which are false. If false, explain why

- To refer to a particular location or element within an array, we specify the name of the array and the value of the particular element.
  - False: an element is referenced by its index, not its value.

- An array definition reserves space for the array.
  - True

- To indicate that 100 locations should be reserved for integer array p, write:
  
```c
p[100];
```

- False it should be:

```c
int p[100];
```

- A C program that initializes the elements of a 15-element array to zero must contain one for statement.
  - False:  It could be written without one like this:

```c
int a[15] = {0};
```

- A C program that totals the elements of a double-subscripted array must contain nested for statements.
  - False:  while or do-while loops could be used as well.

- The mean, median and mode of the following set of values are 5, 6 and 7, respectively: 1, 2, 5, 6, 7, 7, 7.
  - mean: 5 (average)
  - median: 6 (middle one, of the sorted list)

## Write statements to accomplish each of the following

- Display the value of the seventh element of character array f.

```c
printf("%d", f[7]);
```

- Input a value into element 4 of single-subscripted floating-point array b.

```c
b[4] = value;
```

- Initialize each of the five elements of single-subscripted integer array g to 8.

```c
int g[] = {8, 8, 8, 8, 8};
```

- Total the elements of floating-point array c of 100 elements.

```c
float sum = 0;
for(int i = 0; i<100; i++) {
    sum += c[i];
}
```

- Copy array a into the first portion of array b. Assume double a[11], b[34];

```c
for(int i = 0; i < 11; i++) {
    b[i] = a[i];
}
```

- Determine and print the smallest and largest values contained in 99-element floating-point array w.

```c
int small = w[0];
int large = w[0];
for(int i = 1; i < 99; i++) {
    if (small > w[i]) {
        small = w[i];
    } else if (large < w[i]) {
        large = w[i];
    } // end if-else
}  // end for
print("Smallest = %f, Largest = %f.\n", small, large);
```

## Find the error(s) in each of the following statements

a.

```c
Assume: char str[5];
    scanf("%s", str); //user types hello
```

- Has a very real potential for a overflow error if the user types in more than 5 characters
- Need to limit input to 5 chars

```c
    scanf("%5s", str); //user types hello
```

b.

```c
Assume: int a[3];
    printf( "$d  %d  %d\n", a[1], a[2], a[3])
```

- $ should be a %

c.

```c
double f[3] = {1.1, 10.01, 100.001, 1000.0001};
```

- declared as size 3, but given 4 values

d.

```c
Assume: double d[2][10];
    d[1, 9] = 2.345
```

- should be d[1][9] = 2.345;

## (Game of Craps)

__[solution: arrayLab_craps_stats.c](./arrayLab_craps_stats.c)__

Write a program that runs 1000 games of craps (without human intervention) and answers each of the following questions:

- How many games are won on the first roll, second roll, …, twentieth roll and after the twentieth roll?

- How many games are lost on the first roll, second roll, …, twentieth roll and after the twentieth roll?

- What are the chances of winning at craps? [Note: You should discover that craps is one of the fairest casino games. What do you suppose this means?]

- What’s the average length of a game of craps?

- Do the chances of winning improve with the length of the game?

## (Airline Reservations System)

__[solution: arrayLab_airline.c](./arrayLab_airline.c)__

A small airline has just purchased a computer for its new automated reservations system. The president has asked you to program the new system. You’ll write a program to assign seats on each flight of the airline’s only plane (capacity: 10 seats).

Your program should display the following menu of alternatives:

```text
    Please type 1 for "first class"
    Please type 2 for "economy"
```

If the person types 1, then your program should assign a seat in the first class section (seats 1–5). If the person types 2, then your program should assign a seat in the economy section (seats 6–10). Your program should then print a boarding pass indicating the person’s seat number and whether it’s in the first class or economy section of the plane.

Use a single-subscripted array to represent the seating chart of the plane. Initialize all the elements of the array to 0 to indicate that all seats are empty. As each seat is assigned, set the corresponding element of the array to 1 to indicate that the seat is no longer available.

Your program should, of course, never assign a seat that has already been assigned. When the first class section is full, your program should ask the person if it’s acceptable to be placed in the economy section (and vice versa). If yes, then make the appropriate seat assignment. If no, then print the message “Next flight leaves in 3 hours.”
