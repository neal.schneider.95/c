#include <stdio.h>          
#include <stdlib.h>         // for atoi
#include <ctype.h>          // for iscntrl
#include <string.h>         // for strlen

int main () {
    char input[10];         // input buffer
    int value;              // integer value of input

    printf("Enter numbers to convert to ascii\n: ");
    fgets(input, 10, stdin);                // get string from user
    while(strlen(input)>1) {                // quit if emtpy ("\n")
        value = atoi(input);                // convert str to value
        if (value > 0 && value < 128) {     // check bounds
            if (iscntrl(value)) {           // is it a control char?
                printf("%d is a control character. \n---\"%c\"---\n: ", value, value);
            } else {                        // no, so print it
                printf("The ascii char for %d is \"%c\"\n: ", value, value);
            }
        } else {                            // out of bounds, so try again.
            printf("%d is not a valid ascii code.\n: ", value);
        }
        fgets(input, 10, stdin);            // get next value
    }
    printf("I hope this was as much fun for you as it was for me!!\n");
    return 0;
}