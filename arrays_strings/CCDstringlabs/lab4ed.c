#include <stdio.h>          
#include <string.h>         // for strlen

int main () {
    char input[101];         // input buffer
    int pos;
    int len;
    printf("Am I dead?\n: ");
    while (1) {                
        fgets(input, 100, stdin);               // get string from user
        len = strlen(input);                    // convert to int
        if (len == 1) break;                    // quit if empty ("\n")
        
        // since we used fgets, the input[len] is \0 and input[len-1] is \n
        // so we will be looking for ed at len-3 and len-3
        if (len > 3 && input[len-3] == 'e' && input[len-2 == 'd']) {
            printf("%s: ", input);              // echo string
        } // end if
    } // end while
    printf("Thanks for playing.\n");
    return 0;
} // end main