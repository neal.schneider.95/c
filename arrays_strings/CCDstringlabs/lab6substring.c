#include <stdio.h>
#include <string.h>     // where strstr and strlen are defined 

int main() {
    char *searchPtr;    // Pointer to use to get each found term
                        // After it's assigned, it *is* a string
    char phrase[1024] = {0};
    char searchStr[100] = {0};
    printf("Enter a phrase to search: ");
    fgets(phrase, 1024, stdin);                 // get the phrase
    phrase[strlen(phrase)-1] = 0;

    printf("What's the serach phrase? ");
    fgets(searchStr, 100, stdin);              // get the search string
    searchStr[strlen(searchStr)-1] = 0;

    searchPtr = strstr(phrase, searchStr);
    // loop until word comes back empty (NULL)
    while (searchPtr) {

        // print the current string, starting where searchStr was found, at searchPtr
        printf("%s\n", searchPtr);

        searchPtr = strstr(searchPtr+1, searchStr);
    }  // end while
}  // end main

/*
Output:
$ gcc substring.c
$ ./a.out
Enter a phrase to search: now good men... let's look for me...  Don't bother searching for meteorites!
What's the serach phrase? me
men... let's look for me...  Don't bother searching for meteorites!
me...  Don't bother searching for meteorites!
meteorites!
*/