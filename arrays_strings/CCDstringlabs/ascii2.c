#include <stdio.h>
#include <ctype.h>

void main () {
    for (int row = 0; row < 32; row++) {
        printf("%2d ", row);
        for(int col = 0; col < 8; col++) {
            if (iscntrl(col * 64 + row)) {
                printf("\\%2x   |", col * 64 + row );
            } else {
                printf("%c   |", col * 64 + row );
            }
        }
        printf("\n");
    }
}