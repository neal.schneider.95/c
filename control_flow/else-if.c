#include <stdio.h>

void main() {
    int day;
    printf("Enter a numeric day of the week (0-6): ");
    scanf("%d", &day);

    if (day == 0) {
        printf("Sunday\n");
    } else if (day == 1) {
        printf("Monday\n");
    } else if (day == 2) {
        printf("Tuesday\n");
    } else if (day == 3) {
        printf("Wednesday\n");
    } else if (day == 4) {
        printf("Thursday\n");
    } else if (day == 5) {
        printf("Friday\n");
    } else if (day == 6) {
        printf("Saturday\n");
    } else {
        printf("ERROR: This should be impossible!\n");
    }
}
