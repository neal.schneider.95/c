#include <stdio.h>

void main() {
    unsigned int num;
    printf("Enter a positive integer: ");
    scanf("%u", &num);

    do {
        printf("%d\n", num);
        num >>= 1;
    } while (num);
}