#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

struct numNode *buildCList(int *nums, int size)
{
    //check for nums null
    if(nums == NULL){
        return NULL;
    }

    //check for valid size
    else if(size < 1){
        return NULL;
    }

    int count = 0;
    //track head and tail
    struct numNode *head;
    struct numNode *tail;
    //created a new array to store used numbers 
    int mynums[size];

    while(count < size){
        //iterating through my array to see if we have 
        //used the number before
        for(int i = 0; i < (sizeof(mynums)/sizeof(mynums[0])); i++){
            if(nums[count]==mynums[i]){
                continue;
            }
        }
        //creating a new node pointer
        struct numNode *newNode= malloc(sizeof(struct numNode));
        //if head is null set new node equal to head 
        if (head == NULL){
            //sets data 
            newNode->num = nums[count];
            head= newNode;
            tail= newNode;
            //stores used number
            mynums[count] = nums[count];
        }
        else{
            //creates a node at the head of the list 
            newNode->num = nums[count];
            newNode->next = head;
            head = newNode;
            tail->next = head;
            //stores used number
            mynums[count] = nums[count];
        }
        count++;
    }
    
    //returns pointer to the head variable
    return head;
}
//empty the list and free the pointers
int emptyList(struct numNode *head)
{
    //track nodes removed
    int numremoved = 0;
    //return 0 if head is null
    if(head==NULL){
        return 0;
    }
    struct numNode *newNode;
    struct numNode *temp;
    newNode = head->next;
    //traverse the nodes and null them out 
    while(newNode!=head){
        temp = newNode;
        temp=NULL;
        newNode = newNode->next;
        numremoved++;
    }
    head = NULL;
    numremoved++;
    free(temp);
    free(newNode);
    free(head);
    return numremoved;
}

//testing my own stuff
int main(){
    struct numNode *head;
    struct numNode *probe;
    int nums2[] = { 90,80,70,60,50};
    head = buildCList(nums2, 6);
    probe = head;
    while(probe->next!=head){
        printf("number = %d\n", probe->num);
        probe = probe->next;
    }
    int x = 0;
    x = emptyList(head);
    printf("%d", x);
}
