cmake_minimum_required(VERSION 3.14)

project(TestCode)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 11)

set(CMAKE_C_FLAGS_RELEASE "-O0 -g -Wall -fsanitize=address -fsanitize=leak -fno-omit-frame-pointer")
set(CMAKE_CXX_FLAGS_RELEASE "-O0 -g -Wall -fsanitize=address -fsanitize=leak -fno-omit-frame-pointer")
set(CMAKE_C_FLAGS_DEBUG "-O0 -g -Wall")

include(FetchContent)
FetchContent_Declare(
  googletest
  URL https://github.com/google/googletest/archive/609281088cfefc76f9d0ce82e1ff6c30cc3591e5.zip
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

enable_testing()

if (EXISTS "Solution/TestCode.c")
    set(TEST_SOURCES
        testcases.cpp
        Solution/TestCode.c
    )
    set(TEST_HEADERS
        TestCode.h
    )
else()
    set(TEST_SOURCES
        testcases.cpp
        TestCode.c
    )
    set(TEST_HEADERS
        TestCode.h
    )
endif()

add_executable(
    TestCode 
    ${TEST_SOURCES}
)

target_link_libraries(
    TestCode
    gtest_main
)

include(GoogleTest)
gtest_discover_tests(TestCode)
