#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"
// #define RUN_LOCAL 1   //ns added to control compiling of main and printList

// Refer to README.md for the problem instructions

// PROTOTYPES
int checkDuplicates(struct numNode *head, int value);
void printList(struct numNode *head); // ns added

struct numNode *buildCList(int *nums, int size)
{
    if (sizeof(size) == 0 || nums == NULL) 
    //ns sizeof(size) gives you the amount of memory size uses and will never be 0
    {
        return NULL;
    }

    // create head
    //ns struct numNode *head = (struct numNode *)malloc(sizeof(struct numNode));
    //ns this creates a node that is immediately abandoned, creating a mem leak
    struct numNode *head;  //ns added
    head = NULL;

    // loop through items in list
    for (int i = 0; i < size; i++)
    {
        // create new node
        struct numNode *newNode = (struct numNode *)malloc(sizeof(struct numNode));
        // assign the looped number to the nodes value
        newNode->num = nums[i];

        // check if head is empty
        if (head == NULL)
        {
            // if empty assing new node as head
            head = newNode;
            // since its only one item, point heads->next to head
            head->next = head;
        }
        else
        {
            int bl = checkDuplicates(head, nums[i]);
            // check for duplicates
            if (bl == 2)
            {
                // append new item
                newNode->next = head;
                // traverse to end of list
                // struct numNode *probe = (struct numNode *)malloc(sizeof(struct numNode));
                struct numNode *probe; //ns don't create a new node 
                probe = head;
                while (probe->next != head)
                {
                    probe = probe->next;
                }
                probe->next = newNode;
                // update the head
                head = newNode;
            }
        }
    }
    return head;
}

int emptyList(struct numNode *head)
{
    if (head == NULL)
    {
        return 0;
    }
    int freedNodes = 0;

    if (head->next == head)
    {
        free(head);
        freedNodes++;
    }
    else
    {
        //ns struct numNode *probe; = (struct numNode *)malloc(sizeof(struct numNode));
        struct numNode *probe;//ns you don't want to create a node here.
        probe = head->next;  
        while (probe->next != head)
        {
            struct numNode *temp = probe;
            probe = probe->next;
            free(temp);
            freedNodes++;
        }
        free(probe);
        free(head);
        // freedNodes = +2; //ns
        freedNodes += 2;  //ns Here's the problem.... the plus was on the wrong side of the equals.
    }
    return freedNodes;
}

int checkDuplicates(struct numNode *head, int value)
{
    //ns struct numNode *probe = (struct numNode *)malloc(sizeof(struct numNode));
    struct numNode *probe; //ns you don't want to create a node here. (mem leak)
    probe = head;
    //ns while (probe->next != head)
    //ns if there is one node in the list, this will be false and it will allow 
    // the first and second nodes to be the same, so you should have a separate
    // check before the loop to catch that condition.  This will also terminate
    // one before the end, not examining the last item, so a do loop may work better.
    // if (!head && head->next == head) {
    //     return (head->num == value) ? 1: 2;
    // }
    do {
        if (probe->num == value)
        {
            return 1;
        }
        probe = probe->next;
    } while (probe->next != head);
    if (probe->num == value) return 1; // ns check the last value
    return 2;
    // return 1 if item already exists
    // return 2 if items doesn't exist
    //ns Although this return value works, traditionally functions that answer a true/false 
    //ns question are in the form ```int is_duplicate(...)```, with the return values
    //ns 0==False, 1(or anything else)==True.  That way, the calling command is easy to read:
    //ns //....
    //ns       if (!is_duplicate(head, nums[i])) {
    //ns //....
}


void printList(struct numNode *head)
{
	int count = 0;
	if (!head)
	{
		printf("Empty List\n");
		return;
	}
	struct numNode *link = head;
	do
	{
		printf("%d -> ", link->num);
		link = link->next;
		count += 1;
		// sleep(.05);
	} while (link != head);
	printf("[end (%d)]\n", count);
}
#ifdef RUN_LOCAL
void main (){
	int nums[] = {1, 2, 3, 4, 5};
	int num2[] = {1001, 1002, 1003, 1004, 1005};

	struct numNode *a = NULL;
	struct numNode *t = (struct numNode *)malloc(sizeof(struct numNode));
	printList(a);
	t->num = 1;
	t->next = t;
	printList(t);

	t->next = (struct numNode *)malloc(sizeof(struct numNode));
	t->next->num  = 2;
	t->next->next = t;
	printList(t);
	// // t = ins_node(t, 2);
	// printList(t);
	// t = ins_node(t, 1);
	// printList(t);

	// a = ins_node(a, 20);
	// printList(a);
	// a = buildCList(nums, 5);
	// printList(a);

	// a = ins_node(a, 21);
	// printList(a);

	a = buildCList(NULL, 1);
	printList (a);
    printf("1. Freed %d nodes\n", emptyList(a));

	a = buildCList(nums, 1);
	printList(a);
    printf("2. Freed %d nodes\n", emptyList(a));

	a = buildCList(nums, 5);
	printList(a);
    printf("3. Freed %d nodes\n", emptyList(a));

	a = buildCList(num2, 0);
	printList(a);
    printf("4. Freed %d nodes\n", emptyList(a));

	a = buildCList(num2, 5);
	printList(a);
    printf("5. Freed %d nodes\n", emptyList(a));


    int nums4[] = {5, 7,8,7,9,2,34,5,54,1};
    struct numNode* res = buildCList(nums4,10);
    printList(res);
    printf("6. Freed %d nodes\n", emptyList(res));
}
#endif