// Neal Schneider 6 Aug 22
// IDF Instructor
// Subject matter test verification

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "TestCode.h"
    // #define RUNLOCAL

    // Refer to README.md for the problem instructions

    int
    push(struct numNode **top, int data)
{
    if (top == NULL) 
    { 
        return 1;
    }
    struct numNode *newNode = (struct numNode*) malloc(sizeof(struct numNode));
    if (newNode) {
        newNode->next = *top;
        newNode->num = data;
        *top = newNode; 
        return 0;
    } else {
        // malloc failed... likely never to happen here.
        return 1;
    }
}

int pop(struct numNode **top)
{
    if (!top || !(*top)) return 0;
    int data = (*top)->num;
    struct numNode *tmp = *top;
    *top = (*top)->next;
    free (tmp);
    return data;
}

void emptyStack(struct numNode **top)
{
    if (!top || !(*top)) return;
    struct numNode *tmp;
    while (*top) {
        tmp = (*top)->next;
        free(*top);
        *top = tmp;
    }
    return;
}

struct numNode *createStack(int actions[], int numActions)
{
    struct numNode *newStack = NULL;
    for (int i =0; i<numActions*2; i+=2){
        if (actions[i] == 1) {
            pop(&newStack);
        }
        else if (actions[i] == 2) {
            push(&newStack, actions[i + 1]);
        }
        else if  (actions[i] == 3) {
            emptyStack(&newStack);
        }
        else {
            return NULL;
        }
    }
    return newStack;
}

#ifdef RUNLOCAL
void printStack(struct numNode **top)
{
    if (!top || !(*top)){
        printf("[Empty stack]\n");
        return;
    }
    struct numNode *link = *top;
    while (link)
    {
        printf("%d -> ", link->num);
        link = link->next;
    }
    printf("[end]\n");
    return;
}


void main() {
    struct numNode *a = NULL;
    int i = push(NULL, 7734);
    printf ("push(NULL) returns %d\n", i);
    printStack(&a);
    push(&a, 10);
    printStack(&a);
    push(&a, 20);
    printStack(&a);
    printf("Popped %d\n", pop(&a));

    printf("PushCase\n");
    int actions[] = {2, 4, 2, 5, 2, 10};
    struct numNode *res = createStack(actions, 3);
    struct numNode *top = res;
    printStack (&res);
}
#endif