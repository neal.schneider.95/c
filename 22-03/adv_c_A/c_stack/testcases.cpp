#include <gtest/gtest.h>
#include "TestCode.h"

TEST(Push_Tests, zeroCase)
{
    int data = 0;
    struct numNode *top = NULL;
    EXPECT_EQ(0, push(&top, data));
    EXPECT_FALSE(NULL == top);
    EXPECT_EQ(data, top->num);
    EXPECT_TRUE(NULL == top->next);
    free(top);
    top = NULL;
}

TEST(Push_Tests, belowZeroCase)
{
    int data = -1;
    struct numNode *top = NULL;
    EXPECT_EQ(0, push(&top, data));
    EXPECT_FALSE(NULL == top);
    EXPECT_EQ(data, top->num);
    EXPECT_TRUE(NULL == top->next);
    free(top);
    top = NULL;
}

TEST(Push_Tests, aboveZeroCase)
{
    int data = 1;
    struct numNode *top = NULL;
    EXPECT_EQ(0, push(&top, data));
    EXPECT_FALSE(NULL == top);
    EXPECT_EQ(data, top->num);
    EXPECT_TRUE(NULL == top->next);
    free(top);
    top = NULL;
}

TEST(Push_Tests, nullCase)
{
    int data = 7734;
    EXPECT_EQ(1, push(NULL, data));
}

TEST(Pop_Tests, zeroCase)
{
    int data = 0;
    struct numNode *top = NULL;
    EXPECT_EQ(0, push(&top, data));
    EXPECT_FALSE(NULL == top);
    EXPECT_EQ(data, pop(&top));
    EXPECT_TRUE(NULL == top);
}

TEST(Pop_Tests, belowZeroCase)
{
    int data = -1;
    struct numNode *top = NULL;
    EXPECT_EQ(0, push(&top, data));
    EXPECT_FALSE(NULL == top);
    EXPECT_EQ(data, pop(&top));
    EXPECT_TRUE(NULL == top);
}

TEST(Pop_Tests, aboveZeroCase)
{
    int data = 1;
    struct numNode *top = NULL;
    EXPECT_EQ(0, push(&top, data));
    EXPECT_FALSE(NULL == top);
    EXPECT_EQ(data, pop(&top));
    EXPECT_TRUE(NULL == top);
}

TEST(Pop_Tests, nullCase1)
{
    EXPECT_EQ(0, pop(NULL));
}

TEST(Pop_Tests, nullCase2)
{
    struct numNode *top = NULL;
    EXPECT_TRUE(NULL == top);
    EXPECT_EQ(0, pop(&top));
}

TEST(EmptyStack_Tests, oneNodeCase)
{
    int data = 7;
    struct numNode *top = NULL;
    EXPECT_EQ(0, push(&top, data));
    EXPECT_FALSE(NULL == top);
    EXPECT_EQ(data, top->num);
    EXPECT_TRUE(NULL == top->next);

    emptyStack(&top);
    EXPECT_TRUE(NULL == top);
}

TEST(EmptyStack_Tests, twoNodesCase)
{
    int data[] = {1, 3};
    int sz_data = 2;
    struct numNode *top = NULL;
    struct numNode *bottom = NULL;
    for (int i = 0; i < sz_data; i++)
    {
        EXPECT_EQ(0, push(&top, data[i]));
        EXPECT_FALSE(NULL == top);
        EXPECT_EQ(data[i], top->num);
        if (i == 0)
        {
            EXPECT_TRUE(NULL == top->next);
            bottom = top;
        }
        else
        {
            EXPECT_TRUE(bottom == top->next);
        }
    }
    emptyStack(&top);
    EXPECT_TRUE(NULL == top);
    bottom = NULL;
}

TEST(EmptyStack_Tests, threeNodesCase)
{
    int data[] = {1, 3, 6};
    int sz_data = 3;
    struct numNode *top = NULL;
    struct numNode *bottom = NULL;
    for (int i = 0; i < sz_data; i++)
    {
        EXPECT_EQ(0, push(&top, data[i]));
        EXPECT_FALSE(NULL == top);
        EXPECT_EQ(data[i], top->num);
        if (i == 0)
        {
            EXPECT_TRUE(NULL == top->next);
            bottom = top;
        }
        else if (i == 1)
        {
            EXPECT_TRUE(bottom == top->next);
        }
        else
        {
            EXPECT_FALSE(bottom == top->next);
        }
    }
    emptyStack(&top);
    EXPECT_TRUE(NULL == top);
    bottom = NULL;
}

TEST(CreateStack_Tests, pushCase1)
{
    int i;
    int actions[] = { 2, 4, 2, 5, 2, 10 };
    struct numNode *res = createStack(actions, 3);
    struct numNode *top = res;
    int test[] = { 10, 5, 4 };
    EXPECT_FALSE(NULL == res); //res should not be NULL
    for (i = 0; res != NULL; i++, res = res->next)
    {
        EXPECT_EQ(test[i], res->num);
    }
    EXPECT_EQ(3, i); // Should have tested 3 items
    emptyStack(&top);
    EXPECT_TRUE(NULL == top);
}

TEST(CreateStack_Tests, pushEmptyStackCase1)
{
    int actions[] = { 2, 4, 2, 5, 2, 10, 3, 0 };
    struct numNode *res = createStack(actions, 4);
    EXPECT_TRUE(NULL == res);
}

TEST(CreateStack_Tests, pushPopCase1)
{
    int i;
    int actions[] = { 2, 7, 2, 4, 2, 5, 1, 0, 2, 10, 1, 0 };
    struct numNode *res = createStack(actions, 6);
    struct numNode *top = res;
    EXPECT_FALSE(NULL == res); //res should not be NULL
    int test2[] = { 4, 7 };
    for (i = 0; res != NULL; i++, res = res->next)
    {
        EXPECT_EQ(test2[i], res->num);
    }
    EXPECT_EQ(2, i); // Should have tested 2 items
    emptyStack(&top);
    EXPECT_TRUE(NULL == top);
}

TEST(CreateStack_Tests, pushPopCase2)
{
    int actions[] = { 2, 4, 2, 5, 2, 10, 1, 0, 1, 0, 1, 0 };
    struct numNode *res = createStack(actions, 6);
    EXPECT_TRUE(NULL == res);
}

TEST(CreateStack_Tests, pushPopCase3)
{
    int i;
    int actions[] = { 2, 4, 2, 5, 2, 10, 1, 0, 1, 0, 1, 0, 2, 50 };
    struct numNode *res = createStack(actions, 7);
    struct numNode *top = res;
    EXPECT_FALSE(NULL == res); //res should not be NULL
    int test3[] = { 50 };
    for (i = 0; res != NULL; i++, res = res->next)
    {
        EXPECT_EQ(test3[i], res->num);
    }
    EXPECT_EQ(1, i); // We should have tested 1 item
    emptyStack(&top);
    EXPECT_TRUE(NULL == top);
}
