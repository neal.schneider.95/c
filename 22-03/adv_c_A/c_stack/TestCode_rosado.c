#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

int push(struct numNode **top, int data)
{
    if (top == NULL)
    {
        return 1;
    }

    struct numNode *newNode = (struct numNode *)malloc(sizeof(struct numNode));

    newNode->num = data;
    newNode->next = (*top);
    (*top) = newNode;

    return 0;
}

int pop(struct numNode **top)
{

    if (top == NULL || *top == NULL)
    {
        return 0;
    }

    struct numNode *curr = *top;

    int result = curr->num;
    (*top) = (*top)->next;

    free(curr);

    return result;

    
}

void emptyStack(struct numNode **top)
{
    if (top == NULL || *top == NULL)
    {
        return;
    }

    struct numNode *curr = *top;
    while (curr->next != NULL)
    {
        struct numNode *tmp = curr;
        curr = curr->next;
        free(tmp);
    }

    free(curr);
    *top = NULL;
    top = NULL;
}

struct numNode *createStack(int actions[], int numActions)
{
    struct numNode *top = NULL;   

    for (size_t i = 0; i < numActions*2; i+=2)
    {
        switch (actions[i])
        {
            case 1: 
                pop(&top);
                break;
            case 2:
                push(&top,actions[i+1]);
                break;
            case 3:
                emptyStack(&top);
                break;
            default:
                break;
        }
    }    

    return top;
}
