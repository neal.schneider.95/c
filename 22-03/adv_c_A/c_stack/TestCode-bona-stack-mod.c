#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

/* //ns: Overall, Good job.  The code is readable and it's clear what you're doing.  
   However, there are no comments at all. (-5) There is also a memory leak.
   Valgrind reports that there are 5 unfreed blocks at the end of the 17 tests. 
   (no points deducted though.)  

   grade for this problem: 95%
*/

int push(struct numNode **top, int data)
{
    if (top == NULL)
    {
        return 1;
    }

    struct numNode *newNode = (struct numNode*) malloc(sizeof(struct numNode));
    newNode->num = data;

    newNode->next = (*top);

    (*top) = newNode;

    return 0;
}

int pop(struct numNode **top)
{
    if (top==NULL)
    {
        return 0;
    }
    if((*top)==NULL)
    {
        return 0;
    }
    struct numNode *temp = (*top);
    int num;
    (*top) = (*top)->next;
    num = temp->num;
    free(temp);
    return num;
}

void emptyStack(struct numNode **top)
{
    // if (top == NULL)
    // {
    //     return 0;
    // }
    while (pop(top) != 0) //ns clever implementation...
    //ns However, you should not check for 0 as a return value to see 
    //  if the stack is empty, because a 0 could be stored as a value.  A better 
    //  indicator would be if *top points to NULL.  This may be where a 
    //  memory leak occurs because it stops popping while there is data on the stack.
        ;
}

struct numNode *createStack(int actions[], int numActions)
{
    // struct numNode *head = (struct numNode*) malloc(sizeof(struct numNode)); //ns
    struct numNode *head;  //ns No need to allocate here since it starts out NULL
    head = NULL;

    for (int x = 0; x < (numActions*2); x+=2)
    {
        // pop
        if (actions[x] == 1)
        {
            pop(&head);
        }
        // push
        else if (actions[x] == 2)
        {
            push(&head, actions[x+1]);
        }
        // empty stack
        else if (actions[x] == 3)
        {
            emptyStack(&head);
        }
        else
        {
            return NULL;
        }
    }
    return head;
}
