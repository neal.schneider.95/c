#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions
/**
 * @brief pushes a new item onto the stack
 * 
 * @param top a pointer to the top pointer of the stack
 * @param data an integer representing the new data to be added to the stack.
 * @return int 0 if sucessful or 1 if either top was null or memory was unavailable
 */
numNode* createNode(int data, numNode* next)
{
    numNode* new_Node = (numNode*)calloc(1,sizeof(numNode));
    //check and make sure that the memory was able to be allocated
        if (new_Node == NULL){
            return NULL;
        }
        //set the data and pointer to the old top
        new_Node->next = next;
        new_Node->num = data;
        return new_Node;
}

int push(struct numNode **top, int data)
{   if(top != NULL)
    {
        //create new node
        numNode* new_top = createNode(data,*top);
        
        //check for proper allocation
        if(new_top !=NULL)
        {
            //set top pointer to new top
            *top = new_top;
            return 0;
         }
    }
    return 1;
}
/**
 * @brief returns the top most nodes data and removes it from the stack
 * 
 * @param top a pointer to a numNode pointer
 * @return int the data stored in the top of the stack
 */
int pop(struct numNode **top)
{
    if(top != NULL && *top != NULL){
       //creat a temp pointer to top
        numNode* del_ptr = *top;

        //hold the top data
        int rtn_Int = (*top)->num;

        //set top to the next node in the stack
        *top =  (*top)->next;
        
        //zeroize node data
        del_ptr->num = 0;

        //free up the node memory
        free(del_ptr);  
        
        //return the data
        return rtn_Int;
    }
        //return 0 if they are trying to pop and empty stack
        return 0;
    
}
/**
 * @brief emptyies the stack freeing all memory and setting seting top pointer to null
 * 
 * @param top a pointer to a numNode pointer
 */
void emptyStack(struct numNode **top)
{
    //make sure top is valid
    if(top != NULL){
       while(*top != NULL)
        {   //create temp pointer
            numNode* del = *top;
            *top = (*top)->next;
            //zeroize data
            del->num = 0;
            //free node
            free(del);  
            
        }  
    }
}

/**
 * @brief Creates a stack based on the data sent to it and returns the top of the stack when finished.
 * 
 * @param actions array holding the action sequence on even indicies. 1 = pop, 2= push, 3=empty list
 * @param numActions the number of actions in the array
 * @return struct numNode* pointer that represents the top of the stack or NULL if an invalid action or data is porvided
 */
struct numNode *createStack(int actions[], int numActions)
{   
    numNode* top = NULL;
    //make sure action isn't null
    if( actions != NULL){
        //calc size of array
        int actionSize = numActions * 2;
        //loop through all the actions
        for(int i = 0; i< actionSize; i+=2)
        {
            switch (actions[i])
            {
            case 1://pop
                pop(&top);
                break;
            
            case 2://push
                push(&top,actions[i+1]);
                break;
            
            case 3://empty the stack
                emptyStack(&top);
                break;
            
            default://invalid action
                return NULL;
            }
        }
        //since no errors occured return top
        return top;
    }
    //tried to send a null array returning  Null
    return NULL;
}
