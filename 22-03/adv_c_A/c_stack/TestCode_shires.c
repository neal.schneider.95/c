#include <stdio.h>
#include <string.h>
#include<stdlib.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions
//creates a new node/pushes it onto the top of the stack
int push(struct numNode **top, int data)
{
    //if top is null it didn't work return 1
    if(*top == NULL){
        return 1;
    }
    //if data is null return 1
    if(data == NULL){
        return 1;
    }
    //create a new node to push
    struct numNode *newnode = malloc(sizeof(struct numNode));
    //if unable to create node fails
    if(newnode == NULL){
        return 1;
    }
    //give new node data to hold
    newnode->num = data;
    
    //connect new node to the rest of the stack
    newnode->next = *top;
    //new node is new top of stack
    *top = newnode;
    
    //returns 0 if successful
    return 0;
    
}

int pop(struct numNode **top)
{
    //checks if stack is empty
    if(top == NULL){
        printf("Stack is Empty\n");
        return 1;
    }
    //temp node 
    struct numNode *temp;
    //temp node = top 
    temp = *top;
    //moving top to next node in the stack
    *top = temp->next;
    //storing data from temp to return
    int data = temp->num;
    //temps job is done free it 
    free(temp);
    //return data 
    return data;
}

void emptyStack(struct numNode **top)
{
    //temp to set to top
    struct numNode *temp;
    temp = *top;
    //is the stack empty
    if(temp == NULL){
        printf("Stack is Empty\n");
    }
    else{
        //while temp is not null continue to pop off
        while(temp->next!= NULL){
            pop(&temp);
            //move to the next node 
            temp = temp->next;
        }
    pop(&temp);
    //is the stack empty    
    if(temp == NULL){
        printf("Stack is Empty\n");
    }
        //done with temp 
        free(temp);
    }

    
}
//creates a stack and returns top pointer
struct numNode *createStack(int actions[], int numActions)
{
    //create a top pointer
    struct numNode *top = malloc(sizeof(struct numNode));
    //check for valid number of actions
    if(numActions < 1){
        top = NULL;
        return &top;
    }
    int action = 0;
    int actions_taken = 0;
    int i = 0;
    //check that you have taken all the appropriate actions
    while(actions_taken != numActions){
        if(i %2 == 0){
        action = actions[i];

           if(action ==2){
            push(&top, actions[i+1]);
            
            }
            else if (action == 1){
                pop(&top);
               
            }
            else if (action == 3){
                emptyStack(&top);
                
            } 
            else{
                return NULL;
            }
        }
        //increment
        actions_taken++;
        i+=2;
    }    
     return top;
}


