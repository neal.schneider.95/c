#include <stdio.h>
#include <stdlib.h>

struct node {
   int data;
   struct node *next;
};

struct node *head = NULL;
struct node *current = NULL;

//display the list
void printList() {

   struct node *ptr = head;

   printf("\n[head] =>");
   //start from the beginning
   while(ptr != NULL) {        
      printf(" %d =>",ptr->data);
      ptr = ptr->next;
   }

   printf(" [null]\n");
}

//insert link at the first location
void insert(int data) {
   //create a link
   struct node *link = (struct node*) malloc(sizeof(struct node));

   //link->key = key;
   link->data = data;

   //point it to old first node
   link->next = head;

   //point first to new first node
   head = link;
}

int search (int num){  //return position in list
   struct node *ptr = head;
   int count = 0;
   while (ptr->next != NULL && ptr->data != num ){
      count++;
      ptr = ptr->next;
   }
   if (ptr->next == NULL && ptr->data != num) {
      return -1;
   }
   else {
      return count;
   }
}

int removeNode (int num) {
   struct node *ptr = head;
   struct node *minNode = head;
   int count = 0;
   if (head == NULL) {
      return -1;
   }
   while (ptr->next != NULL) {
      if (ptr->data != num) {
         minNode= ptr;
         ptr = ptr->next;
         count ++;
      }
      else {
            minNode->next = ptr->next;
            free (ptr);
            return count;    
      }
   }
   return -1;
}

// insertion sort implementation

int sort(struct node **head) {
   struct node *newhead = head;
   struct node *unsorted = head; 
   struct node *probe;
   struct node *minNode = NULL;     // previous node as looking for next min, previous node to min until next one is found
   int min;

   if (newhead == NULL || head == NULL) return -1;
   while (unsorted) {
      //find min of remaining
      min = unsorted->data;
      probe = unsorted->next;
      minNode = unsorted;
      int sorted = 0;
      while (probe) {
         if (min > probe->data) {
            min   = probe->data;
         } else {
            minNode = probe;
         }
         probe = probe->next;
      } 
      if (minNode == unsorted){
         
      }
      probe
      *head = newhead;
   }
}

int main() {
   int tmp;
   printList();
   insert(10);
   insert(20);
   insert(30);
   insert(1);
   insert(40);
   insert(56); 

   printList();

   printf ("Position of 56: %d \n", search (56));
   printf ("Position of 10: %d \n", search (10));
   printf ("Position of 3: %d \n", search (3));

   tmp = removeNode (1);
   printf ("removed 1 at %d", tmp);
   printList();

   tmp = removeNode ( 1001);
   printf ("removed 1001 at %d", tmp);
   
   return 0;
}