// testing code snippets from https://docs.google.com/forms/d/e/1FAIpQLScrETz520CTJA4GuCo7S3JlRxopU8T2804oS8APaoE0B0LEAQ/viewform

#include <stdio.h>
#define foo(x, y) x / y + x
int main()
{
    int i = -6, j = 3;
    printf("%d\n", foo(i + j, 3));
    return 0;

    // int i = -5;
    // i = i / 3;
    // printf("%d\n", i);
    // return 0;

    // int d, a = 1, b = 2;
    // d = a++ + ++b;
    // printf ("%d %d %d", d, a, b);

    // void foo2();
    // printf("1 ");
    // foo2();    

}
// void foo2() { printf("2 "); }