//////////////ASSERT EXAMPLE 1///////////////////

#include <assert.h>     //defines assert()

#include <stdlib.h>     //defines calloc()
#include <stdio.h>      //defines string-related functions

int main(void)
{
    //string pointer
    char * dynamicString = NULL;
    //dynamic string
    dynamicString = calloc(20, sizeof(char));

    //assert that dynamicString is not NULL
    assert(dynamicString);
    
    //read a string
    fgets(dynamicString, 20, stdin);

   //assert that the string is nul terminated
   assert(dynamicString[19] == '\0');

   //print the string
   puts(dynamicString);
   return 0; 
}
