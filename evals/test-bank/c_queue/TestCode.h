#pragma once

#include <stdio.h>

struct numNode
{
    int num;
    struct numNode *next;
};


#ifdef __cplusplus
extern "C" {
#endif
    int enqueue(struct numNode **end, int data);
    int dequeue(struct numNode **front);
    void emptyQueue(struct numNode **front);
    struct numNode *makeQueue(int [], int);
#ifdef __cplusplus
}
#endif