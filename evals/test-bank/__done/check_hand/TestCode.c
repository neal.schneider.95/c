// #define RUNLOCAL 1
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

// Refer to README.md for the problem instructions

const char *hands[] = {"straight", "four", "three", "nothing", "invalid"};

const char *checkHand(int hand[]) 
{
    if (!hand) return hands[4];
    int card_count[14] = {0};
    int high_count = 0, i, j, hand_index;
    // printf("Checking [");
    for (i = 0; i<5; i++)
        if (hand[i] <1 || hand[i] > 13) {
            // printf("%d]<-invalid card\n", hand[i]);
            return hands[4];
        }
        else {
            card_count[hand[i]] +=1;
            // printf("%d, ", hand[i]);
        }
    for (i = 1; i<14; i++)
        if (card_count[i] > high_count)
            high_count = card_count[i];
    switch (high_count) {
        case 4:                         // Four of a kind
            hand_index = 1;
            break;
        case 3:                         // Three of a kind
            hand_index = 2;
            break;
        case 2:                         // A pair... who cares?
            hand_index = 3;
            break;
        case 1:                         // No matches... could it be a straight?
            hand_index = 0;
            i = 1;
            while (!card_count[i++]);   // find the first 1
            for (j = 0; j < 4; j++) {   // The next 4 have to be 1 to be a straight
                if (!card_count[i+j])   
                    return hands[3];    // found a zero before the loop ends, so its nothing
            }
            break; 
    }   // end case
    // printf ("] = %s\n", hands[hand_index]);
    return hands[hand_index];
}

#ifdef RUNLOCAL
void main () {
    int h[12][5] =  {
        {3,5,6,7,8 },
        {2,3,4,5,6 },
        {7,7,7,7,1 },
        {1,7,7,7,7 },
        {10,10,10,2,2 },
        {10,10,5,10,8 },
        {2,10,2,10,2 },
        {13,9,11,12,10},
        {1,1,10,2,2 },
        {10,10,10,2,10 },
        { 0,10,10,2,2 },
        { 10,10,10,2,20 }};
    for (int i = 0; i<12; i++){
        printf("%d: ",i);
        printf("%s \n", checkHand(h[i]));
    }
}
#endif