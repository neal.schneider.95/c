// #define RUNLOCAL 1  // Uncomment to include printArray and main local test
#include <stdio.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions


int BSearch(int arr[], int left, int right, int target)
{
    if (left > right) {
        return -1;
    }

    int mid = (right + left) / 2;
    if (target < arr[mid] )
        return BSearch(arr, left, mid-1, target);
    else if (target > arr[mid])
        return BSearch(arr, mid+1, right, target);
    else if (arr[mid] == target)
        return mid;
}


#ifdef RUNLOCAL
int main() {
    int searchArray[] = { 2, 5, 8, 12, 16, 23, 38, 56, 72, 91 };

    printf("{ 2, 5, 8, 12, 16, 23, 38, 56, 72, 91 }\n");
    printf("%d -1\n", BSearch(searchArray, 0, 9, 100));
    printf("%d 0\n",  BSearch(searchArray, 0, 9, 2));
    printf("%d 4\n",  BSearch(searchArray, 0, 9, 16));
    printf("%d 9\n",  BSearch(searchArray, 0, 9, 91));
}
#endif