// #define RUNLOCAL 1  // Uncomment to include printArray and main local test
#include <stdio.h>
#include <stdlib.h>
#include "TestCode.h"
#include <math.h>

// Refer to README.md for the problem instructions

int arrayManipulate(int *arr, unsigned int size)
{
    int temp;                                   // temp var for sorting
    if (!arr || size < 2) return -1;            // Check error conditions

    for (int i = 0; i < size; i++) {            // loop to apply rules
        if (arr[i] % 2 == 0 && arr[i] > 6) {    // Rule 1
            arr[i] *= arr[i];
        } else {
            if (arr[i] % 2 == 1 || arr[i] > 2){ // Rule 2
                arr[i] *= 2;
            }
        }
    }   // end rule-applying for loop

    for(int i=0; i<size; i++){                 // sort array
        for(int j = 0; j < i; j++){
            if (arr[j] > arr[i]) {
                temp = arr[j];
                arr[j] = arr[i];
                arr[i] = temp;
            }   // endif
        }       // end for j
    }           // end for i

    temp = size/2;
    if (size % 2 == 0) 
        return arr[temp] + arr[temp-1];     // if even, 
    else
        return arr[temp];
}

#ifdef RUNLOCAL

void printArray(int* arr, int size){
    for (int i = 0; i<size; i++) 
        printf("%3d ", arr[i]);
    printf("\n");
}

int main(){
    
    int size = 10, t;
    int x [] = {2, 5, 6, 7, 8, 9, 12, 3, 1, 18};
    int y [] = {2, 2, 6, 10, 12, 14, 18, 64, 144, 324};
    printArray(x, size);
    t = arrayManipulate(x, size);
    printArray(x, size);
    printArray(y, size);
    printf("result: = %d\n\n", t);

    size = 9;
    int a [] = {22, 15, 6, 7, 8, 9, 12, 3, 18};
    int b [] = {6, 12, 14, 18, 30, 64, 144, 324, 484};
    printArray(a, size);
    t = arrayManipulate(a, size);
    printArray(a, size);
    printArray(b, size);
    printf("result: = %d\n\n", t);

    size = 2;
    int c [] = {22, 0};
    int d [] = {0,484};
    printArray(c, size);
    t = arrayManipulate(c, size);
    printArray(c, size);
    printArray(d, size);
    printf("result: = %d\n", t);

    }
#endif