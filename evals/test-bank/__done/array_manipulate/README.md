# C Programming: Array Manipulate

## KSAT List
This question is intended to evaluate the following topics:
- A0087: Create and implement a sort routine.
- A0018: Analyze a problem to formulate a software solution.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0099: Use pointer arithmetic to traverse an array.
- S0097: Create and use pointers.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.

## Tasks
Write the function `arrayManipulate` that updates an array based on the [update rules](#update-rules) below. The 
function must iterate over the array using an integer pointer and pointer arithmetic (required) to access each array 
element.

**PARAMETERS:**
1. `arr`: a pointer to an array of integers
2. `size`: an unsigned int indicating the size of the array

**RETURN:** the middle element's content if `size` is odd, the sum of the two middle elements if `size` is even, or `-1` 
if `arr` is NULL or `size` is less than 2

- Assume all values in the array are greater than or equal to zero.

### Update Rules
Iterate through each element in the array. Check if the element's value applies to rule 1. If rule 1 does not apply 
then check rule 2. If neither rule applies then do nothing. After each element has been updated in place, sort the 
array in ascending order using any sorting algorithm.

- Rule 1: If the value in the element is an even number and greater than six (6), then square the contents of the
  element and move on to the next element.
- Rule 2: If the value in the element is an odd number or it is greater than two (2), then double the contents in the
  element.

### Examples
- The element is 12. Rule 1 applies, and it will be updated to 144 and iteration will continue to the next element.
- The element is 5. Rule 2 applies, and it will be updated to 10 and iteration will continue on.
- The element is 4. Rule 2 applies, and it will be updated to 8 and iteration will continue on.

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
