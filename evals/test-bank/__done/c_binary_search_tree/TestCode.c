#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"
// #define RUN_LOCAL 1
// Refer to README.md for the problem instructions

Node_ptr new_node(int value)    // Create a new node
{
    Node_ptr tmp = (Node_ptr)calloc(1, sizeof(Node));
    tmp->val = value;
    tmp->left = tmp->right = NULL;
    return tmp;
}

Node_ptr insert_node(Node_ptr node, char value) // insert a node
{
    if (node == NULL) return new_node(value);   // If empty create a new node
    if (value < node->val) {                    // insert left if less
        node->left = insert_node(node->left, value);    
    }
    else if (value > node->val) {               // insert right if more
        node->right = insert_node(node->right, value);
    }
    return node;
}

Node *buildBST(int nums[], int size)
{
    if (!nums || size < 1)
        return NULL;
    Node_ptr root = new_node(nums[0]);
    for (int i = 1; i < size; i++)
    {
        insert_node(root, nums[i]);
    }
    return root;
}

int destroyBST(struct numNode *root)
{
    int count;
    if (root) {
        count = destroyBST(root->left) + 1; // Death to the left of me
        count += destroyBST(root->right);   // Death to the right of me
        // printf("d (%d)", root->val);
        free(root);                         // Death to me...
        return count;
    } else {
        return 0;
    }
}

#ifdef RUN_LOCAL
// Pretty-print Tree (-ish)
void printPadding(char c, int n)
{
    for (int i = 0; i < n; i++)
        putchar(c);
}

int ppTree(Node_ptr root, int level)
{
    int count;
    if (root)
    {
        ppTree(root->right, level + 1);
        printPadding('\t', level);
        printf("%d\n", root->val);
        count += ppTree(root->left, level + 1);
    }
    else
    {
        printPadding('\t', level);
        puts("*");
    }
}
void main () {
    int nums[] = {7, 3, 8, 9, 2, 6};
    int size = sizeof(nums) / sizeof(*nums);
    Node_ptr root = buildBST(nums, size);
    ppTree(root, 0);
    printf("\nDeleted %d Nodes.\n", destroyBST(root));
}
#endif