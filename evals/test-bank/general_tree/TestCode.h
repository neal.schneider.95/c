#pragma once
#define 
#include <stdio.h>

typedef struct TreeNode         // added typedef
{
    int parent_number;
    int number;
    struct TreeNode *firstChild ;
    struct TreeNode *nextSibling ;
} *NodePtr, Node;               // added TreePtr, Node

#ifdef __cplusplus
extern "C" {
#endif
    struct TreeNode *buildTree(int [], int);

#ifdef __cplusplus
}
#endif
