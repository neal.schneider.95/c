# C Programming: Check Syntax
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0034: Declare and implement appropriate data types for program requirements.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0036: Declare and implement a char * array (string).
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0097: Create and use pointers.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.

## Tasks
Implement a function called `validateSyntax` that creates a rudimentary stack to keep track of the opening/closing 
container characters in a segment of code and returns a status when finished.

**PARAMETERS:**
1. `code`: a character pointer to a string of code 
2. `stack`: an empty character array

**RETURN:** an integer with one of the status codes below indicating success or errors

- Correct syntax in computer programming requires all opening container characters such as parenthesis '(', braces '{', 
  and brackets '[' have a closing matching container character ')','}', and ']'.
- There must also be the same number of opening container character types as closing container character types.
- For valid syntax, when a closing character is encountered, it means a matching opening container character exists.
- There cannot be any other opening container characters between the closing and opening one.
- If the code string received is empty, the function should return `ERROR_INVALID_DATA`.
- If the code string received has valid syntax, the function should return `ERROR_SUCCESS`.
- If the code string received is invalid, the function should return `ERROR_INVALID_BLOCK`.

### Example
The following is valid syntax example:

```c
if (x == y) { scores[(x + 1)] = 50; }
```

The following is an invalid syntax example:

```c
if (x == y) { scores[(x + 1]) = 50; }  // for the ] there is a previous ( before the matching [
```

The following is an invalid syntax example:

```c
if (x == y) { scores[(x + 1)] = 50;   // there is no matching } for the opening {
```

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
