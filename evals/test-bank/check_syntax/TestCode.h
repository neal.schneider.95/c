#pragma once

#include <stdio.h>

#define ERROR_SUCCESS 0
#define ERROR_INVALID_DATA 13
#define ERROR_INVALID_BLOCK 9

#ifdef __cplusplus
extern "C" {
#endif
    // Task One
    int validateSyntax(const char *code, char stack[]);

#ifdef __cplusplus
}
#endif