#pragma once
#include "binary_tree.h"


#ifdef __cplusplus
extern "C" {
#endif

int calculate_tree_sum(p_tree tree_ptr);

#ifdef __cplusplus
}
#endif

#define TREE_ERROR -121312

#define STACK_IS_EMPTY(stack_ptr) ((stack_ptr)->size == 0)
