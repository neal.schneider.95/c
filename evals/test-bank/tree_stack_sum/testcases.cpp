#include "TestCode.h"
#include <gmock/gmock.h>
#include "binary_tree.h"
#include "stack.h"

class tree_sum_test : public ::testing::Test {
protected:
    p_tree test_tree_one_ = nullptr;
    p_tree test_tree_two_ = nullptr;
    p_tree test_tree_three_ = nullptr;
    p_tree test_tree_four_ = nullptr;

    int answer_one_ = 30;
    int answer_two_ = 22;
    int answer_three_ = 5;
    int answer_four_ = 400;

    void SetUp() override {
        if (CREATE_TREE_SUCCESS == create_tree(&test_tree_one_)) {
            tree_attach_root(test_tree_one_, create_node(23));
            tree_attach_left(test_tree_one_, test_tree_one_->root, create_node(4));
            tree_attach_right(test_tree_one_, test_tree_one_->root, create_node(2));
            tree_attach_left(test_tree_one_, test_tree_one_->root->right, create_node(1));
        }

        if (CREATE_TREE_SUCCESS == create_tree(&test_tree_two_)) {
            tree_attach_root(test_tree_two_, create_node(10));
            tree_attach_left(test_tree_two_, test_tree_two_->root, create_node(5));
            tree_attach_right(test_tree_two_, test_tree_two_->root, create_node(5));
            tree_attach_right(test_tree_two_, test_tree_two_->root->left, create_node(2));
            tree_attach_right(test_tree_two_, test_tree_two_->root->right, create_node(1));
            tree_attach_left(test_tree_two_, test_tree_two_->root->right->right, create_node(-1));
        }

        if (CREATE_TREE_SUCCESS == create_tree(&test_tree_three_)) {
            tree_attach_root(test_tree_three_, create_node(2));
            tree_attach_right(test_tree_three_, test_tree_three_->root, create_node(1));
            tree_attach_right(test_tree_three_, test_tree_three_->root->right, create_node(1));
            tree_attach_right(test_tree_three_, test_tree_three_->root->right->right, create_node(1));
        }

        if (CREATE_TREE_SUCCESS == create_tree(&test_tree_four_)) {
            tree_attach_root(test_tree_four_, create_node(2));
            tree_attach_left(test_tree_four_, test_tree_four_->root, create_node(10));
            tree_attach_right(test_tree_four_, test_tree_four_->root->left, create_node(300));
            tree_attach_right(test_tree_four_, test_tree_four_->root->left->right, create_node(8));
            tree_attach_right(test_tree_four_, test_tree_four_->root->left->right->right, create_node(40));
            tree_attach_right(test_tree_four_, test_tree_four_->root->left->right->right->right, create_node(30));
            tree_attach_right(test_tree_four_, test_tree_four_->root->left->right->right->right->right, create_node(10));
        }
    }

    void TearDown() override {
        if (test_tree_one_)
            destroy_tree(test_tree_one_);
        if (test_tree_two_)
            destroy_tree(test_tree_two_);
        if (test_tree_three_)
            destroy_tree(test_tree_three_);
        if (test_tree_four_)
            destroy_tree(test_tree_four_);
    }
};

TEST_F(tree_sum_test, SumsCorrectly) {
    ASSERT_EQ(answer_one_, calculate_tree_sum(test_tree_one_));
    ASSERT_EQ(answer_two_, calculate_tree_sum(test_tree_two_));
    ASSERT_EQ(answer_three_, calculate_tree_sum(test_tree_three_));
    ASSERT_EQ(answer_four_, calculate_tree_sum(test_tree_four_));
}

TEST_F(tree_sum_test, NullTreeReturnsCorrectly) {
    ASSERT_EQ(TREE_ERROR, calculate_tree_sum(NULL));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
