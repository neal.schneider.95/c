#include <gmock/gmock.h>
#include "TestCode.h"
#include <string.h>

static char removeStr[5][7] = {
    "this",
    "string",
    "not",
    "",
    "nope",
};

static char removed[4][66] = {
    " is a string",
    "this is also a , unlike the first string",
    "aher one, not a great one, and not really like the last two.",
    ""
};

static char stringlist[5][65] = {
    "this is a string",
    "this is also a string, unlike the first string",
    "another one, not a great one, and not really like the last two.",
    "",
    "a third string!"
};

TEST(DeleteWordTest, remove_a_word)
{
    char buf[MAX_PATH + 1] = { 0 };
    int i;
    for(i = 0; i < 3; i++){
        snprintf(buf, MAX_PATH, "%s", stringlist[i]);
        ASSERT_EQ(ERROR_SUCCESS, deleteWord(buf, removeStr[i]));
        ASSERT_EQ(ERROR_SUCCESS, strcmp(buf, removed[i]));
    }
}


TEST(DeleteWordTest, bad_input)
{
    ASSERT_EQ(ERROR_INVALID_PARAMETER, deleteWord(stringlist[0], NULL));
    ASSERT_EQ(ERROR_INVALID_PARAMETER, deleteWord(NULL, removeStr[0]));
    ASSERT_EQ(ERROR_INVALID_PARAMETER, deleteWord(NULL, NULL));
}

TEST(DeleteWordTest, empty_string)
{
    char buf[MAX_PATH + 1] = { 0 };
    snprintf(buf, MAX_PATH, "%s", stringlist[3]);
    ASSERT_EQ(ERROR_SUCCESS, deleteWord(buf,removeStr[3]));
}

TEST(DeleteWordTest, item_not_found)
{
    char buf[MAX_PATH + 1] = { 0 };
    snprintf(buf, MAX_PATH, "%s", stringlist[4]);
    ASSERT_EQ(ERROR_NOT_FOUND, deleteWord(buf, removeStr[4]));
}
