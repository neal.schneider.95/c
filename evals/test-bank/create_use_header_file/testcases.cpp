#include <gmock/gmock.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "TestCode.h"
#include "celsius.h" // try to include twice to see if guards in place


#ifdef __cplusplus
}
#endif

#define MAY 30
#define AUG 31

TEST(Header_File_Tests, header_file_created)
{
    getCelsius(99);  // verify function exists with correct signature in celsius.h
}

TEST(Header_File_Tests, header_file_proper)
{
    char str[120];

    FILE *fp = fopen("celsius.h" , "r");
    ASSERT_TRUE(fp != NULL);  //file doesn't exist
    int count = 0;
    while(fgets(str, sizeof(str), fp) != NULL) {
        if(strstr(str, "#ifndef") != NULL)
            count++;
        if(strstr(str, "#define") != NULL && count == 1)
            count++;
        if(strstr(str, "#endif") != NULL && count == 2)
            count++;       
    }
    ASSERT_EQ(count, 3); //need to find all guards
    fclose(fp);
}

TEST(Header_File_Tests, header_test_normalCases)
{
    int temps1[MAY][2] = { {63,97},
                         {61, 94},
                         {57,82},
                         {74,104},
                         {74,99},
                         {69,98},
                         {77, 101},
                         {54, 82},
                         {61, 94},
                         {57,82},
                         {74,104},
                         {74,99},
                         {69,98},
                         {77, 101},
                         {54, 82},                         
                         {61, 94},
                         {57,82},
                         {74,104},
                         {74,99},
                         {69,98},
                         {77, 101},
                         {54, 82},
                         {61, 94},
                         {57,82},
                         {74,104},
                         {74,99},
                         {69,98},
                         {77, 101},
                         {54, 82},
                         {56, 80}};

    int res = processTemps(temps1, MAY);
    
    int cTemps[MAY][2] =   {{17, 36},
                            {16, 34},
                            {13, 27},
                            {23, 40},
                            {23, 37},
                            {20, 36},
                            {25, 38},
                            {12, 27},
                            {16, 34},
                            {13, 27},
                            {23, 40},
                            {23, 37},
                            {20, 36},
                            {25, 38},
                            {12, 27},
                            {16, 34},
                            {13, 27},
                            {23, 40},
                            {23, 37},
                            {20, 36},
                            {25, 38},
                            {12, 27},
                            {16, 34},
                            {13, 27},
                            {23, 40},
                            {23, 37},
                            {20, 36},
                            {25, 38},
                            {12, 27},
                            {13, 26}};

    //checking array to make sure each value converted correctly                 
    for(int i = 0; i < MAY; i++)
    {
        ASSERT_EQ(temps1[i][0],cTemps[i][0]);
        ASSERT_EQ(temps1[i][1],cTemps[i][1]);
    }
    // diff between high and low should be 28
    ASSERT_EQ(28, res);
}

TEST(Header_File_Tests, header_test_normalCases2)
{
    int temps1[AUG][2] = { {73,107},
                         {71, 99},
                         {57,88},
                         {70,104},
                         {70,99},
                         {69,93},
                         {77, 106},
                         {73, 104},
                         {79, 105},
                         {81,109},
                         {78,104},
                         {79,99},
                         {79,98},
                         {72, 90},
                         {75, 92},                         
                         {73, 94},
                         {77,96},
                         {70,104},
                         {78,99},
                         {72,101},
                         {77, 104},
                         {81, 89},
                         {72, 88},
                         {81,97},
                         {74,95},
                         {74,99},
                         {83,103},
                         {77, 106},
                         {76, 93},
                         {76, 94},
                        {81, 99}};

    int res = processTemps(temps1, AUG);
    
    int cTemps[AUG][2] =   {{22,41},
                            {21,37},
                            {13,31},
                            {21,40},
                            {21,37},
                            {20,33},
                            {25,41},
                            {22,40},
                            {26,40},
                            {27,42},
                            {25,40},
                            {26,37},
                            {26,36},
                            {22,32},
                            {23,33},
                            {22,34},
                            {25,35},
                            {21,40},
                            {25,37},
                            {22,38},
                            {25,40},
                            {27,31},
                            {22,31},
                            {27,36},
                            {23,35},
                            {23,37},
                            {28,39},
                            {25,41},
                            {24,33},
                            {24,34},
                            {27,37} };

    //checking array to make sure each value converted correctly                
    for(int i = 0; i < AUG; i++)
    {
        ASSERT_EQ(temps1[i][0],cTemps[i][0]);
        ASSERT_EQ(temps1[i][1],cTemps[i][1]);
    }
    // diff between high and low should be 29
    ASSERT_EQ(29, res);
}


TEST(Header_File_Tests, header_test_edge_cases)
{
    int temps1[AUG][2] = { {73,107},
                         {71, 99},
                         {57,88},
                         {70,104},
                         {70,99},
                         {69,93},
                         {77, 106},
                         {73, 104},
                         {79, 105},
                         {81,109},
                         {78,104},
                         {79,99},
                         {99,98},
                         {72, 90},
                         {75, 92},                         
                         {73, 94},
                         {77,96},
                         {70,104},
                         {78,99},
                         {72,101},
                         {77, 104},
                         {81, 89},
                         {72, 88},
                         {81,97},
                         {74,95},
                         {74,99},
                         {83,103},
                         {77, 106},
                         {76, 93},
                         {76, 94},
                        {81, 99}};

    int res = processTemps(temps1, 27); 
                    
    ASSERT_EQ(-1, res);  //too few days

    res = processTemps(temps1, 35); 
                    
    ASSERT_EQ(-1, res);  // too many days

    res = processTemps(temps1, AUG); 
                    
    ASSERT_EQ(-1, res); // bad data in array
}

int main(int argc, char** argv, char** envp)
{
    ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
