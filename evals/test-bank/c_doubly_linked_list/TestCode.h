#pragma once

#include <stdio.h>

typedef struct nameNode
{
    const char *name;
    struct nameNode *next;
    struct nameNode *prev;
}nameNode;


#ifdef __cplusplus
extern "C" {
#endif
    nameNode *buildList(const char **, int);
    nameNode *removeNode(nameNode *, const char *);
    void freeMemory(nameNode *);
#ifdef __cplusplus
}
#endif