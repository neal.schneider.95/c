#pragma once

#include <stdio.h>

#define ERROR_INVALID_DATA 13

typedef enum {
    TypeBitmask,
    TypeFuncPtr,
} StructType;

typedef struct {
    unsigned long        id;
    StructType            type;
    union {
        unsigned long    bitmask;
        int(*func)(unsigned long param);
    } u;
} CONTAINER, *PCONTAINER;

#ifdef __cplusplus
extern "C" {
#endif
    // Task One
    int computeScore(int final, int midTerm, int project[], int quiz[]);

#ifdef __cplusplus
}
#endif