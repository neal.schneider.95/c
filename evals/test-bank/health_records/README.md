# C Programming: Health Records
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034: Declare and implement appropriate data types for program requirements.
- S0033: Utilize assignment operators to update a variable.
- S0042: Open and close an existing file.
- S0043: Read, parse, write (append, insert, modify) file data.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0090: Allocate memory on the heap (malloc).
- S0097: Create and use pointers.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0156: Utilize a struct composite data type.
- S0160: Utilize the standard library.

## Tasks
Create two structs that manage patient health information and functions that calculate statistics for patients using 
those structs.

### Task 1
Create a struct `BirthDate` that will contain a date of birth.

**ATTRIBUTES:**

1. `month`: An integer containing the month
2. `day`: An integer containing the day of the month
3. `year`: An integer containing the year

- This struct should be defined in the header file.

### Task 2
Create a struct `HealthProfile` that will contain the health statistics of a patient.

**ATTRIBUTES:**

1. `firstName`: A 14 element character array containing the patient's first name
2. `lastName`: A 14 element character array containing the patient's last name
3. `gender`: A character for the person's gender
4. `dateOfBirth`: A BirthDate structure for the person's date of birth
5. `age`: An int containing the current age in years
6. `height`: an int containing the height in inches
7. `weight`: an int containing the weight in pounds
8. `maxHeartRate`: An int with the calculated maximum heart rate
9. `maxTargetRate`: A double representing the person's max target heart rate
10. `minTargetRate`: A double representing the person's min target heart rate
11. `BMI`: A double containing the person's Body Mass Index

- This struct should be defined in the header file.
- The `HealthProfile` needs to be defined before your program will compile and the unit tests need to use your structs 
  without modification to the unit tests. 
- Include a definition for `NAME_LENGTH`, the size of the name arrays (each name should be 14 characters long). 

### Task 3
Implement a function `readData` that reads a file containing health information and fills a database.

**PARAMETERS:**
1. `healthDatabase`: A pointer to an array of `healthProfile`

**RETURN:** void

- Assume `healthDatabase` will never be null.
- Read the data (firstName, lastName, gender, dateOfBirth, height, weight) of 5 persons from the file `PersonsData.txt`
  and use it to set the attributes of the elements in the `HealthProfile` array.

### Task 4
Implement a function `calculateMaxHeartRate` that will calculate and return a maximum heart rate.

**PARAMETERS:**
1. `age`: An integer containing the age in years

**RETURN:** An integer containing the maximum heart rate according to the calculation below

- The formula for calculating your maximum heart rate in beats per minute is:
  - maxHeartRate = 220 - your age in years

### Task 5
Implement a function `calculateMaxTargetHeartRate` that will calculate and return a maximum target heart rate.

**PARAMETERS:**
1. `maxHR`: An integer containing the maximum heart rate

**RETURN:** An integer containing the maximum target heart rate according to the calculation below

- Your target heart rate is a range that’s 50–85% of your maximum heart rate:
  - maxTargetRate = 0.85 * maxHeartRate
- round the return value to 2 decimal numbers

### Task 6
Implement a function `calculateMinTargetHeartRate` that will calculate and return a minimum target heart rate.

**PARAMETERS:**
1. `maxHR`: A double containing the maximum heart rate

**RETURN:** An integer containing the minimum target heart rate according to the calculation below

- Your target heart rate is a range that’s 50–85% of your maximum heart rate:
  - minTargetRate = 0.50 * maxHeartRate
- Round the return value to 2 decimal numbers

### Task 7
Implement a function `calculateBMI` that will calculate and return a body mass index.

**PARAMETERS:**
1. `hp`: A pointer to a `healthProfile` struct

**RETURN:** A double containing the calculated BMI, or `0` to handle division by 0 or if `hp` is null

- The formulas for calculating BMI is:
  - BMI = (weightInPounds × 703) / (heightInInches × heightInInches)
- Round BMI to 1 decimal number

### Task 8
Implement a function: `processHealthProfiles` that reads a file containing health information and fills a database with 
the previous tasks' calculations.

**PARAMETERS:**
- No input parameters

**RETURN:** a pointer to the array of health profiles created in the function, or `NULL` if memory cannot be allocated

- Use the functions created in the previous tasks to solve this task.
- This function should use `readData` to ingest data from the `PersonsData.txt` file.
- The function will return the health profiles of all persons after the calculations of age, maximum heart rate, 
  maxTargetRate, minTargetRate, and BMI. 
- Note that the ages of the persons will depend on the current date.
- For this problem to work, the current directory should remain the folder with the text files (i.e. the root of the 
  question's directory).


## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
