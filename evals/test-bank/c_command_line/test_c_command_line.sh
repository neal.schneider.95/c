#!/bin/bash

set -e  #Halt on any error

cmake -DCMAKE_BUILD_TYPE=Release -B $PWD/build/release -S $PWD
make -C $PWD/build/release/
./build/release/TestCode
