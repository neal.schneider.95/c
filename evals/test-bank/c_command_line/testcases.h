#pragma once

#ifdef __cplusplus
extern "C" {
#endif

    int doTest(const char *binPath);

#ifdef __cplusplus
}
#endif