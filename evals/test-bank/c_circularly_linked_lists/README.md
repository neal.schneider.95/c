# C Programming: C Circularly Linked List
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0034: Declare and implement appropriate data types for program requirements.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0069: Find an item in a Circularly Linked List.
- S0070: Add and remove nodes from a Circularly Linked List.
- S0052: Implement a function that returns a single value.
- S0057: Create and destroy a Circularly Linked List.
- S0053: Implement a function that returns a memory reference.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0090: Allocate memory on the heap (malloc).
- S0097: Create and use pointers.
- S0091: Unallocating memory from the heap (free).
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0156: Utilize a struct composite data type.
- S0160: Utilize the standard library.

## Tasks
Implement functions that build and destroy a circularly linked list where the last item in the list points to the first 
item in the list.

### Task 1
Implement the function `buildCList` that creates a circularly linked list from an array of numbers and returns the head 
of the list.

**Parameters:**
1. `nums`: An array of integers to place in the linked list
2. `size`: An int representing the size of the `nums` array

**Return:** struct `numNode` pointer to the head of the circularly-linked list, or `null` if `size` is 0 or `nums` is null

- The last node must point back to the first.
- Iterate over the `nums` array and set the number of a new node to be inserted
- Each new node will be inserted at the head of the circularly linked list.
- If a number in `nums` is already in the circularly linked list, do not insert it into the circularly linked list and 
  continue iterating over the `nums` list.

### Task 2
Implement the function `emptyList` which removes each node, frees all the memory, and returns the number of nodes freed.

**Parameters:**
1. `head`: The head of a circularly linked list that is to be deleted

**Return:** int that contains the number of nodes removed

- If `head` is NULL, the function should return zero (0).

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
