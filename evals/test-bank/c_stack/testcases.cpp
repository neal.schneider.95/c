#include <gmock/gmock.h>
#include "TestCode.h"

TEST(Push_Tests, zeroCase)
{
    int data = 0;
    struct numNode *top = NULL;
    ASSERT_EQ(0, push(&top, data));
    ASSERT_FALSE(NULL == top);
    ASSERT_EQ(data, top->num);
    ASSERT_TRUE(NULL == top->next);
    free(top);
    top = NULL;
}

TEST(Push_Tests, belowZeroCase)
{
    int data = -1;
    struct numNode *top = NULL;
    ASSERT_EQ(0, push(&top, data));
    ASSERT_FALSE(NULL == top);
    ASSERT_EQ(data, top->num);
    ASSERT_TRUE(NULL == top->next);
    free(top);
    top = NULL;
}

TEST(Push_Tests, aboveZeroCase)
{
    int data = 1;
    struct numNode *top = NULL;
    ASSERT_EQ(0, push(&top, data));
    ASSERT_FALSE(NULL == top);
    ASSERT_EQ(data, top->num);
    ASSERT_TRUE(NULL == top->next);
    free(top);
    top = NULL;
}

TEST(Push_Tests, nullCase)
{
    int data = 7734;
    ASSERT_EQ(1, push(NULL, data));
}

TEST(Pop_Tests, zeroCase)
{
    int data = 0;
    struct numNode *top = NULL;
    ASSERT_EQ(0, push(&top, data));
    ASSERT_FALSE(NULL == top);
    ASSERT_EQ(data, pop(&top));
    ASSERT_TRUE(NULL == top);
}

TEST(Pop_Tests, belowZeroCase)
{
    int data = -1;
    struct numNode *top = NULL;
    ASSERT_EQ(0, push(&top, data));
    ASSERT_FALSE(NULL == top);
    ASSERT_EQ(data, pop(&top));
    ASSERT_TRUE(NULL == top);
}

TEST(Pop_Tests, aboveZeroCase)
{
    int data = 1;
    struct numNode *top = NULL;
    ASSERT_EQ(0, push(&top, data));
    ASSERT_FALSE(NULL == top);
    ASSERT_EQ(data, pop(&top));
    ASSERT_TRUE(NULL == top);
}

TEST(Pop_Tests, nullCase1)
{
    ASSERT_EQ(0, pop(NULL));
}

TEST(Pop_Tests, nullCase2)
{
    struct numNode *top = NULL;
    ASSERT_TRUE(NULL == top);
    ASSERT_EQ(0, pop(&top));
}

TEST(EmptyStack_Tests, oneNodeCase)
{
    int data = 7;
    struct numNode *top = NULL;
    ASSERT_EQ(0, push(&top, data));
    ASSERT_FALSE(NULL == top);
    ASSERT_EQ(data, top->num);
    ASSERT_TRUE(NULL == top->next);

    emptyStack(&top);
    ASSERT_TRUE(NULL == top);
}

TEST(EmptyStack_Tests, twoNodesCase)
{
    int data[] = {1, 3};
    int sz_data = 2;
    struct numNode *top = NULL;
    struct numNode *bottom = NULL;
    for (int i = 0; i < sz_data; i++)
    {
        ASSERT_EQ(0, push(&top, data[i]));
        ASSERT_FALSE(NULL == top);
        ASSERT_EQ(data[i], top->num);
        if (i == 0)
        {
            ASSERT_TRUE(NULL == top->next);
            bottom = top;
        }
        else
        {
            ASSERT_TRUE(bottom == top->next);
        }
    }
    emptyStack(&top);
    ASSERT_TRUE(NULL == top);
    bottom = NULL;
}

TEST(EmptyStack_Tests, threeNodesCase)
{
    int data[] = {1, 3, 6};
    int sz_data = 3;
    struct numNode *top = NULL;
    struct numNode *bottom = NULL;
    for (int i = 0; i < sz_data; i++)
    {
        ASSERT_EQ(0, push(&top, data[i]));
        ASSERT_FALSE(NULL == top);
        ASSERT_EQ(data[i], top->num);
        if (i == 0)
        {
            ASSERT_TRUE(NULL == top->next);
            bottom = top;
        }
        else if (i == 1)
        {
            ASSERT_TRUE(bottom == top->next);
        }
        else
        {
            ASSERT_FALSE(bottom == top->next);
        }
    }
    emptyStack(&top);
    ASSERT_TRUE(NULL == top);
    bottom = NULL;
}

TEST(CreateStack_Tests, pushCase1)
{
    int i;
    int data[] = { 4, 5, 10, -3, 7, 5, 2, 99, 8, 26, -11, 17 };
    struct numNode *res = createStack(data, 12);
    struct numNode *top = res;
    int test[] = { 17, -11, 26, 8, 99, 2, 5, 7, -3, 10, 5, 4 };
    ASSERT_FALSE(NULL == res); //res should not be NULL
    for (i = 0; res != NULL; i++, res = res->next)
    {
        ASSERT_EQ(test[i], res->num);
    }
    ASSERT_EQ(12, i); // Should have tested 12 items
    emptyStack(&top);
    ASSERT_TRUE(NULL == top);
}

TEST(CreateStack_Tests, pushEmptyStackCase1)
{
    int data[] = { 4, 5, 10 };
    struct numNode *res = createStack(data, 3);
    emptyStack(&res);
    ASSERT_TRUE(NULL == res);
}

TEST(CreateStack_Tests, pushPopCase1)
{
    int i;
    int data[] = { 7, 4, 5 };
    struct numNode *top = createStack(data, 3);
    ASSERT_EQ(5, top->num);
    ASSERT_EQ(5, pop(&top));
    ASSERT_EQ(4, top->num);
    push(&top, 10);
    ASSERT_EQ(10, top->num);
    ASSERT_EQ(10, pop(&top));
    struct numNode *res = top;
    int test2[] = { 4, 7 };
    for (i = 0; res != NULL; i++, res = res->next)
    {
        ASSERT_EQ(test2[i], res->num);
    }
    ASSERT_EQ(2, i); // Should have tested 2 items
    emptyStack(&top);
    ASSERT_TRUE(NULL == top);
}

TEST(CreateStack_Tests, pushPopCase2)
{
    int data[] = { 4, 5, 10 };
    struct numNode *res = createStack(data, 3);
    ASSERT_EQ(10, pop(&res));
    ASSERT_EQ(5, pop(&res));
    ASSERT_EQ(4, pop(&res));
    ASSERT_TRUE(NULL == res);
}
