#include <gmock/gmock.h>
#include <sys/types.h>  //waitpid
#include <sys/wait.h>   //waitpid
#include "TestCode.h"


// CProgrammingTests.cpp : Defines the entry point for the console application.
#define _CRT_SECURE_NO_WARNINGS 1
#define _SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING 1

int run_grep(const char* regexp);  //Helper function for header file tests

TEST(Sort_Tests, ascendingCases)
{
    int refSet[] = {1,2,4,4,5,8};
    int dataSet[] = {5,4,8,4,1,2};
    sort(dataSet,6, ascending);
    for(int i = 0; i < 6; i++)
    {
        ASSERT_EQ(dataSet[i], refSet[i]);
    }
}

TEST(Sort_Tests, descendingCases)
{
    int refSet[] = {8,5,4,4,2,1};
    int dataSet[] = {5,4,8,4,1,2};
    sort(dataSet, 6, descending);
    for(int i = 0; i < 6; i++)
    {
        ASSERT_EQ(dataSet[i], refSet[i]);
    }
}

TEST(Sort_Tests, checkAscendingFunctionPointerDeclared)
{
    //The complexity of the expression is because of the multiple levels of interpretation.  By using
    //exec() instead of system() we avoid bash interpolation, and by keeping it out of the macro we
    //avoid macro expansion problems. 
    int result=run_grep("[(]\\s*\\*\\s*ascending\\s*[)]"); //grep returns 0 if match found
    ASSERT_EQ(0, result) <<"ERROR: function pointer syntax not found (a specific syntax is required)."; //Embedding the system call directly in the macro results in odd quoting errors
}

TEST(Sort_Tests, checkDescendingFunctionPointerDeclared)
{
    int result=run_grep("[(]\\s*\\*\\s*descending\\s*[)]"); //grep returns 0 if match found
    ASSERT_EQ(0, result) <<"ERROR: function pointer syntax not found (a specific syntax is required)."; //Embedding the system call directly in the macro results in odd quoting errors
}

//Helper function that runs grep on header file using fork and exec
//This is to avoid the extra quoting system() would perform by invoking bash
int run_grep(const char* regexp) {
    int status;

    //Fork so we can exec
    pid_t pid = fork();

    //exec or wait, depending on who we are
    if (pid == -1) {
        perror("Fork failed\n");
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
    } else { // we are the child, so let's exec()
        execlp("grep","grep","-q",regexp,"TestCode.h",(char*) NULL);  //Simpler to use execl than system to avoid shell interpolation
        perror("Exec failed.");
    }
    return status;  //We can't just put this in case 2 because the compiler doesn't realize cases 1 and 3 abort this function
}
