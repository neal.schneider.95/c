# C Programming: Function Pointers
## KSAT List
This question is intended to evaluate the following topics:
- A0087: Create and implement a sort routine.
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034: Declare and implement appropriate data types for program requirements.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0097: Create and use pointers.
- S0108: Utilize post and pre increment/decrement operators.
- S0098: Implement a function pointer to call another function.
- S0391: Create and utilize a function prototype*/

## Tasks
Create two comparison functions, `ascending` and `descending` and a use them as arguments in a `sort` function.

### Task 1
Create a function that compares two items to see which is larger and returns whether they are in ascending order. Then 
create a function pointer `ascending` that points to this function.

**PARAMETERS:**
1. an integer that contains the first item to compare
2. an integer that contains the second item to compare

**RETURN:** an int indicating if an ascending sort is needed

- You must declare your function pointer in the header file.

### Task 2
Create a function that compares two items to see which is larger and returns whether they are in descending order. Then 
create a function pointer `descending` that points to this function.

**PARAMETERS:**
1. an integer that contains the first item to compare
2. an integer that contains the second item to compare

**RETURN:** an int indicating if a descending sort is needed

- This function should end up returning the opposite result of the equivalent `ascending` function call.
- You must declare your function pointer in the header file.

### Task 3
Create a function, called `sort()`, that will sort an array of integers according to a function pointer that is passed 
into it as an input parameter.

**PARAMETERS:**
1. an array of int
2. an int specifying the length of the array
3. a function pointer to a comparison function

**RETURN:** void

- The `sort` function should not have any return values and must modify the input array. 
- The `sort` function will run the comparison function to determine the sorting order when comparing during the sort. 
- You may use any sorting algorithm to sort the array, so long as it uses the aforementioned function pointers when 
  comparing elements.
- The function needs to be compatible with the passed in `ascending` and `descending` function pointers.
- These function pointers must each point to their corresponding comparison function that sort can use.
- When sort is passed ascending, the function will modify the data set to be in ascending order e.g. 1, 2, 3, 4, 5. 
- When passed descending, the data set will be modified in descending order e.g. 5, 4, 3, 2, 1.

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
