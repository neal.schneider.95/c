#pragma once
#include <stdio.h>

#define ERROR_INVALID_PARAMETER 87
#ifdef __cplusplus
extern "C" {
#endif
    double stringToDouble(const char *str);
#ifdef __cplusplus
}
#endif