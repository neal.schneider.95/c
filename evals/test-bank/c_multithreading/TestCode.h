#pragma once

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif
	// Task One
	int FindPrimes(const char *, int *, int);
	
#ifdef __cplusplus
}
#endif