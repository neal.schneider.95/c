#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

struct nameNode *processNames(const char **names)
{
    //names cannot be null
    if(!names){
        return NULL;
    }
    //checks for null values in the names array
    for (int i = 0; i < 10; i++){
        if(names[i] == NULL){
            return NULL;
        }
    }
    
    struct nameNode *head = NULL;

    //creating a new multi dimensional array to store the names to allow for editing
    char new_names[100][100] = {0};
    //used for name swapping 
    char name[100];

    //counter for how many names are in our new names array 
    int x = 0;

    for(int i =0; i<10; i++){
        //don't copy if names[i] is empty string
        if(strlen(names[i]) == 0){
            continue;
        }
        else{

           
            strcpy(new_names[x], names[i]);
            //increment j only if we put something in it
            x++;
           
            }

    }

    // sort new names array into alphabetical order checks every name
    for(int i=0;i<x;i++){
        for(int j=i+1;j<x;j++){
            //compares name at current element against the name in the next element swaps if they are out of order
            if(strcmp(new_names[i], new_names[j])>0){
                strcpy(name,new_names[i]);
                strcpy(new_names[i],new_names[j]);
                strcpy(new_names[j],name);
            }
        }
    }

    //used for below while loop to create the linked list
    int i = 0;
    while(i < x){

        //allocating mem to new node and giving value to new node 
        struct nameNode *newNode = malloc(sizeof(struct nameNode));

        newNode->name = new_names[i];
        newNode->next = NULL;

       
        //checking if head already exist / if not create it 
        if(!head){
            head = newNode;
        }
        //if head does exist traverse list using node curr till you find the end of list 
        else{
            struct nameNode *curr = malloc(sizeof(struct nameNode));
            curr = head;
            while(curr->next != NULL){
                curr = curr->next;
            }
            //add the new node to the end of the list
            curr->next = newNode;

        }
        i++;

    }



    return head;
}
//free all the nodes
void freeMemory(struct nameNode *head)
{
    return;
    struct nameNode *temp = NULL;
    //walk through list free the nodes as we go
    while(head){
        temp = head;
        head = head->next;
        free(temp);
    }
}


// void main () {
//     const char *names[] = { "Joe", "Ace", "Gene", "Paul", "Peter", "Hank", "Timmy", "Sarah", "Alice", "Carol" };
//     const char *names2[] = { "Ace", "Alice", "Carol", "Gene", "Hank", "Joe", "Paul", "Peter", "Sarah", "Timmy" };
//     // struct nameNode *res = processNames(names);
//     // struct nameNode *probe = res;
//     // while (probe->next) {
//     //     printf("%s->", probe->name);
//     //     probe = probe->next;
//     // }
//     printf("[done]\n");
// }

