# KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0066: Add and remove nodes from a Linked List.
- S0065: Find an item in a Linked List.
- S0055: Create and destroy a Linked List.
- S0053: Implement a function that returns a memory reference.
- S0048: Implement a function that receives input parameters.
- S0090: Allocate memory on the heap (malloc).
- S0097: Create and use pointers.
- S0091: Unallocating memory from the heap (free).
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0156: Utilize a struct composite data type.
- S0160: Utilize the standard library.

# Tasks
## Task 1

Write the function `processNames` that receives a 10-element array of char *. The array contains a list of peoples' 
first names. The `processNames` function should create a linked list using the `nameNode` struct defined in 
`TestCode.h`. The `processNames` function should iterate the array and insert the names into nodes so they are in 
ascending (alphabetical) order in the linked list. If the array is NULL or contains NULL elements, the function should 
return NULL; empty strings should be ignored. The `processNames` function should return a pointer to the head node of 
the link list once processed.

## Task 2

Write the function `freeMemory` that receives a pointer to your linked-list's head node so you can iterate over the 
link list and free all allocated memory.
