#include <stdio.h>
#include <stdlib.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

int enqueue(struct numNode **end, int data)
{
    if (end == NULL) return 1;              //check for empty queue
    struct numNode *newNode = (struct numNode*) malloc(sizeof(struct numNode));
    if (newNode == NULL) return 1;          // Failed mem allocation, ret err
    newNode->next = NULL;                   // Clear next link in new node
    newNode->num = data;
    (*end)->next = newNode;                 // Add new node at the rear of the queue
    *end = newNode;                         // Update end pointer to end of queue
    return 0;
}
int dequeue(struct numNode **front)
{   
    struct numNode *tempNode;
    int frontVal;
    if (front == NULL || *front == NULL)            // check for empty queue
        return 1;  
    frontVal = (*front)->num;                       // get value from queue
    tempNode = *front;                              // keep front node for later freeing
    if ((*front)->next) {                           // if not at last, go to next
        *front = (*front)->next;
    }
    free (tempNode);
    return frontVal;
}

void emptyQueue(struct numNode **front)
{
    struct numNode *tempNode = NULL, *curr = *front;
    if (front == NULL || *front == NULL) return ;  // check for empty queue
    do {
        tempNode = curr;
        curr = curr->next;
        free(tempNode);
    } while (curr);
    *front = NULL;
    return;
}

struct numNode *makeQueue(int actions[], int numActions)
{
    struct numNode *front = NULL, *end = NULL;
    for (int i = 0; i < numActions * 2; i += 2){
        switch (actions[i]) {
            case 1: 
                dequeue (&front);
                break;
            case 2:
                if (front==NULL) {
                    end = front = (struct numNode*) malloc(sizeof(struct numNode));
                    front->next = NULL;
                    front->num = actions[i+1];
                } else {
                    enqueue (&end, actions[i+1]);
                }
                break;
            case 3: 
                emptyQueue(&front);
                break;
            default:
                emptyQueue(&front);
                return NULL;
        } 
    }
    return front;
}

void printQueue(struct numNode **front){
    if (front == NULL || (*front) == NULL) {
        printf("Empty Queue\n");
        return;
    }
    printf("Front -> ");
    struct numNode *curr = *front;
    while (curr) {
        printf ("%d -> ", curr->num);
        curr = curr->next;
    } 
    printf ("End\n");
}

// #define LOCAL
#ifdef LOCAL
void main () {
    struct numNode *front = NULL, *end = NULL;
    int actions[] = { 2, 4, 2, 5, 2, 10, 2, 99 };
    
    printQueue(&front);
    front = makeQueue (actions, 4);
    printQueue(&front);
    emptyQueue(&front);
    printQueue(&front);
    printf("Done.\n");
}
#endif