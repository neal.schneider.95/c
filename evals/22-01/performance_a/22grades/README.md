# KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0160: Utilize the standard library.

# Tasks
In a computer programming course, a student’s overall score is computed as follows. The student takes four (4) 
quizzes, and six (6) projects, a Midterm exam, and a Final Exam. The student’s lowest project score is discarded 
therefore only including five (5) projects in the scoring. Each score is in a range from 0 to 100. Essentially, the 
overall score is computed using the values below as such:

- Final – 30%
- Midterm – 25%
- quizzes – 5% each
- Projects – 5% each

overallScore = final * .30 + midterm * .25 + quiz1 * .05 + quiz2 * 05 + ....etc.

Write a function `computeScore` that accepts the final exam score as an int, midterm exam score as an int, an array of 
six project scores as ints, and an array of four quiz scores as ints. The function should compute the floating point 
average of the student’s score, then round the score to the nearest integer and return it.

- If any score is < 0 or > 100, return `-1`.
