#include <stdio.h>
#include "TestCode.h"

int main()
{   
    // INVALID TEST CASES 
    int final[] = { 105, 89, 89 };
    int mid[] = { 89, -5, 90 };

    int proj[][6] = { { 88, 78, 98, 92, 70, 92 },
                     { 89, 89, 89, 89, 89, 89 },
                     { 89, 89, 89, 89, 101, 89 } };

    int quiz[][4] = { {80, 85, 95, -1 },
                    { 89, 89, 89, 89 },
                    { 89, 89, 89, 89 } };


    if(computeScore(final[2], mid[2], proj[2], quiz[2]) == -1){printf("Test 1 :: PASSED\n");}
    else{printf("TEST 1 :: FAILED\n");}
    if(computeScore(final[2], mid[2], proj[1], quiz[0]) == -1){printf("Test 2 :: PASSED\n");}
    else{printf("TEST 2 :: FAILED\n");}
    if(computeScore(final[0], mid[2], proj[0], quiz[2]) == -1){printf("Test 3 :: PASSED\n");}
    else{printf("TEST 3 :: FAILED\n");}
    if(computeScore(final[2], mid[1], proj[0], quiz[2]) == -1){printf("Test 4 :: PASSED\n");}
    else{printf("TEST 4 :: FAILED\n");}

    // VALID TEST CASES
    int final2[] = { 95, 89, 57 };
    int mid2[] = {89, 89, 82};

    int proj2[][6] = { { 88, 78, 98, 92, 70, 92 },
                     { 89, 89, 89, 89, 0, 89 },
                     { 89, 89, 89, 89, 100, 89 } };

    int quiz2[][4] = { {80, 85, 95, 97 },
                    { 89, 89, 89, 89 },
                    { 74, 69, 80, 81 } };


    if(computeScore(final2[0], mid2[0], proj2[0], quiz2[0]) == 91){printf("Test 5 :: PASSED\n");}
    else{printf("TEST 5 :: FAILED\n");}
    if(computeScore(final2[1], mid2[1], proj2[1], quiz2[1]) == 89){printf("Test 6 :: PASSED\n");}
    else{printf("TEST 6 :: FAILED\n");}
    if(computeScore(final2[2], mid2[2], proj2[2], quiz2[2]) == 76){printf("Test 7 :: PASSED\n");}
    else{printf("TEST 7 :: FAILED\n");}

    return 0;
}
