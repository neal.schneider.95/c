# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.21

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake.exe

# The command to remove a file.
RM = /usr/bin/cmake.exe -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /c/code/c/evals/22-01/performance_a/grades

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /c/code/c/evals/22-01/performance_a/grades

# Include any dependencies generated for this target.
include CMakeFiles/TestCode.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include CMakeFiles/TestCode.dir/compiler_depend.make

# Include the progress variables for this target.
include CMakeFiles/TestCode.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/TestCode.dir/flags.make

CMakeFiles/TestCode.dir/testcases.cpp.o: CMakeFiles/TestCode.dir/flags.make
CMakeFiles/TestCode.dir/testcases.cpp.o: testcases.cpp
CMakeFiles/TestCode.dir/testcases.cpp.o: CMakeFiles/TestCode.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/c/code/c/evals/22-01/performance_a/grades/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/TestCode.dir/testcases.cpp.o"
	/usr/bin/c++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT CMakeFiles/TestCode.dir/testcases.cpp.o -MF CMakeFiles/TestCode.dir/testcases.cpp.o.d -o CMakeFiles/TestCode.dir/testcases.cpp.o -c /c/code/c/evals/22-01/performance_a/grades/testcases.cpp

CMakeFiles/TestCode.dir/testcases.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/TestCode.dir/testcases.cpp.i"
	/usr/bin/c++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /c/code/c/evals/22-01/performance_a/grades/testcases.cpp > CMakeFiles/TestCode.dir/testcases.cpp.i

CMakeFiles/TestCode.dir/testcases.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/TestCode.dir/testcases.cpp.s"
	/usr/bin/c++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /c/code/c/evals/22-01/performance_a/grades/testcases.cpp -o CMakeFiles/TestCode.dir/testcases.cpp.s

CMakeFiles/TestCode.dir/TestCode.c.o: CMakeFiles/TestCode.dir/flags.make
CMakeFiles/TestCode.dir/TestCode.c.o: TestCode.c
CMakeFiles/TestCode.dir/TestCode.c.o: CMakeFiles/TestCode.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/c/code/c/evals/22-01/performance_a/grades/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building C object CMakeFiles/TestCode.dir/TestCode.c.o"
	/usr/bin/cc.exe $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -MD -MT CMakeFiles/TestCode.dir/TestCode.c.o -MF CMakeFiles/TestCode.dir/TestCode.c.o.d -o CMakeFiles/TestCode.dir/TestCode.c.o -c /c/code/c/evals/22-01/performance_a/grades/TestCode.c

CMakeFiles/TestCode.dir/TestCode.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/TestCode.dir/TestCode.c.i"
	/usr/bin/cc.exe $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /c/code/c/evals/22-01/performance_a/grades/TestCode.c > CMakeFiles/TestCode.dir/TestCode.c.i

CMakeFiles/TestCode.dir/TestCode.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/TestCode.dir/TestCode.c.s"
	/usr/bin/cc.exe $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /c/code/c/evals/22-01/performance_a/grades/TestCode.c -o CMakeFiles/TestCode.dir/TestCode.c.s

# Object files for target TestCode
TestCode_OBJECTS = \
"CMakeFiles/TestCode.dir/testcases.cpp.o" \
"CMakeFiles/TestCode.dir/TestCode.c.o"

# External object files for target TestCode
TestCode_EXTERNAL_OBJECTS =

TestCode.exe: CMakeFiles/TestCode.dir/testcases.cpp.o
TestCode.exe: CMakeFiles/TestCode.dir/TestCode.c.o
TestCode.exe: CMakeFiles/TestCode.dir/build.make
TestCode.exe: lib/libgmock_main.a
TestCode.exe: lib/libgmock.a
TestCode.exe: lib/libgtest.a
TestCode.exe: CMakeFiles/TestCode.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/c/code/c/evals/22-01/performance_a/grades/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Linking CXX executable TestCode.exe"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/TestCode.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/TestCode.dir/build: TestCode.exe
.PHONY : CMakeFiles/TestCode.dir/build

CMakeFiles/TestCode.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/TestCode.dir/cmake_clean.cmake
.PHONY : CMakeFiles/TestCode.dir/clean

CMakeFiles/TestCode.dir/depend:
	cd /c/code/c/evals/22-01/performance_a/grades && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /c/code/c/evals/22-01/performance_a/grades /c/code/c/evals/22-01/performance_a/grades /c/code/c/evals/22-01/performance_a/grades /c/code/c/evals/22-01/performance_a/grades /c/code/c/evals/22-01/performance_a/grades/CMakeFiles/TestCode.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/TestCode.dir/depend

