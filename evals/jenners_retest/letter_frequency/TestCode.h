#pragma once

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif
    // Task One
    bool letterFrequency(const char *, int *);

#ifdef __cplusplus
}
#endif
