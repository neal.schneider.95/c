/*
SSgt Robert Jenners
9/7/2022
C-Programming Test: Grades
*/

#include <stdio.h>
#include <math.h>
#include "TestCode.h"

//attempting to make an error macro
// ns: error code is defined in header file so no need to define here
// however, this will expand to a literal string causing errors later on 
// #define ERROR "ERROR_INVALID_DATA"


/**
 * @brief //function that is called when error occurs
 * 
 * @return int 
 */
// no need for an error function, just return the error code
// int error(void)
// {
//         return ERROR;
// }

/**
 * @brief Computes the average of a student’s score with different weights for each part and returns the course grade
 * 
 * @param midTerm 
 * @param project 
 * @param quiz 
 * @return int 
 */
int computeScore(int final, int midTerm, int project[], int quiz[])
{
    //variables to hold collective scores
    float project_score = 0;
    // float lowest_project = 0; 
    float lowest_project = project[0]; //ns added 
    // ns: if lowest project starts at 0, no score will be less than that, 
    // and will remain zero
    float quiz_score = 0;
    int overall_score = 0;

    //making sure no value from first 2 ints are beyond scope for a test
    if((final < 0 || final > 100) ||(midTerm < 0 || midTerm > 100))
    {
        // error();  //ns: this doesn't return the error and execution continues
        return ERROR_INVALID_DATA;  //ns added
    }

    //looping through all scores in project
    for(int i = 0; i < 6; i++)
    {
        //making sure no value from project is beyond scope for a test
        if (project[i] < 0 || project[i] > 100)
        {
        // error();
        return ERROR_INVALID_DATA;  //ns added
        }
        else
        {
            //attempting to exclude the smallest test score
            // if(project[i]<project[i+1])    //ns added
            if(project[i]<lowest_project)
            {
                //setting smallest value to variable to be deleted later
                lowest_project=project[i];
            }
            project_score += project[i];
        }
    }
    //calculating total project scores minus lowest score
    project_score = project_score - lowest_project;
    
    //looping through all scores in quiz
    for(int i = 0; i < 4; i++)
    {
        //making sure no value from quiz is beyond scope for a test
        if (quiz[i] < 0 || quiz[i] > 100)
        {
        // error();
        return ERROR_INVALID_DATA;  //ns added
        }
        else
        {
            //adding the scores together
            quiz_score+=quiz[i];
        }
    }
    //calculating the overall score based on different criteria
    //ns: multiple errors in computation... see comments in merge request
    // overall_score = round((final * .30) + (midTerm * .25) + ((quiz_score/4) * .05) + ((project_score/5) * 05)) ;
    overall_score = round((final * .30) + (midTerm * .25) + ((quiz_score) * .05) + ((project_score) * .05)) ;

    //returning the overall score
    return overall_score;
    
}
