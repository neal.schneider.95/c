#pragma once

#include <stdio.h>

#define ERROR_INVALID_DATA 13

#ifdef __cplusplus
extern "C" {
#endif
    // Task One
    int computeScore(int final, int midTerm, int project[], int quiz[]);

#ifdef __cplusplus
}
#endif
