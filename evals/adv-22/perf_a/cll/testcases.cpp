#include <gmock/gmock.h>
#include "TestCode.h"



TEST(TestCase2, circular_test_normalCases)
{
	int nums2[] = { 90,80,70,60,50 };

	struct numNode *res = buildCList(nums2, sizeof(nums2) / sizeof(*nums2));
	if (res == NULL)
		ASSERT_EQ(1, 0); //res should not be NULL
	struct numNode *temp = res;

	for (int i = 4; i >= 0; i--)
	{
		ASSERT_EQ(temp->num, nums2[i]);
		temp = temp->next;
	}

	ASSERT_EQ(temp->num, 50);

	int count = emptyList(res);

	ASSERT_EQ(5, count);


    int nums4[] = {5, 7,8,7,9,2,34,5,54,1};
    int nums3[] = {5, 7,8,9,2,34,54,1};
    
	res = buildCList(nums4, sizeof(nums4) / sizeof(*nums4));
	if (res == NULL)
		ASSERT_EQ(1, 0); //res should not be NULL
	temp = res;

	for(int i = 7; i >=0; i--)
	{
		ASSERT_EQ(temp->num, nums3[i]);
		temp = temp->next;
	}

      ASSERT_EQ(temp->num, 1);

	  count = emptyList(res);

	  ASSERT_EQ(8, count);

	  res = buildCList(nums2, 0);
	  if (res == NULL)
		  ASSERT_EQ(1, 1); //res should be NULL
	  
	  count = emptyList(res);
	  ASSERT_EQ(0, count);
}