/*
Neal Schneider, IDF Instructor
1 Sep 22
Subject matter test verification
*/
#include <stdio.h>
#include <stdlib.h>
#include "TestCode.h"
// #define RUNLOCAL 1

typedef struct numNode NumNode;

// Refer to README.md for the problem instructions

int printQueue(NumNode **front){
    NumNode *link = *front;
    int count = 0;
    while (link) {
        printf("%d->", link->num);
        link = link->next;
        count++;
    }
    printf("[end %d]\n", count);
}

int enqueue(struct numNode **end, int data)
{
    if (!end) return 1;  // check for NULL end
    NumNode *newNode = (NumNode*) malloc(sizeof(NumNode));
    if (!newNode) return 1;  // malloc failed so return error
    if (*end) {  // non-empty queue so add num to end
        (*end)->next = newNode;
    }
    newNode->next = NULL;
    newNode->num = data;
    *end = newNode;
    return 0;
}

int dequeue(struct numNode **front)
{
    if (!front || !(*front)) return 0;  // check for NULL front and empty queue
    NumNode *tmp = *front;
    int data = (*front)->num;
    *front = (*front)->next;
    free(tmp);
    return data;
}

void emptyQueue(struct numNode **front)
{
    if (!front || !(*front)) return; // check for NULL front and empty queue
    NumNode *link = *front;
    while (link) {
        NumNode *tmp = link;
        link = link->next;
        free (tmp);
    }
    *front = NULL;
    return;
}

struct numNode *makeQueue(int actions[], int numActions)
{
    // Check for null actions, and bad number of actions
    if (!actions || numActions < 1  ) return NULL;
    NumNode *front = NULL, *end = NULL;
    for (int i = 0; i < numActions*2; i+=2) {
        switch (actions[i])
        {
        case 1:
            dequeue(&front);
            break;
        case 2:
            if (!front) {  // Empty Queue
                enqueue(&front, actions[i+1]);
                end = front;
            } else {   // non-empty, so add to end
                enqueue(&end, actions[i+1]);
            }
            break;
        case 3:
            emptyQueue(&front);
            break;
        default:
            return NULL; // Action not valid (1,2,3)
            break;
        }
    }
    return front;
}

#ifdef RUNLOCAL
int main (){
    int actions[] = {2, 4, 2, 5, 2, 10};
    struct numNode *head = makeQueue(actions, 3);
    struct numNode *res = head;
    printQueue(&head);
}
#endif