#include <stdio.h>
#include <math.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

#define ERROR -1

int invalid(int score){
    return (score < 0 || score > 100);
}

int computeScore(int final, int midTerm, int project[], int quiz[])
{
    int low_proj = project[0];
    int low_index = 0;
    float sum;
    if (invalid(final) || invalid (midTerm)) {
        return ERROR;
    }
    
    // find minimum project score
    for (int i = 1; i<6; i++){
        if (low_proj > project[i]) {
            low_proj = project[i];
            low_index = i;

        } 
    }
    sum = final * 0.3 + midTerm * 0.25;
    for (int i = 0; i<4; i++) {
        if (invalid(quiz[i])) return ERROR;
        sum += quiz[i] * 0.05;
    }
    // add the portion
    for (int i =0; i<6; i++) {
        if (invalid(project[i])) return ERROR;
        if (i != low_index) {
            sum += project[i] * 0.05;
        }
    }
    return round(sum);
}